export const environment = {
  production: false,

  API_URL: 'http://103.109.230.11:8086/api/',

  // image Path
  defaultCompany: 'वित्तीय हस्तान्तरण प्रबन्धन प्रणाली',
  defaultImagePath: './assets/img/province_4.jpg',

  // Menu Actions
  menuActions: {
    create: 'iscreate',
    update: 'isupdate',
    delete: 'isdelete',
    view: 'isview',
    report: 'isreport',
    apporve: 'isapprove'
  },

  // Menu Codes
  menuCodes: {
    // setup modules
    fiscalYearSetup: 'S001',
    districtSetup: 'S003',
    localBodySetup: 'S004',
    documentTypeSetup: 'S005',
    officeSetup: 'S006',
    employeeSetup: 'S007',
    applicationProfileSetup: 'S009',

    // Security Modules
    roleSetup: 'A001',
    userSetup: 'A002',
    userRoleOfficeSetup: 'A003',
    roleMenuRightSetup: 'A005',

  }
};
