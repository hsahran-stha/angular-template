import { Injectable } from '@angular/core';
import {FiscalYears} from "../../models/init/fiscal-years";
import {FiscalYear} from "../../models/init/fiscal-year";

/**
 * Fiscal Year Service
 */
@Injectable({
  providedIn: 'root'
})
export class FiscalYearsService {

  /**
   * Current Fiscal Years Instance
   * @type {fiscalYears}
   */
  public fiscalYears: FiscalYears = new FiscalYears({});

  /**
   * Default Constructor
   */
  constructor() {}

  /**
   * Set FiscalYears Instances
   */
  public setFiscalYears(fiscalYears: FiscalYears) {
    this.clearParam();
    if (fiscalYears) this.fiscalYears = fiscalYears;
  }

  /**
   * Clear current FiscalYears
   */
  public clearParam() {
    this.fiscalYears = Object.assign({});
  }

  /**
   * Check has a fiscalYears of given property
   * @param  {string}     property name to get fiscalYears
   * @return {boolean}   boolean
   */
  public has(property: string): boolean {
    return this.fiscalYears[property] &&
    this.fiscalYears[property] != null
      ? true
      : false;
  }

  /**
   * Return a fiscalYears value of given property
   * @param  {[type]} property
   * @return {any}
   */
  public get(property: string): FiscalYear | null {
    return this.has(property) && this.fiscalYears[property];
  }

  /**
   * Sets the current fiscalYears property value
   * @param {string} property property name to set
   * @param {any}    value property name to set
   */
  public set(property: string, value: any): void {
    this.fiscalYears[property] = value;
  }
}
