import { Injectable } from '@angular/core';
import {BudgetGlobalParams} from "../../models/init/budget-global-params";

@Injectable({
  providedIn: 'root'
})
export class BudgetGlobalParamsService {

  /**
   * Current BudgetGlobalParams Instance
   * @type {budgetGlobalParams}
   */
  public budgetGlobalParams: BudgetGlobalParams = new BudgetGlobalParams({});

  /**
   * Default Constructor
   */
  constructor() {}

  /**
   * Set BudgetGlobalParams Instances
   */
  public setBudgetGlobalParams(budgetGlobalParams: BudgetGlobalParams) {
    this.clearParam();
    if (budgetGlobalParams) this.budgetGlobalParams = budgetGlobalParams;
  }

  /**
   * Clear current BudgetGlobalParams
   */
  public clearParam() {
    this.budgetGlobalParams = Object.assign({});
  }

  /**
   * Check has a BudgetGlobalParams of given property
   * @param  {string}     property name to get BudgetGlobalParams
   * @return {boolean}   boolean
   */
  public has(property: string): boolean {
    return this.budgetGlobalParams[property] &&
    this.budgetGlobalParams[property] != null
      ? true
      : false;
  }

  /**
   * Return a budgetGlobalParams value of given property
   * @param  {[type]} property
   * @return {any}
   */
  public get(property: string): any {
    return this.has(property) && this.budgetGlobalParams[property];
  }

  /**
   * Sets the current budgetGlobalParams property value
   * @param {string} property property name to set
   * @param {any}    value property name to set
   */
  public set(property: string, value: any): void {
    this.budgetGlobalParams[property] = value;
  }
}
