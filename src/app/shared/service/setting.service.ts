import { Injectable } from '@angular/core';
import { Setting } from '../models/setting';

@Injectable({
  providedIn: 'root',
})
export class SettingsService {
  /**
   * Current Setting Instance
   * @type {Setting[]}
   */
  public all: Setting[];

  /**
   * Default Constructor
   */
  constructor() {}

  /**
   * Set All Setting Instances
   * @param {Setting[]} settings
   */
  public setSettings(settings: Setting[]) {
    if (settings) this.all = settings;
  }

  /**
   * Clear current setting
   */
  public clear() {
    this.all = Object.assign({});
  }

  /**
   * Check has a setting of given name
   * @param  {string}     name name to get setting
   * @param  {boolean =    false}       returnVal retrun Value
   * @return {boolean}   boolean|value
   */
  public has(name: string, returnVal: boolean = false): boolean {
    let value = null;
    this.all.forEach((setting) => {
      if (setting.name == name) {
        //return value or booelan
        value = returnVal == true ? setting.value : true;
      }
    });
    return value || false;
  }

  /**
   * Return a setting value of given name
   * @param  {[type]} name
   * @return {any}
   */
  public get(name): any {
    //Check has value as return true
    return this.has(name, true);
  }

  /**
   * Return all setting values
   * @return {Setting[]}
   */
  public getAll(): Setting[] {
    return this.all;
  }
}
