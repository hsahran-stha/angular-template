import {Injectable} from '@angular/core';

import {Title} from '@angular/platform-browser';
import {environment} from '../../../environments/environment';


const APP_TITLE = environment.defaultCompany;

@Injectable({
  providedIn: 'root'
})
export class TitleService {
  constructor(
    private titleService: Title
  ) {
  }

  init(title?: string) {
    this.titleService.setTitle(title ? title : APP_TITLE);
  }

}
