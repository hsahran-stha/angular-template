import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LocalJsonService {
  constructor(private http: HttpClient) {}

  /**
   * Function that returns json file from a specific path
   * @param jsonPath Json File Path
   */
  public getJSON(jsonPath: string): Observable<any> {
    return this.http.get(jsonPath);
  }
}
