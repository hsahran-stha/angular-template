import {Injectable, Injector} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
/***
 * App Base Service
 */
export class AppService {

  /**
   * Http Client Instance
   */
  public httpClient: HttpClient;

  /**
   * Default Constructor
   * @param injector
   */
  constructor(public injector: Injector) {
    this.httpClient = this.injector.get(HttpClient);
  }

  /**
   * Prepare Param Query
   * @param queryObj
   */
  public prepareQueryParams(queryObj: Object) {
    let paramString = '';
    for (let param in queryObj) {
      paramString += `${param}=${queryObj[param]}/`;
    }
    return paramString;
  }

  /**
   * Prepare Param Query
   * @param queryObj
   */
  public prepareQueryAndParams(queryObj: Object) {
    let paramString = '';
    for (let param in queryObj) {
      paramString += `${param}=${queryObj[param]}&`;
    }
    return paramString.slice(0, -1);
  }
}
