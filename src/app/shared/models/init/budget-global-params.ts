import {BudgetGlobalParam} from "./budget-global-param";

/**
 * Global Params Config
 */
export class BudgetGlobalParams {
  previous: BudgetGlobalParam;
  current: BudgetGlobalParam;
  next: BudgetGlobalParam;

  /**
   * Default Constructor
   * @param params
   */
  constructor(params: Object = {}) {
    for(let param in params) {
      this[param] = params[param];
    }
  }
}
