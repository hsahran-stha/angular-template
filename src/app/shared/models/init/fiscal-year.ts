/**
 * Fiscal Year Class
 */
import {BudgetGlobalParam} from "./budget-global-param";

export class FiscalYear {
  id: number;
  fiscalYear: string;
  startDate: string;
  endDate: string;
  oprStatus: number;
  status: number;
  budgetGlobalParams: BudgetGlobalParam;

  /**
   * Current Period Data
   */
  currentPeriod: Object = null;

  /**
   * Default Constructor
   * @param params
   */
  constructor(params: object = {}) {
    for(let param in params) {
      this[param] = params[param];
    }
  }
}
