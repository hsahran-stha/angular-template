export class BudgetGlobalParam {
  id: number;
  fiscalyear: string;
  datetype: number;
  oprstatys: number;
  realeasefrequencyid: number;
  fiscalyearid: number;
  approriationfrequencyid: number;
  budgetglobalparamasterid: number;
  advancebudgetpct: number;
  startdate: string;
  enddate: string;
  expensefequencyid: number;
  globalbudgetworkflowauthlevel: number;
  globalreviewworkflowauthlevel: number;
  globalgenericworkflowauthlevel: number;

  /**
   * Default Constructor
   * @param {Object = {}} params
   */
  constructor(params: Object = {}) {
    for (let param in params) {
      this[param] = params[param];
    }
  }
}
