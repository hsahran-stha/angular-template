import {FiscalYear} from "./fiscal-year";

/**
 * Fiscal Years Config
 */
export class FiscalYears {
  previous: FiscalYear;
  current: FiscalYear;
  next: FiscalYear;

  /**
   * Default Constructor
   * @param params
   */
  constructor(params: Object = {}) {
    for(let param in params) {
      this[param] = params[param];
    }
  }
}
