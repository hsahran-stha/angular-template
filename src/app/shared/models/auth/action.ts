/**
 * Action Model
 */
export class Action {
  id: number;
  name: string;

  /**
   * Default Model Constructor
   * @param {Object = {}} params
   */
  constructor(params: Object = {}) {
    for (let name in params) {
      this[name] = params[name];
    }
  }
}
