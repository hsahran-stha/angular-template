export class Department {
  id: number;
  deptnameloc: string;
  deptaliasloc: string;
  deptcode: string;
  deptname: number;
  deptalias: string;
  isbudgetmasterdepartment: boolean;

  /**
   * Model Constructor
   * @param {Object = {}} params
   */
  constructor(params: Object = {}) {
    for (let name in params) {
      this[name] = params[name];
    }
  }
}
