export class UserDetails {
  //user details
  UserTypeName: string
  address: string
  email: string
  employeeName: string
  employeeid: string
  entrydate: any
  fullname: string
  firstname: string
  images: any
  logo: any
  languageoption: number
  phonenumber: string
  status: number
  themeoption: number
  twofactorenabled: boolean
  usertype: number

  /**
   * Model Constructor
   * @param {Object = {}} params
   */
  constructor(params: Object = {}) {
    for (let name in params) {
      this[name] = params[name];
    }
  }
}
