/**
 * User Model Class
 */
import {MaxAuthData} from "./maxAuthData";

export class Menu {
  applicationmenu_id: number;
  menuname: string;
  menunamelocal: string;
  menucode: string;
  menuiconclass: string;
  controllername: string;
  mastermenuid: number;
  url: string;

  iscreate: boolean;
  isupdate: boolean;
  isdelete: boolean;
  isreport: boolean;
  isview: boolean;
  approval: boolean; // Indicates Users Navigate to Current Menu From Approval Dashboard
  hide: boolean = false;
  children: Array<Menu> = [];
  maxAuthData: MaxAuthData;
  countApprovalData: number;

  /**
   * Model Constructor
   * @param {Object = {}} params
   */
  constructor(params: Object = {}) {
    for (let name in params) {
      this[name] = params[name];
    }
  }
}
