export class ApplicationProfile {
  favIcon: number;
  applicationUrl: string;
  fullLogo: string;
  applicationName: string;
  applicationNameLocal: string;
  shortlogo: number
  deptalias: string;

  /**
   * Model Constructor
   * @param {Object = {}} params
   */
  constructor(params: Object = {}) {
    for (let name in params) {
      this[name] = params[name];
    }
  }
}
