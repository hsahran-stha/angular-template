export class Branch {
  id: number;
  projectcode: string;
  officename: string;
  officenamelocal: string;
  branchtypeid: number;
  branchtypename: string;
  assignmentmasterid: number;
  roles: string;


  /**
   * Model Constructor
   * @param {Object = {}} params
   */
  constructor(params: Object = {}) {
    for (let name in params) {
      this[name] = params[name];
    }
  }
}
