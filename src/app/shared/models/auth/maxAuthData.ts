
export class MaxAuthData {
  applicationmenuid: number;
  flowmasterid: number;
  maxauthlevel: number;
  workflowtypeid: number;

  /**
   * Model Constructor
   * @param {Object = {}} params
   */
  constructor(params: Object = {}) {
    for (let name in params) {
      this[name] = params[name];
    }
  }
}
