import {Role} from './role';
import {Authorities} from './authorities';
import {Branch} from './branch';
import {Menu} from './menu';
import {UserDetails} from './userDetails'
import {Department} from './department';

/**
 * User Model Class
 */
export class User {
    userId: number;
    passwordExpired: Boolean;
    username: string;
    roles: Role[];
    authorities: Authorities[];
    branches: Branch[];
    department: Department;
    departments: Department[];
    menus: Menu[];
    currentMenu: Menu;
    userDetails: UserDetails

    /**
     * Model Constructor
     * @param {Object = {}} params
     */
    constructor(params: Object = {}) {
        for (let name in params) {
            this[name] = params[name];
        }
    }
}
