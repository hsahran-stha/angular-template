import { Action } from './action';
/**
 * Auth Role Model
 */
export class Role {
  roleId: number;
  roleName: string;
  roleName_np: string;
  description: string;

  actions: Action[];
  /**
   * Model Constructor
   * @param {Object = {}} params
   */
  constructor(params: Object = {}) {
    for (let name in params) {
      this[name] = params[name];
    }
  }
}
