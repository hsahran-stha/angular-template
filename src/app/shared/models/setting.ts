/**
 * Application Setting Model
 */
export class Setting {
  id: number;
  name: string;
  value: string;

  /**
   * Default Constructor
   * @param {Object = {}} params
   */
  constructor(params: Object = {}) {
    for (let param in params) {
      this[param] = params[param];
    }
  }
}
