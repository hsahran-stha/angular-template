import {AbstractControl, ValidationErrors} from '@angular/forms';

export class FiscalYearValidator {
  static validText(control: AbstractControl): ValidationErrors | null {
    if (control.value != null) {
      let fiscal_year = control.value.split('/');
      let curYear = parseInt(fiscal_year[0].toString().substring(2));
      let nextYear = parseInt(fiscal_year[1]);

      if (control.value.length !== 7 || (nextYear - curYear) !== 1 && (nextYear - curYear) !== -99  )  {
        return {inValidText: true};
      }
    }
    return null;
  }
}
