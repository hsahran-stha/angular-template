import {AbstractControl, FormControl, Validators} from '@angular/forms';

export class OfficeEmailValidator {
  static commaSepEmail = (control: AbstractControl): { [key: string]: any } | null => {
    let emails = [];
    if (control.value) {
      emails = control.value.split(',').map(e => e.trim());
    }
    const forbidden = emails.some(email => Validators.email(new FormControl(email)));
    return forbidden ? {'email': {value: control.value}} : null;
  };

}


