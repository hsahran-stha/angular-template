import {AbstractControl, FormControl, Validators} from '@angular/forms';

export class NumberValidator {

  static commaSepNumber = (control: AbstractControl): { [key: string]: any } | null => {
    console.log(control.value);
    let mobilenum = [];
    if (control.value) {
      mobilenum = control.value.split(',').map(e => e.trim());
    }
    const forbidden = mobilenum.some(mobilenumber => Validators.pattern('^9\\d{9}$')(new FormControl(mobilenumber)));
    return forbidden ? {'mobilenumber': {value: control.value}} : null;
  };
}
