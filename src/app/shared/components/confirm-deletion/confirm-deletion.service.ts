import {
  EmbeddedViewRef,
  Injectable,
  ComponentFactoryResolver,
  ComponentRef,
  ApplicationRef,
  Injector
} from '@angular/core';
import {ConfirmDeletionComponent} from './confirm-deletion/confirm-deletion.component';


@Injectable({
  providedIn: 'root'
})
export class ConfirmDeletionService {

  componentRef: ComponentRef<ConfirmDeletionComponent>;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
    private injector: Injector,
  ) {
  }

  public show() {
    this.drop();
    let componentFactory = this.componentFactoryResolver.resolveComponentFactory(ConfirmDeletionComponent);
    this.componentRef = componentFactory.create(this.injector);
    this.appRef.attachView(this.componentRef.hostView);
    const domElem = (this.componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
    document.getElementById('confirmDeletion').appendChild(domElem);
  }

  /**
   * Drop a component if exists
   *
   * @return void
   */
  public drop() {
    if (this.componentRef) {
      this.appRef.detachView(this.componentRef.hostView);
      this.componentRef.destroy();
    }
  }


  ngOnDestroy() {
  }
}
