import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConfirmDeletionComponent} from './confirm-deletion/confirm-deletion.component';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [ConfirmDeletionComponent],
    imports: [
        CommonModule,
        TranslateModule
    ],
  exports: [ConfirmDeletionComponent]
})
export class ConfirmDeletionModule {
}
