import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ButtonsComponent} from './buttons/buttons.component';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [ButtonsComponent],
  imports: [
    CommonModule, TranslateModule
  ],
  exports: [ButtonsComponent]
})
export class ButtonsModule {
}
