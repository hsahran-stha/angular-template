import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CurrentUserService} from '../../../../auth/current-user.service';
import {environment} from '../../../../../environments/environment';
import {Status} from '../../../enums/status';

@Component({
  selector: 'app-buttons',
  templateUrl: './buttons.component.html',
  styleUrls: ['./buttons.component.scss']
})
export class ButtonsComponent implements OnInit {

  @Input() public button: object = {
    save: false,
    saveAsDraft: false,
    saveAsFinal: false,
    requestSaveAsFinal: false,
    selectionSaveAsFinal: false,
    delete: false,
    reset: false,
    create: false,
    exit: false,
    update: false,
    approve: false,
    backForReview: false,
    count: false,
    history: false,
    generate: false,
    bulkGrantTransfer: false,
  };

  @Output() save = new EventEmitter();
  @Output() saveAsDraft = new EventEmitter();
  @Output() requestSaveAsFinal = new EventEmitter();
  @Output() selectionSaveAsFinal = new EventEmitter();
  @Output() reset = new EventEmitter();
  @Output() delete = new EventEmitter();
  @Output() exit = new EventEmitter();
  @Output() create = new EventEmitter();
  @Output() approve = new EventEmitter();
  @Output() backForReview = new EventEmitter();
  @Output() history = new EventEmitter();
  @Output() generate = new EventEmitter();
  @Output() bulkGrantTransfer = new EventEmitter();

  @Input() countItem: number;

  public environment = environment;

  constructor(public currentUser: CurrentUserService) {
  }

  ngOnInit(): void {
  }

  saveClicked(): void {
    this.save.emit(true);
  }

  bulkGrantTransferClicked(): void {
    this.bulkGrantTransfer.emit(true);
  }

  requestSaveFinalClicked(): void {
    this.requestSaveAsFinal.emit(true);
  }

  selectionSaveFinalClicked(): void {
    this.selectionSaveAsFinal.emit(true);
  }

  saveDraftClicked(): void {
    this.saveAsDraft.emit(true);
  }

  resetClicked(): void {
    this.reset.emit(true);
  }

  deleteClicked(): void {
    this.delete.emit(true);
  }

  createClicked(): void {
    this.create.emit(true);
  }

  exitClicked(): void {
    this.exit.emit(true);
  }

  approveClicked(): void {
    this.approve.emit(Status.ACTIVE);
  }

  backForReviewClicked(): void {
    this.backForReview.emit(Status.BACK_FOR_REVIEW);
  }

  historyClicked(): void {
    this.history.emit(true);
  }

  generateClicked(): void {
    this.generate.emit(true);
  }

}
