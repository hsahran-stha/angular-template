import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DocumentUploadComponent} from './document-upload/document-upload.component';
import {PqgridModule} from '../pqgrid/pqgrid.module';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {ButtonsModule} from '../buttons/buttons.module';


@NgModule({
  declarations: [DocumentUploadComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TranslateModule,
    PqgridModule,
    ButtonsModule
  ],
  exports: [DocumentUploadComponent]
})
export class DocumentUploadModule {
}
