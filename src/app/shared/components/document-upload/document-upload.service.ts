import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
/***
 * App Base Service
 */
export class DocumentUploadService {

  private uploadDocumentUrl: string = `${environment.API_URL}document/upload`;
  private getDocByIdUrl: string = `${environment.API_URL}document/`;

  constructor(public httpClient: HttpClient) {
  }

  uploadDocument(data: FormData): Observable<object> {
    return this.httpClient.post(this.uploadDocumentUrl, data).pipe(map(data => data['data']));
  }

  downloadFile(docGuid: string): Observable<HttpResponse<Blob>> {
    return this.httpClient.get<Blob>(`${this.getDocByIdUrl}${docGuid}`, {
      observe: 'response',
      responseType: 'blob' as 'json'
    });
  }


}
