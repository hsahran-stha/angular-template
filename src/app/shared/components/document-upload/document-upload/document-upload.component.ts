import {Component, EventEmitter, ElementRef, Injector, OnInit, Output, ViewChild, Input, OnChanges, SimpleChanges} from '@angular/core';
import {AppController} from '../../../../app.controller';
import {FormGroup, Validators} from '@angular/forms';
import {DocumentUploadService} from '../document-upload.service';
import {map, take} from 'rxjs/operators';
import {DocumentTypeService} from '../../../../featured/setup/document-type/document-type.service';
import {Status} from '../../../enums/status';
import {HttpResponse} from '@angular/common/http';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';
import {TransactionAction} from '../../../enums/transactionAction';
import {fileExtensionType} from '../../../enums/fileExtentionType';

@Component({
  selector: 'app-document-upload',
  templateUrl: './document-upload.component.html',
  styleUrls: ['./document-upload.component.scss']
})

export class DocumentUploadComponent extends AppController implements OnInit, OnChanges {

  public documentUpload: FormGroup;
  public documentTypeList: object[];

  public documentColumns: object[];

  // to display in grid only means this variable does not exists row with action==3 i.e 3 means DELETE
  public documentList: object[] = [];
  // to save and update that contain all data with having action=3 also
  public documentListToSaveUpdate: object[] = [];

  public selectedDocumentType: object;

  public tempFileName: string;

  public displayImage: SafeUrl;
  public displayPdf: SafeUrl;

  @Output() fileUploaded: EventEmitter<object> = new EventEmitter();

  public isImage: boolean = false;
  public isPdf: boolean = false;
  public isDocument: boolean = false;
  public isSlide: boolean = false;
  public isExcel: boolean = false;

  @Input() pageName: string;
  @Input() dataFromAPI: object;
  @Input() delayTimer: boolean = false;

  constructor(public injector: Injector,
              private documentUploadService: DocumentUploadService,
              private dom: DomSanitizer,
              private documentTypeService: DocumentTypeService) {
    super(injector);
  }

  ngOnInit(): void {
    this.fetchDocumentType();
    this.initializeForm();
    this.manageColumn();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.dataFromAPI && changes.dataFromAPI.currentValue) {
      this.documentList = changes.dataFromAPI.currentValue;
      changes.dataFromAPI.currentValue.forEach(item => {
        this.documentListToSaveUpdate.push(item);
      });
      this.fileUploaded.emit(this.documentListToSaveUpdate);
    }
  }

  fetchDocumentType(): void {
    this.documentTypeService.get(Status.ACTIVE).pipe(
      map(data => {
        this.documentTypeList = data;
      })
    ).subscribe();
  }

  manageColumn(): void {
    this.documentColumns = [
      this.manageEachColumn('documentType', 'documentTypeName', true),
      this.manageEachColumn('documentTitle', 'docTitle', true, true),
      this.manageEachColumn('documentNumber', 'docNo', true),
      this.manageEachColumn('mimeType', 'mimeType', true),
      this.manageEachColumn('size', 'size', true),
      this.pageName === 'create' || this.pageName === 'edit' ?
        this.manageEachColumn('action', '', false, false, '', 'view-download-delete', this) :
        this.manageEachColumn('action', '', false, false, '', 'view-download', this),
    ];
  }

  @ViewChild('contentImgLg') contentImgLg: ElementRef;
  @ViewChild('contentPdfLg') contentPdfLg: ElementRef;

  actionView(ui): void {

    const ext = ui.rowData.mimeType.split('/')[1];
    const isPdf: boolean = ext == fileExtensionType.PDF;
    const isImage: boolean = ext == fileExtensionType.JPEG || ext == fileExtensionType.JPG || ext == fileExtensionType.PNG || ext == fileExtensionType.GIF;

    if (isPdf || isImage) {
      this.documentUploadService.downloadFile(ui.rowData.docGuid).subscribe((data: HttpResponse<Blob>) => {
        if (isPdf) {
          const blob = new Blob([data.body], {type: 'application/pdf'});
          const url = URL.createObjectURL(blob);
          this.displayPdf = this.dom.bypassSecurityTrustResourceUrl(url);
          this.openLg(this.contentPdfLg, 'xl');
          return;
        } else if (isImage) {
          const blob = new Blob([data.body]);
          const url = URL.createObjectURL(blob);
          this.displayImage = this.dom.bypassSecurityTrustUrl(url);
          this.openLg(this.contentImgLg, 'xl');
        }

      });
    } else {
      this.alertify.warning(this.translate.instant('cannotView'));
      return;
    }
  }

  actionDownload(ui): void {
    this.documentUploadService.downloadFile(ui.rowData.docGuid).subscribe((data: HttpResponse<Blob>) => {
      var a = document.createElement('a');
      var blob = new Blob([data.body]);
      a.href = window.URL.createObjectURL(blob);
      a.download = data.url.split('/').pop();
      a.click();
    });
  }

  actionDelete(ui): void {

    // to show confirmation box
    let obj = {
      body: this.translate.instant('Delete Confirmation'),
      btnClass: 'btn-danger',
      alertClass: 'ic-alert',
      buttonName: this.translate.instant('confirm')
    };
    this.confirmDialogBoxService.show(obj);
    const processConfirmation$ = this.confirmDialogBoxService.getProcessConfirmation();
    processConfirmation$.pipe(take(1)).subscribe((val) => {
      if (val) {
        if (ui.rowData.id) {
          // UPDATE CASE
          ui.rowData.action = TransactionAction.DELETE;

          // filter data to remove form table grid
          this.documentList = this.documentList.filter(d => d['id'] != ui.rowData.id);

          // filter already contain data
          this.documentListToSaveUpdate = this.documentListToSaveUpdate.filter(d => d['id'] != ui.rowData.id);

          // push data with action == 3
          this.documentListToSaveUpdate.push(ui.rowData);
        } else {
          // NEW CREATE CASE
          this.documentList = this.documentList.filter(d => d['docGuid'] != ui.rowData.docGuid);
          this.documentListToSaveUpdate = this.documentListToSaveUpdate.filter(d => d['docGuid'] != ui.rowData.docGuid);
        }
        this.fileUploaded.emit(this.documentListToSaveUpdate);

      }
    });

  }


  initializeForm(): void {
    this.documentUpload = this.formBuilder.group({
      documentTypeId: ['', Validators.required],
      documentTypeName: [''],
      docTitle: ['', Validators.required],
      docNo: [''],
      docGuid: [''],
      documentFile: ['', Validators.required],
      mimeType: ['', Validators.required],
      size: ['', Validators.required],
      action: [TransactionAction.ADD]
    });
  }

  processFile(imageInput: any): void {
    const file: File = imageInput.files[0];
    const reader = new FileReader();
    if (!this.selectedDocumentType) {
      this.alertError();
      return;
    }
    let fileExtension: string;

    if (!file) {
      return;
    }
    if (parseInt(String(file.size / 1024)) > parseInt(this.selectedDocumentType['maxfilesizeinkb'])) {
      if (this.translate.currentLang == 'np') {
        this.alertify.warning(`चयनित फाईलको आकार
      ${parseInt(String(file.size / 1024))} KB हो। अधिकतम फाईल आकार सीमा
       ${parseInt(this.selectedDocumentType['maxfilesizeinkb'])} KB छ।`);
      } else {
        this.alertify.warning(`Size of selected file is
      ${parseInt(String(file.size / 1024))} KB. Maximum file size limit is
       ${parseInt(this.selectedDocumentType['maxfilesizeinkb'])} KB`);
      }
      this.isImage = false;
      this.isPdf = false;
      this.isDocument = false;
      this.isSlide = false;
      this.isExcel = false;
      return;
    }

    fileExtension = file.name.split('.')[1].toLowerCase();
    console.log(fileExtension);
    this.tempFileName = '';
    this.isPdf = false;
    if (fileExtension == fileExtensionType.JPG ||
      fileExtension == fileExtensionType.JPEG ||
      fileExtension == fileExtensionType.PNG) {
      if (!this.isAllowed('isimageallowedstatus')) {
        return;
      }
    } else if (fileExtension == fileExtensionType.DOCX ||
      fileExtension == fileExtensionType.DOC ||
      fileExtension == fileExtensionType.PDF) {
      if (fileExtension == fileExtensionType.PDF) {
        this.isPdf = true;
      }
      if (!this.isAllowed('isdocumentsallowedstatus')) {
        return;
      }
    } else if (fileExtension == fileExtensionType.PPT ||
      fileExtension == fileExtensionType.PPTX) {
      if (!this.isAllowed('isslideallowedstatus')) {
        return;
      }
    } else if (fileExtension == fileExtensionType.XLS ||
      fileExtension == fileExtensionType.XLSX) {
      if (!this.isAllowed('isexcelallowedstatus')) {
        return;
      }
    } else {
      this.alertError();
      return;
    }
    reader.addEventListener('load', (event: any) => {
      const selectedFile = new FileSnippet(event.target.result, file);
      this.tempFileName = selectedFile.file.name;
      this.documentUpload.patchValue({
        documentFile: selectedFile.file,
        mimeType: selectedFile.file.type,
        size: parseInt(String(selectedFile.file.size / 1024))
      });
    });
    reader.readAsDataURL(file);
  }

  onDocumentTypeChange($event): void {
    const getId = $event.target.value;
    this.selectedDocumentType = this.documentTypeList.find(d => d['id'] == getId);
    console.log(this.selectedDocumentType);
    this.documentUpload.patchValue({
      documentTypeName: this.translate.currentLang == 'np' ? this.selectedDocumentType['name'] : this.selectedDocumentType['namelocal']
    });
  }

  onAddDocument(): void {
    if (this.documentUpload.invalid) {
      this.validateAllFormFields(this.documentUpload);
      this.displayInvalidFormControls(this.documentUpload);
      return;
    }
    this.documentUploadService.uploadDocument(this.createFormData(this.documentUpload.value)).pipe(
      map(data => {
        this.documentList.push(data);
        this.documentListToSaveUpdate.push(data);
        this.fileUploaded.emit(this.documentListToSaveUpdate);
        this.documentUpload.reset();
        this.documentUpload.patchValue({
          documentTypeId: '',
          action: TransactionAction.ADD
        });
        this.tempFileName = '';
      })
    ).subscribe();

  }

  isAllowed(status): boolean {
    this.isImage = status == 'isimageallowedstatus';
    this.isDocument = status == 'isdocumentsallowedstatus';
    this.isSlide = status == 'isslideallowedstatus';
    this.isExcel = status == 'isexcelallowedstatus';

    if (!this.selectedDocumentType[status]) {
      this.alertError();
      this.documentUpload.patchValue({
        documentFile: '',
      });
      return false;
    }
    return true;
  }

  alertError(): void {
    this.isImage = false;
    this.isPdf = false;
    this.isDocument = false;
    this.isSlide = false;
    this.isExcel = false;

    this.alertify.warning(this.translate.instant('extensionValidation'));
  }

}


class FileSnippet {
  constructor(public src: string, public file: File) {
  }
}
