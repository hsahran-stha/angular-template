import {
  EmbeddedViewRef,
  Injectable,
  ComponentFactoryResolver,
  ComponentRef,
  ApplicationRef,
  Injector
} from '@angular/core';
import {PqgridComponent} from './pqgrid/pqgrid.component';


@Injectable({
  providedIn: 'root'
})
export class PqgridService {

  componentRef: ComponentRef<PqgridComponent>;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
    private injector: Injector,
  ) {
  }

  show(obj) {
    this.drop();
    let componentFactory = this.componentFactoryResolver.resolveComponentFactory(PqgridComponent);
    this.componentRef = componentFactory.create(this.injector);

    this.appRef.attachView(this.componentRef.hostView);

    const instance = this.componentRef.instance;
    instance.columns = obj.columns;
    instance.data = obj.data;
    instance.searchBox = true;

    const domElem = (this.componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;

    document.getElementById('pqGridTemplate').appendChild(domElem);
  }

  /**
   * Drop a component if exists
   *
   * @return void
   */
  private drop() {
    if (this.componentRef) {
      this.appRef.detachView(this.componentRef.hostView);
      this.componentRef.destroy();
    }
  }


  ngOnDestroy() {
  }
}
