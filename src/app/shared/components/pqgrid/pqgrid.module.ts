import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PqgridComponent} from './pqgrid/pqgrid.component';


@NgModule({
  declarations: [PqgridComponent],
  imports: [
    CommonModule
  ],
  exports: [PqgridComponent]
})
export class PqgridModule {
}
