import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PqgridComponent } from './pqgrid.component';

describe('PqgridComponent', () => {
  let component: PqgridComponent;
  let fixture: ComponentFixture<PqgridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PqgridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PqgridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
