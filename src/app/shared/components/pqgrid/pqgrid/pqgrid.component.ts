import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';
import pq from '../../../../../assets/paramquery';
import {TranslateService} from '@ngx-translate/core';
import {Status} from '../../../enums/status';
import {CurrentUserService} from '../../../../auth/current-user.service';
import {NepaliDateConverter} from '../../../directives/date-picker2/nepali-date-picker.service';
import {replacementRules, unicodeMapping} from '../../../directives/nepali-unicode/mapping';

@Component({
  selector: 'app-pqgrid',
  templateUrl: './pqgrid.component.html',
  styleUrls: ['./pqgrid.component.scss'],
})
export class PqgridComponent implements OnChanges, AfterViewInit {
  @ViewChild('pqgrid') div: ElementRef;
  @Input() columns: any;
  @Input() data: any;
  @Input() title: any;
  @Input() height: any = 'flex';
  @Input() searchBox: boolean = false;
  @Input() allowRowSelect: boolean;
  @Input() pagination: boolean = true;
  @Input() virtualScroll: boolean = false;
  @Input() delayTimer: boolean = false;
  @Input() scrollEvent: boolean = false;
  @Input() export: boolean = false;
  @Input() tempSelectedRowIndx: number;
  @Input() paginationValue: any;
  @Input() remotePaging?: boolean;
  @Input() remoteUrl?: string;
  @Output() selectedRowData = new EventEmitter();
  @Output() editorPressData = new EventEmitter();
  @Output() checkBoxHeaderClicked = new EventEmitter();
  @Input() rowDblClick: boolean = false;
  @Output() rowDoubleClicked = new EventEmitter();
  @Output() paginatedData = new EventEmitter();
  @Output() virtualScrolledData = new EventEmitter();

  options: pq.gridT.options;
  grid: pq.gridT.instance;

  remoteDataDetails: any;

  allowToClickToolBarBtnInRemotePaging: boolean = false;
  exportClicked: boolean = false;
  printClicked: boolean = false;


  constructor(private translate: TranslateService,
              private nepaliDateConverter: NepaliDateConverter,
              private currentUser: CurrentUserService) {
  }

  private optionSet() {
    this.options = {
      width: 'auto',
      scrollModel: {autoFit: true},
      resizable: true,
      reactive: true,
      locale: 'en',
      height: this.height ? this.height : 'flex',
      wrap: false,
      hwrap: false,
      rowHt: 28,
      columnBorders: true,
      trackModel: {on: true},
      numberCell: {title: this.translate.instant('sn'), show: true, width: 40},
      columnTemplate: {width: 100},
      dataModel: {data: this.data},
      colModel: this.columns,
      postRenderInterval: -1, // synchronous post rendering.
      animModel: {on: true},
      filterModel: {on: true, header: true, type: 'local', menuIcon: false},
      sortModel: {on: true, type: 'local'},
      collapsible: {on: false, toggle: false, collapsed: false},
      selectionModel: {type: 'cell'},
      showBottom: true,
      strNoRows: this.translate.instant('noRows'),
      hoverMode: 'row',
      showTop: true,
      showTitle: false,

      menuIcon: true, // to hide and show columns
      menuUI: {
        tabs: ['hideCols']
      },
    };

    // Check Pagination
    this.checkPagination();

    this.setEditorPress();

    if (this.rowDblClick) {
      this.handleRowDoubleClick();
    }
    this.setFormulas();
    this.toolBarItem();

  }

  private createGrid() {
    this.grid = pq.grid(this.div.nativeElement, this.options);
  }

  ngOnChanges(obj: SimpleChanges) {
    if (obj.remoteUrl && !obj.remoteUrl.firstChange) {
      let timer = this.delayTimer ? 500 : 100;
      setTimeout(() => {
        this.remoteUrl = `${obj.remoteUrl.currentValue}`;
        this.options['pageModel'] = {curPage: 1};
        this.options.dataModel = this.getDataModel();
        return;
      }, timer);
    }
    if (obj.data) {
      if (this.remotePaging) {
        obj.data.firstChange = false;
      }
      if (!obj.data.firstChange) {
        if (obj.data.previousValue === undefined && !this.searchBox) {
          this.optionSet();
          if (this.allowRowSelect) {
            this.setRowSelect();
          }

          let timer = this.delayTimer ? 500 : 100;

          setTimeout(() => {
            this.createGrid();
          }, timer);
          if (this.allowRowSelect) {
            this.setSelectionModel();
          }
          //To focus edit button by default
          this.defaultFocus();


        } else {
          this.dataChange(obj.data.currentValue);

        }
      }

    }


    // if on key press reach to end of data then scroll automatically to find next data
    if (obj.scrollEvent) {
      if (!obj.scrollEvent.firstChange) {
        if (obj.scrollEvent.currentValue) {
          let table: any = this.div.nativeElement.getElementsByClassName('pq-cont-right');
          table[1].scrollTop = table[1].scrollTop + table[1].offsetHeight;
          setTimeout(() => {
            let indexNo = this.tempSelectedRowIndx + 1;
            let editButton: any = document.getElementsByClassName('index_' + indexNo);
            if (editButton) {
              try {
                editButton[0].focus();
                editButton[0].click();
              } catch (e) {
              }
            }
          }, 100);
        }
      }
    }
  }

  dataChange(val: object[]) {
    try {
      if (this.delayTimer) {
        const timer = 500;
        setTimeout(() => {
          this.options.dataModel.data = val;
        }, timer);
      } else {
        this.options.dataModel.data = val;
      }

      this.defaultFocus();
    } catch (e) {
    }
  }

  ngAfterViewInit() {
    this.setupForSearchDialog();
  }

  /**
   * if pqgrid table is search dialog
   * */
  setupForSearchDialog() {
    if (this.searchBox) {
      this.optionSet();
      this.setRowSelect();
      if (this.delayTimer) {
        setTimeout(() => {
          this.createGrid();
        }, 500);
      } else {
        this.createGrid();
      }
      this.setSelectionModel();
    }
  }

  setRowSelect() {
    let that = this;
    this.options['rowSelect'] = function(evt, ui) {
      if (ui['addList'].length > 0) {
        that.selectedRowData.emit(ui['addList'][0]['rowData']);
      }
    };
  }

  setSelectionModel() {
    this.options['selectionModel'] = {type: 'row'};
  }

  setEditorPress() {
    let that = this;
    this.options['change'] = function(evt, ui) {
      that.editorPressData.emit(ui['updateList'][0]['rowData']);
    };
  }


  handleRowDoubleClick() {
    this.options['rowDblClick'] = (evt, ui) => {
      this.rowDoubleClicked.emit(ui);
    };
  }

  defaultFocus() {
    let timer = this.delayTimer ? 500 : 100;
    setTimeout(() => {
      // To focus edit button by default
      let editButton = this.div.nativeElement.getElementsByClassName('edit_btn')[0];
      if (editButton) {
        editButton.focus();
        if (this.delayTimer) {
          editButton.click();
        }
      } else {
        let viewButton = this.div.nativeElement.getElementsByClassName('view_btn')[0];
        if (viewButton) {
          viewButton.focus();
          if (this.delayTimer) {
            viewButton.click();
          }
        }
      }
    }, timer);

  }

  /**
   * Uc Words Sentence JS
   * @param str
   */
  public ucwords(str: string): string {
    let pattern = new RegExp('(?<= )[^\\s]|^.', 'g');
    return str.toLowerCase().replace(pattern, (a) => a.toUpperCase());
  }

  getDataModel(): any {
    if (this.remoteUrl) {
      let data = {
        dataType: 'JSON',
        location: 'remote',
        method: 'GET',
        data: 'content',
        url: this.remoteUrl,
        postData: (el) => {
          if (el.dataModel.data.length == 0) {
            return {
              pq_curpage: 1
            };
          }
        },
        getData: (dataJSON, textStatus, jqXHR) => {
          this.paginatedData.emit(dataJSON);
          this.remoteDataDetails = dataJSON;
          if (dataJSON.data.releasedamountlist) {
            return {
              totalRecords: dataJSON.data.releasedamountlist.totalElements,
              data: dataJSON.data.releasedamountlist.content
            };
          } else if (dataJSON.data.data) {
            return {totalRecords: dataJSON.data.totalRecords, data: dataJSON.data.content};
          }
          this.remoteDataDetails = dataJSON.data.totalPages;

          // logic to allow export in case of remotePaging
          this.allowToClickToolBarBtnInRemotePaging = dataJSON.data.totalPages == 1;
          this.grid.enable();

          this.exportClickedEvent();
          this.printClickedEvent();

          return {
            totalRecords: dataJSON.data.totalElements,
            data: dataJSON.data.content,
            totalPages: dataJSON.data.totalPages
          };

        }
      };
      return data;
    }
  }

  exportClickedEvent() {
    if (this.exportClicked) {
      let btn_export: any = document.getElementsByClassName('btn-export');
      setTimeout(() => {
        btn_export[0].focus();
        btn_export[0].click();
        this.exportClicked = false;
      }, 500);
    }
  }

  printClickedEvent() {
    if (this.printClicked) {
      let btn_print: any = document.getElementsByClassName('btn-print');
      setTimeout(() => {
        btn_print[0].focus();
        btn_print[0].click();
        this.printClicked = false;
      }, 500);
    }
  }

  pqVirtualScroll() {
    if (this.remoteUrl) {
      let that = this;
      let pqVS = {
        rpp: this.paginationValue ? this.paginationValue : 20, //records per page.
        totalRecords: 0,
        requestPage: 1, //begins from 1.
        data: []
      };
      this.options['beforeTableView'] = function(evt, ui) {
        var initV = ui.initV,
          finalV = ui.finalV,
          data = ui.pageData,
          rpp = pqVS.rpp,
          requestPage;
        if (initV != null) {
          //if records to be displayed in viewport are not present in local cache,
          //then fetch them from remote database/server.

          if (data[initV] && data[initV].pq_empty) {
            requestPage = Math.floor(initV / rpp) + 1;
          } else if (data[finalV] && data[finalV].pq_empty) {
            requestPage = Math.floor(finalV / rpp) + 1;
          }
          if (requestPage >= 1) {
            if (pqVS.requestPage != requestPage) {

              pqVS.requestPage = requestPage;

              //initiate remote request.
              this.refreshDataAndView();
            }
          }
        }
      };
      return {
        dataType: 'JSON',
        location: 'remote',
        url: this.remoteUrl,
        postData: function() {
          return {
            pq_curpage: pqVS.requestPage,
            pq_rpp: pqVS.rpp
          };
        },
        getData: function(response) {
          var data = response.data.content,
            totalRecords = response.data.totalElements,
            len = data.length,
            curPage = response.data.pageable['pageNumber'] + 1,
            pq_data = pqVS.data,
            init = (curPage - 1) * pqVS.rpp;

          if (!pqVS.totalRecords) {
            //first time initialize the rows.
            for (var i = len; i < totalRecords; i++) {
              pq_data[i + init] = {pq_empty: true};
            }
            pqVS.totalRecords = totalRecords;
          }
          for (var j = 0; j < len; j++) {
            pq_data[j + init] = data[j];
            pq_data[j + init].pq_empty = false;
          }
          that.virtualScrolledData.emit(pq_data);
          return {data: pq_data};
        }
      };
    }
  }

  checkPagination() {
    if (this.pagination && !this.remotePaging) {
      this.options['pageModel'] = {
        type: 'local',
        rPP: this.paginationValue ? this.paginationValue : 20,
        strRpp: '{0}',
        strDisplay: `${this.translate.instant('strDisplay')}`,
        strPage: `${this.translate.instant('strPage')}`,
        layout: ['first', 'prev', 'next', 'last',
          '|', 'strRpp', '|', 'strPage', '|', 'strDisplay',
        ],
        rPPOptions: [10, 20, 50, 100, 500, 1000, 10000, 100000, 1000000],
      };
    } else if (this.pagination && this.remotePaging) {
      this.options.dataModel = this.getDataModel();
      this.options.dataModel['beforeSend'] = (jqXHR, settings) => {
        let token = localStorage.getItem('access_token');
        jqXHR.setRequestHeader('Authorization', `Bearer ${token}`);
      };
      this.options['pageModel'] = {
        type: 'remote',
        rPP: this.paginationValue ? this.paginationValue : 20,
        strRpp: '{0}',
        strDisplay: `${this.translate.instant('strDisplay')}`,
        strPage: `${this.translate.instant('strPage')}`,
        layout: ['first', 'prev', 'next', 'last',
          '|', 'strRpp', '|', 'strPage', '|', 'strDisplay',
        ],
        rPPOptions: [10, 20, 50, 100, 500, 1000, 10000, 100000, 1000000],
      };

      // Add Remote Filtering
      this.options['filterModel']['type'] = 'remote';

      // Add Remote Sorting
      this.options['sortModel']['type'] = 'remote';

    } else if (this.virtualScroll && this.remotePaging) {
      this.options.dataModel = this.pqVirtualScroll();
      this.options.dataModel['beforeSend'] = (jqXHR, settings) => {
        let token = localStorage.getItem('access_token');
        jqXHR.setRequestHeader('Authorization', `Bearer ${token}`);
      };
    } else {
      this.options['pageModel'] = {type: ''};
    }
  }

  /**
   * export,print button in every local and server side grid
   */
  toolBarItem() {
    // not to display export button on searchbox grid
    if (this.searchBox) {
      return;
    }
    if (!this.remotePaging) {
      this.allowToClickToolBarBtnInRemotePaging = true;
    }

    let that = this;
    this.options['showToolbar'] = true;
    this.options['toolbar'] = {
      items: [
        {
          type: 'button',
          cls: 'btn-export',
          label: this.translate.instant('export'),
          icon: 'ui-icon-arrowthickstop-1-s',
          listener: function(ui) {
            if (!that.allowToClickToolBarBtnInRemotePaging) {
              that.exportClicked = true;
              that.onrPPOptionChange();
              return;
            }
            var format = 'csv';
            let blob = this.exportData({
              format: format,
              nopqdata: true, //applicable for JSON export.
              render: false,
            });
            if (typeof blob === 'string') {
              blob = new Blob([blob]);
            }
            var a = document.createElement('a');
            a.href = window.URL.createObjectURL(blob);
            that.title = that.currentUser.getCurrentMenu()['menuname'];
            a.download = that.title + '.' + format;
            a.click();
          }
        }, {
          type: 'button',
          cls: 'btn-print',
          icon: 'ui-icon-print',
          label: this.translate.instant('print'),
          listener: function() {
            if (!that.allowToClickToolBarBtnInRemotePaging) {
              that.printClicked = true;
              that.onrPPOptionChange();
              return;
            }
            var exportHtml = this.exportData({
                title: that.currentUser.getCurrentMenu()['menuname'],
                format: 'htm',
                render: false
              }),
              newWin = window.open('', '', 'width=1200, height=700'),
              doc = newWin.document.open();
            doc.write(exportHtml);
            doc.close();
            newWin.print();
          }
        }]
    };
  }

  onrPPOptionChange() {
    this.grid.disable();
    let pq_page_select: any = document.getElementsByClassName('pq-page-select');
    pq_page_select[0].value = this.options['pageModel']['rPPOptions'].slice(-1);
    pq_page_select[0].dispatchEvent(new Event('change'));
  }

  setFormulas() {
    this.options['formulas'] = [
      // status handle
      // ['status', (rd) => {
      //   if (isNaN(rd['status'])) {
      //     return rd['status'];
      //   } else {
      //     if (Status[rd['status']] == null) {
      //       return;
      //     }
      //     return this.ucwords(
      //       Status[rd['status']].toString().replace(/_/g, ' ')
      //     );
      //
      //   }
      // }],
      ['startdate', (rd) => {
        if (this.translate.currentLang == 'np') {
          return this.convertToNepaliText(this.nepaliDateConverter.ADtoBS(rd['startdate']));
        } else {
          return rd['startdate'];
        }
      }],
      ['enddate', (rd) => {
        if (this.translate.currentLang == 'np') {
          return this.convertToNepaliText(this.nepaliDateConverter.ADtoBS(rd['enddate']));
        } else {
          return rd['enddate'];
        }
      }],
      ['applicationStartDate', (rd) => {
        if (this.translate.currentLang == 'np') {
          return this.convertToNepaliText(this.nepaliDateConverter.ADtoBS(rd['applicationStartDate']));
        } else {
          return rd['applicationStartDate'];
        }
      }],
      ['entrydate', (rd) => {
        if (this.translate.currentLang == 'np' && rd['entrydate']) {
          return this.convertToNepaliText(this.nepaliDateConverter.ADtoBS(rd['entrydate']));
        } else {
          return rd['entrydate'];
        }
      }],
      ['createddate', (rd) => {
        if (this.translate.currentLang == 'np') {
          return this.convertToNepaliText(this.nepaliDateConverter.ADtoBS(rd['createddate']));
        } else {
          return rd['createddate'];
        }
      }],
      ['effectDate', (rd) => {
        if (this.translate.currentLang == 'np') {
          return this.convertToNepaliText(this.nepaliDateConverter.ADtoBS(rd['effectDate']));
        } else {
          return rd['effectDate'];
        }
      }],
      ['effectTillDate', (rd) => {
        if (this.translate.currentLang == 'np' && rd['effectTillDate']) {
          return this.convertToNepaliText(this.nepaliDateConverter.ADtoBS(rd['effectTillDate']));
        } else {
          return rd['effectTillDate'];
        }
      }],
      ['grantedDate', (rd) => {
        if (this.translate.currentLang == 'np' && rd['grantedDate']) {
          return this.convertToNepaliText(this.nepaliDateConverter.ADtoBS(rd['grantedDate']));
        } else {
          return rd['grantedDate'];
        }
      }],
      ['agreementDate', (rd) => {
        if (this.translate.currentLang == 'np' && rd['agreementDate']) {
          return this.convertToNepaliText(this.nepaliDateConverter.ADtoBS(rd['agreementDate']));
        } else {
          return rd['agreementDate'];
        }
      }],
      ['isviewed', (rd) => {
        if (rd['isviewed']) {
          return 'Yes';
        } else {
          return 'No';
        }
      }]

    ];

  }

  /**
   * Function that converts passed input text string into nepali text
   * @param value
   */
  public convertToNepaliText(value: string): string {
    var mapObj = {
      0: '०',
      1: '१',
      2: '२',
      3: '३',
      4: '४',
      5: '५',
      6: '६',
      7: '७',
      8: '८',
      9: '९'
    };
    value = value.toString().replace(/0|1|2|3|4|5|6|7|8|9/gi, function(matched) {
      return mapObj[matched];
    });

    return value;
  }


}
