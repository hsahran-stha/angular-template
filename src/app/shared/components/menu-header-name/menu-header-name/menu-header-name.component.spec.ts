import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuHeaderNameComponent } from './menu-header-name.component';

describe('MenuHeaderNameComponent', () => {
  let component: MenuHeaderNameComponent;
  let fixture: ComponentFixture<MenuHeaderNameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuHeaderNameComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuHeaderNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
