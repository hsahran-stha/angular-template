import {Component, Input, OnInit} from '@angular/core';
import {CurrentUserService} from '../../../../auth/current-user.service';
import {TitleService} from '../../../service/title.service';

@Component({
  selector: 'menu-header-name',
  templateUrl: './menu-header-name.component.html',
  styleUrls: ['./menu-header-name.component.scss']
})
export class MenuHeaderNameComponent implements OnInit {
  @Input() heading: string;

  constructor(public currentUser: CurrentUserService, public titleService: TitleService) {
  }

  ngOnInit(): void {
  }

}
