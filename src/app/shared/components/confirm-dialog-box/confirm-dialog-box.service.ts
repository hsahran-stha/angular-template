import {
  EmbeddedViewRef, Injectable, ComponentFactoryResolver,
  ComponentRef, ApplicationRef, Injector, Input
} from '@angular/core';
import {ConfirmDialogBoxComponent} from './confirm-dialog-box/confirm-dialog-box.component';
import {takeUntil} from 'rxjs/operators';
import {componentDestroyed} from '../../../core/takeUntil-function';
import {Observable, Subject} from 'rxjs';
import {Status} from '../../enums/status';


@Injectable({
  providedIn: 'root'
})
export class ConfirmDialogBoxService {

  private componentRef: ComponentRef<ConfirmDialogBoxComponent>;
  private processConfirmation = new Subject();

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
    private injector: Injector,
  ) {
  }

  show(obj) {
    this.drop();
    let componentFactory = this.componentFactoryResolver.resolveComponentFactory(ConfirmDialogBoxComponent);
    this.componentRef = componentFactory.create(this.injector);

    this.appRef.attachView(this.componentRef.hostView);

    const instance = this.componentRef.instance;

    instance.title = obj.title;
    instance.body = obj.body;
    instance.alertClass = obj.alertClass;
    instance.alertClass2 = obj.alertClass2;
    instance.btnClass = obj.btnClass;
    instance.buttonName = obj.buttonName;
    instance.showRemarks = obj.status == Status.ACTIVE || obj.status == Status.BACK_FOR_REVIEW;

    this.componentRef.instance.cancelBtn.pipe(takeUntil(componentDestroyed(this))).subscribe(
      (data: boolean) => {
        this.setProcessConfirmation(false);
        this.drop();
      }
    );

    this.componentRef.instance.confirmBtn.pipe(takeUntil(componentDestroyed(this))).subscribe(
      (data: object | boolean) => {
        this.setProcessConfirmation(data);
        this.drop();
      }
    );

    const domElem = (this.componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;

    document.body.appendChild(domElem);

  }

  /**
   * Drop a component if exists
   *
   * @return void
   */
  private drop() {
    if (this.componentRef) {
      this.appRef.detachView(this.componentRef.hostView);
      this.componentRef.destroy();
    }
  }

// getter and setter of confirmation
  public getProcessConfirmation(): Observable<any> {
    return this.processConfirmation.asObservable();
  }

  private setProcessConfirmation(val) {
    this.processConfirmation.next(val);
  }

  ngOnDestroy() {
  }
}
