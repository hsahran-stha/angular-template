import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConfirmDialogBoxComponent} from './confirm-dialog-box/confirm-dialog-box.component';
import {TranslateModule} from '@ngx-translate/core';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [ConfirmDialogBoxComponent],
  imports: [
    CommonModule, TranslateModule, ReactiveFormsModule
  ],
  exports: [ConfirmDialogBoxComponent]
})
export class ConfirmDialogBoxModule {
}
