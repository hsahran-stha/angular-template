import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-confirm-dialog-box',
  templateUrl: './confirm-dialog-box.component.html',
  styleUrls: ['./confirm-dialog-box.component.scss']
})
export class ConfirmDialogBoxComponent implements OnInit, OnDestroy, AfterViewInit {


  @Input() title: string;
  @Input() body: string;
  @Input() alertClass: string;
  @Input() alertClass2: string;
  @Input() btnClass: string;
  @Input() buttonName: string;
  @Input() showRemarks: boolean;

  @Output() confirmBtn: EventEmitter<boolean> = new EventEmitter();
  @Output() cancelBtn: EventEmitter<boolean> = new EventEmitter();
  @Output() close: EventEmitter<boolean> = new EventEmitter();

  public approvalForm: FormGroup;

  constructor(private translate: TranslateService, private formBuilder: FormBuilder) {
  }


  ngOnInit() {
    this.approvalForm = this.formBuilder.group({
      remarks: ['', Validators.required]
    });
    if (!this.showRemarks) {
      this.approvalForm.get('remarks').clearValidators();
      this.approvalForm.get('remarks').updateValueAndValidity();
    }

    this.alertClass2 = this.alertClass == 'ic-save' ? 'text-blue' : 'text-red';

    if (this.buttonName == this.ucwords(this.translate.instant('approve'))) {
      this.btnClass = 'btn-success';
      this.alertClass2 = 'text-success';
    } else if (this.buttonName == this.ucwords(this.translate.instant('backForReview'))) {
      this.btnClass = 'btn-primary';
      this.alertClass2 = 'text-primary';
    } else if (this.buttonName == this.ucwords(this.translate.instant('rejected'))) {
      this.btnClass = 'btn-danger';
      this.alertClass2 = 'text-danger';
    }
  }

  ngAfterViewInit() {
    this.focusTextArea();
  }


  confirmBtnClick() {

    if (this.approvalForm.invalid) {
      this.focusTextArea();
      return;
    }
    const data = this.showRemarks ? this.approvalForm.value : true;
    this.confirmBtn.emit(data);
  }

  dismiss() {
    this.cancelBtn.emit(false);
  }

  cancelBtnClick() {
    this.cancelBtn.emit(false);
  }

  ngOnDestroy() {
  }

  /**
   * Uc Words Sentence JS
   * @param str
   */
  public ucwords(str: string): string {
    let pattern = new RegExp('(?<= )[^\\s]|^.', 'g');
    return str.toLowerCase().replace(pattern, (a) => a.toUpperCase());
  }

  /**
   * Fxn to check form control validity
   * @param controlToCheck : Form Control to validate
   * @param form : Form to validate
   */
  public checkFormControlInvalid(controlToCheck: any): boolean {
    let field = this.approvalForm.get(controlToCheck);
    return field != null && field.invalid && (field.touched || field.dirty);
  }

  /**
   * focus textarea
   */
  focusTextArea(): void {
    const remarks: any = document.getElementById('remarks');
    if (remarks) {
      document.getElementById('remarks').focus();
    }
  }

}
