/**
 * Office Type
 */
export enum OfficeType {
  HEAD = 1,
  SUBJECTIVE = 2,
  LOCAL = 3,
}
