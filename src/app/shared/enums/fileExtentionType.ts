export enum fileExtensionType {
  PNG = 'png',
  JPEG = 'jpeg',
  JPG = 'jpg',
  GIF = 'gif',
  DOCX = 'docx',
  DOC = 'doc',
  PDF = 'pdf',
  PPT = 'ppt',
  PPTX = 'pptX',
  XLSX = 'xlsx',
  XLS = 'xls',
  ICO = 'ico',
}
