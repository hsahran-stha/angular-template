/**
 * Status
 */
export enum Status {
  ALL = -2,
  DRAFT = -1,
  NEW = 0,
  ACTIVE = 1,
  INACTIVE = 2,
  BACK_FOR_REVIEW = 3,
  REJECTED = 4,
  PENDING = 5,
}
