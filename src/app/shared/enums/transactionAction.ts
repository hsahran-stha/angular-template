export enum TransactionAction {
  ADD = 1,
  EDIT = 2,
  DELETE = 3,
  CHANGE = 4,
  VIEW = 5
}
