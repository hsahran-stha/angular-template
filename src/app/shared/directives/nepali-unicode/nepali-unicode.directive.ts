import {Directive, ElementRef, HostListener, Input} from '@angular/core';
import {FormControl, NgControl} from '@angular/forms';
import {replacementRules, unicodeMapping} from './mapping';

@Directive({
  selector: '[nepaliUnicode]'
})
/**
 * Directive for nepali unicode
 */
export class NepaliUnicodeDirective {

  /**
   * Default Constructor
   * @param elementRef
   * @param formControl
   */
  constructor(private elementRef: ElementRef,  private formControl : NgControl) { }

  /**
   * Handle element input element
   */
  @HostListener('input',['$event']) onEvent($event){
    //Input text on element
    const inputText: string =  this.elementRef.nativeElement.value;
    if(!inputText)
      return;
    //Convert into Nepali unicode
    const convertedValue: string = this.convertToUnicode(inputText);
    //Set Converted value
    this.elementRef.nativeElement.value = convertedValue;
    //Set Form Control Value
    this.formControl.control.setValue(convertedValue);
  }

  /**
   * Function that converts passed input text string into nepali unicode text
   * @param inputText Input Text
   */
  public convertToUnicode(inputText: string): string {
    //Converted text to be returned
    let outputText: string;
    //Map each input text character
    outputText = inputText.toString().split('').map((key) => {
      return unicodeMapping[key] || key;
    }).join('');
    //Replace converted values
    replacementRules.forEach((rule) => {
      outputText = outputText.replace(new RegExp(rule[0], 'g'), rule[1]);
    });
    return outputText;
  }
}
