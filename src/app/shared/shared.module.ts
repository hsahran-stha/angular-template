import {NepaliUnicodeDirective} from './directives/nepali-unicode/nepali-unicode.directive';
import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PqgridModule} from './components/pqgrid/pqgrid.module';
import {ButtonsModule} from './components/buttons/buttons.module';
import {InputDirective} from './directives/input.directive';
import {appDatePickerDirective} from './directives/date-picker2/date-picker.directive';
import {TranslateModule} from '@ngx-translate/core';
import {ConfirmDeletionModule} from './components/confirm-deletion/confirm-deletion.module';
import {NgxSpinnerModule} from 'ngx-spinner';
import {ConfirmDialogBoxModule} from './components/confirm-dialog-box/confirm-dialog-box.module';
import {MenuHeaderNameComponent} from './components/menu-header-name/menu-header-name/menu-header-name.component';
import {EnglocalPipe} from './pipes/englocal.pipe';
import {EngToNepNumberPipe} from './pipes/engToNepNumbers/eng-to-nep-numbers.pipe';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';
import {DocumentUploadModule} from './components/document-upload/document-upload.module';


@NgModule({
  declarations: [
    InputDirective,
    NepaliUnicodeDirective,
    appDatePickerDirective,
    MenuHeaderNameComponent,
    EnglocalPipe,
    EngToNepNumberPipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PqgridModule,
    ButtonsModule,
    TranslateModule,
    ConfirmDialogBoxModule,
    ConfirmDeletionModule,
    NgxSpinnerModule,
    AngularMultiSelectModule,
    DocumentUploadModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    PqgridModule,
    ButtonsModule,
    InputDirective, ConfirmDialogBoxModule,
    appDatePickerDirective,
    NepaliUnicodeDirective,
    TranslateModule,
    ConfirmDeletionModule,
    NgxSpinnerModule,
    MenuHeaderNameComponent,
    EnglocalPipe,
    EngToNepNumberPipe,
    AngularMultiSelectModule,
    DocumentUploadModule
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders<any> {
    return {
      ngModule: SharedModule,
      providers: [],
    };
  }
}
