import { Pipe, PipeTransform } from '@angular/core';
import { NumbersToWordsNpService } from './numbers-to-words-np.service'

@Pipe({
  name: 'numbersToWordsNp'
})
export class NumbersToWordsNpPipe implements PipeTransform {
  constructor(private numbersToWordsNpService: NumbersToWordsNpService) {

  }

  /**
   * Transformation to Nepali Words
   * @param value Value to be convert
   * @param format Format of converted word
   */
  transform(value: any, format: string = 'text'): string {
    console.log(value);

    //Return converted nepali words
    return this.numbersToWordsNpService.toWords(value, format);
  }


}
