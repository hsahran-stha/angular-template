import { TestBed } from '@angular/core/testing';

import { NumbersToWordsNpService } from './numbers-to-words-np.service';

describe('NumbersToWordsNpService', () => {
  let service: NumbersToWordsNpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NumbersToWordsNpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
