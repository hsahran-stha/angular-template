import { Injectable } from '@angular/core';
import { Converter } from './converter';

@Injectable({
  providedIn: 'root'
})
export class NumbersToWordsNpService {

  /**
   * Default Constructor
   */
  constructor() { }

  /**
   * Function that converts provided number into corresponding words in nepali
   * @param num
   */
  public toWords(num: number | string, format: string = 'text'): string {
    const convertor = new Converter(num, format);
    return convertor.returnWords();
  }
}
