import {Pipe, PipeTransform} from '@angular/core';

/**
 * Pipe to switch english and nepali values based on current langauage
 *
 * @export
 * @class EnglocalPipe
 * @implements {PipeTransform}
 */
@Pipe({
  name: 'englocal',
  pure: false
})
export class EnglocalPipe implements PipeTransform {

  /**
   *  Switches value based on current session langauge
   * @param engvalue English Value
   * @param locvalue Local Value
   */
  transform(engvalue: any, locvalue: any): any {
    var lang = localStorage.getItem('lang');
    return lang == 'en' ? engvalue : locvalue;
  }

}
