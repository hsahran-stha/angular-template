import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Title} from '@angular/platform-browser';
import {TitleService} from './shared/service/title.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'FTMS-frontend';


  constructor(private translate: TranslateService, private  titleService: TitleService) {
    this.titleService.init();
    /**
     * Load Language File Based on Session
     */
    const sess = localStorage.getItem('lang');
    if (sess == null) {
      translate.use('np');
    } else {
      translate.use(sess);
    }
  }

  public setTitle(newTitle: string) {
    this.titleService.init();
  }

}
