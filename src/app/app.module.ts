import {APP_INITIALIZER, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {ToastrModule} from 'ngx-toastr';
import {SharedModule} from './shared/shared.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {HttpInterceptorService} from './interceptor/http.interceptor';
import {HttpErrorInterceptorService} from './interceptor/http-error.interceptor';
import {AlertMessageComponent} from './shared/components/alert-message/alert-message/alert-message.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {InitializerService} from './initializer.service';
import {DocumentTypeService} from './featured/setup/document-type/document-type.service';
import {
  DocumentTypeAdapter,
  DocumentTypeFormAdapter
} from './featured/setup/document-type/model/document-type.adapter';
import {EffectsModule} from "@ngrx/effects";
import {StoreModule} from "@ngrx/store";
import {rootReducer} from "./core/reducer";

export function HttpLoaderFactory(http: HttpClient) {
  localStorage.setItem('lang', 'np');
  return new TranslateHttpLoader(http);
}

/**
 * Init Application On first load
 */
export function initApp(initializer: InitializerService) {
  return () => initializer.initApplication().then(status => {
    //Check Status if necessery
  });
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    EffectsModule.forRoot([]),
    StoreModule.forRoot(
      rootReducer
    ),

    TranslateModule.forRoot({
      defaultLanguage: 'np',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    SharedModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 4000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    }),
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initApp,
      multi: true,
      deps: [InitializerService],
    },
    /**
     * Enable Hash Routing
     */
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptorService,
      multi: true,
    },
    DocumentTypeService, DocumentTypeAdapter, DocumentTypeFormAdapter
  ],
  bootstrap: [AppComponent],
  entryComponents: [AlertMessageComponent],
})
export class AppModule {
}
