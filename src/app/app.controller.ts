import {Injector} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {PqgridService} from './shared/components/pqgrid/pqgrid.service';
import {ConfirmDialogBoxService} from './shared/components/confirm-dialog-box/confirm-dialog-box.service';
import {Location} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';
import {ConfirmDeletionService} from './shared/components/confirm-deletion/confirm-deletion.service';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Status} from './shared/enums/status';
import {AlertifyService} from './shared/service/alertifyjs/alertify.service';
import {CurrentUserService} from './auth/current-user.service';
import {InitializerService} from './initializer.service';
import {AuthService} from './auth/auth.service';
import {environment} from '../environments/environment';
import {FiscalYearsService} from './shared/service/init/fiscal-years.service';
import {NepaliDateConverter} from './shared/directives/date-picker2/nepali-date-picker.service';
import {OfficeType} from './shared/enums/office-type';
import {TransactionAction} from './shared/enums/transactionAction';
import {fileExtensionType} from './shared/enums/fileExtentionType';
import {replacementRules, unicodeMapping} from './shared/directives/nepali-unicode/mapping';
import {RootReducerState} from "./core/reducer";
import {Store} from "@ngrx/store";

declare var jQuery: any;

export class AppController {
  public environment = environment;

  public formBuilder: FormBuilder;
  public router: Router;
  public activatedRoute: ActivatedRoute;
  public pqGridService: PqgridService;
  public confirmDialogBoxService: ConfirmDialogBoxService;
  public confirmDeletionService: ConfirmDeletionService;
  public location: Location;
  public invalidControls = [];
  public translate: TranslateService;
  public modalService: NgbModal;
  public alertify: AlertifyService;
  public currentUser: CurrentUserService;
  public initializer: InitializerService;
  public authService: AuthService;
  public fiscalYearsService: FiscalYearsService;
  public nepaliDateConverter: NepaliDateConverter;

  public routeId: number;

  private closeResult: string = '';

  public status = [{id: Status.ACTIVE, name: Status[Status.ACTIVE]}, {id: Status.INACTIVE, name: Status[Status.INACTIVE]}];
  public officeType = OfficeType;

  public store: Store<RootReducerState>;
  constructor(public injector: Injector) {
    this.formBuilder = this.injector.get(FormBuilder);
    this.router = this.injector.get(Router);
    this.activatedRoute = this.injector.get(ActivatedRoute);
    this.pqGridService = this.injector.get(PqgridService);
    this.confirmDialogBoxService = this.injector.get(ConfirmDialogBoxService);
    this.confirmDeletionService = this.injector.get(ConfirmDeletionService);
    this.location = this.injector.get(Location);
    this.translate = this.injector.get(TranslateService);
    this.modalService = this.injector.get(NgbModal);

    this.alertify = this.injector.get(AlertifyService);
    this.currentUser = this.injector.get(CurrentUserService);
    this.initializer = this.injector.get(InitializerService);
    this.authService = this.injector.get(AuthService);
    this.fiscalYearsService = this.injector.get(FiscalYearsService);
    this.nepaliDateConverter = this.injector.get(NepaliDateConverter);
    this.store = this.injector.get(Store);
  }

  /**
   * Create Form Data from Object
   *
   * @param {Object} object
   * @param {FormData} [form]
   * @param {string} [namespace]
   * @returns {FormData}
   * @memberof RegistrationComponent
   */
  public createFormData(
    object: Object,
    form?: FormData,
    namespace?: string
  ): FormData {
    const formData = form || new FormData();
    for (let property in object) {
      if (!object.hasOwnProperty(property) || !object[property]) {
        continue;
      }
      const formKey = namespace ? `${namespace}.${property}` : property;
      if (object[property] instanceof Date) {
        formData.append(formKey, object[property].toISOString());
      } else if (
        typeof object[property] === 'object' &&
        !(object[property] instanceof File)
      ) {
        this.createFormData(object[property], formData, formKey);
      } else {
        formData.append(formKey, object[property]);
      }
    }
    return formData;
  }

  /**
   * To get all invalid controls from a form
   * @param formGroup : Name of the formGroup
   */
  public findInvalidControls(formGroup: FormGroup) {
    this.invalidControls = [];
    const controls = formGroup.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        this.invalidControls.push(name);
      }
    }
    return this.invalidControls;
  }

  /**
   * Fxn to check form control validity
   * @param controlToCheck : Form Control to validate
   * @param form : Form to validate
   */
  public checkFormControlInvalid(controlToCheck: any, form: any) {
    let field = form.get(controlToCheck);
    return field != null && field.invalid && (field.touched || field.dirty);
  }

  /**
   * Checks validity after form submit
   * Just loop over all the formControl by making dirty as true
   * @param currentForm
   */
  public validateAllFormFields(currentForm: FormGroup) {
    let firstInvalidElement: string = null;
    Object.keys(currentForm.controls).forEach((field) => {
      const control = currentForm.get(field);
      if (control.status == 'INVALID' && firstInvalidElement == null) {
        firstInvalidElement = field;
      }

      if (control instanceof FormControl) {
        control.markAsTouched({onlySelf: true});
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });

    if (firstInvalidElement) {
      const element: HTMLElement = document.querySelector(
        '[formControlName=\'' + firstInvalidElement + '\']'
      );
      if (element) {
        element.focus();
      }
    }
  }

  /**
   * to focus reactive form text field
   * @param inputField
   */
  focusFormControlById(inputField) {
    const element: HTMLElement = document.querySelector(
      '[id=\'' + inputField + '\']'
    );

    if (element) {
      element.focus();
    }
  }

  /**
   * Find Invalid fields in a form
   * @param currentForm
   */
  public displayInvalidFormControls(currentForm: FormGroup) {
    const invalid = [];
    const controls = currentForm.controls;
    for (const name in controls) {
      if (controls[name].status == 'INVALID') {
        invalid.push(name);
      }
    }
  }

  /**
   * fn. to go to previous page
   */
  public goToPreviousPage() {
    this.location.back();
  }

  /**
   * To manage each columns
   * */

  manageEachColumn(
    title,
    dataIndx,
    filter: boolean = false,
    sortable: boolean = true,
    dataType: string = 'string',
    action: any = false,
    that: any = [],
    gridConfig: object = {},
    align?
  ): Object {
    let self = this;
    let columns: Object = {
      title: this.translate.instant(title),
      dataType: dataType,
      dataIndx: dataIndx,
      editable: false,
      sortable: true, // sortable
      halign: 'center',
      align: align, // set header align center
    };

    // Float Type Handle
    if (dataType == 'float') {
      columns['format'] = '#.00';
    }

    // Date Type Handlep
    if (dataType == 'date') {
      columns['format'] = 'yy-mm-dd';
    }

    if (dataIndx == 'status') {
      columns['render'] = (ui) => {
        const status = Status[ui.rowData.status].toString().replace(/_/g, ' ');
        return this.translate.instant(status);
      };
    }

    if (filter) {
      if (dataIndx.includes('date')
        || dataIndx.includes('applicationStartDate')
        || dataIndx.includes('effectTillDate')
        || dataIndx.includes('effectDate')
        || dataIndx.includes('agreementDate')
        || dataIndx.includes('grantedDate')) {
        columns['filter'] = {
          attr: `placeholder='${self.translate.instant('date')}' readonly`,
          crules: [{condition: 'between'}],
          listeners: [
            {
              change: function(evt, ui) {
                console.log(ui);
                let filterRules = [
                  {
                    dataIndx: ui.dataIndx,
                    condition: 'between',
                    value: self.translate.currentLang == 'np' && ui.value ? self.convertToNepaliText(ui.value) : ui.value,
                    value2: self.translate.currentLang == 'np' && ui.value2 ? self.convertToNepaliText(ui.value2) : ui.value2,
                  },
                ];
                this.filter({
                  oper: 'replace',
                  rules: filterRules,
                });
              },
            },
          ],
          init: function(ui: any) {
            const $cell = ui.$cell;
            const datePicker1: any = $cell[0].getElementsByTagName('input')[0];
            const datePicker2: any = $cell[0].getElementsByTagName('input')[1];

            datePicker1.setAttribute('placeholder', self.translate.instant('startDate'));

            const language = self.translate.currentLang == 'np' ? 'nepali' : 'gregorian';

            jQuery(datePicker1).calendarsPicker({
              calendar: jQuery.calendars.instance(language),
              onSelect: (dates) => {
                datePicker1.value = dates;
                datePicker1.dispatchEvent(new Event('change'));

              },
            });

            datePicker2.setAttribute('placeholder', self.translate.instant('endDate'));

            jQuery(datePicker2).calendarsPicker({
              calendar: jQuery.calendars.instance(language),
              onSelect: (dates) => {
                datePicker2.value = dates;
                datePicker2.dispatchEvent(new Event('change'));

              },
            });
          }
        };
      } else if (dataIndx == 'status') {
        let options = [
          self.translate.instant(Status[Status.ACTIVE].toString().replace(/_/g, ' ')),
          self.translate.instant(Status[Status.INACTIVE].toString().replace(/_/g, ' ')),
          self.translate.instant(Status[Status.DRAFT].toString().replace(/_/g, ' ')),
          self.translate.instant(Status[Status.BACK_FOR_REVIEW].toString().replace(/_/g, ' ')),
        ];

        columns['filter'] = {
          attr: `placeholder='${this.translate.instant('click')}...' readonly`,
          options: options,
          crules: [{condition: 'equal'}],
          listeners: [
            {
              change: function(evt, ui) {
                if (ui.value == self.translate.instant(Status[Status.ACTIVE])) {
                  ui.value = Status.ACTIVE;
                } else if (ui.value == self.translate.instant(Status[Status.INACTIVE])) {
                  ui.value = Status.INACTIVE;
                } else if (ui.value == self.translate.instant(Status[Status.DRAFT])) {
                  ui.value = Status.DRAFT;
                } else if (ui.value == self.translate.instant(Status[Status.BACK_FOR_REVIEW])) {
                  ui.value = Status.BACK_FOR_REVIEW;
                }
                let filterRules = [
                  {
                    dataIndx: ui.dataIndx,
                    condition: 'contain',
                    value: ui.value,
                  },
                ];
                this.filter({
                  oper: 'replace',
                  rules: filterRules,
                });
              },
            },
          ],
          init: function(ui: any) {
            const $cell = ui.$cell;
            const checkedBox: any = $cell[0].getElementsByTagName('input')[0];
            checkedBox.dispatchEvent(new Event('change'));
          }
        };
      } else {
        columns['filter'] = {
          attr: `placeholder=\'${this.translate.instant('search')}...\' autocomplete=\'off\'`,
          crules: [{condition: 'contain'}],
          listeners: [
            {
              keyup: function(evt, ui) {
                if (!ui.value) {
                  return;
                }
                const local = dataIndx.includes('local') || dataIndx.includes('Local');
                evt.target.value = local && self.translate.currentLang == 'np' ? self.convertToUnicode(ui.value) : ui.value;
              },
              change: function(evt, ui) {
                console.log(ui.value);
                let filterRules = [
                  {
                    dataIndx: ui.dataIndx,
                    condition: 'contain',
                    value: ui.value,
                  },
                ];
                this.filter({
                  oper: 'replace',
                  rules: filterRules,
                });
              },
            },
          ],
          init: function(ui: any) {
            const $cell = ui.$cell;
            const searchBox: any = $cell[0].getElementsByTagName('input')[0];
            searchBox.type = 'search';
          }
        };
      }
    }

    if (action) {
      columns['width'] = 105; // Fixed width as per feedback
      columns['maxWidth'] = 105; // Fixed width as per feedback
      columns['render'] = (ui) => {
        return this.pqGridActionButtons({action: action, ui: ui});
      };
      columns['postRender'] = function(ui) {
        var grid = this,
          $cell = grid.getCell(ui);
        let selectedRow = ui.rowData.id;
        /***
         * view button
         * */
        $cell
          .find('.view_btn')
          .button({icons: ''})
          .off('click')
          .on('click', function(evt) {
            that.actionView(ui);
          });
        /***
         /***
         * edit button
         * */
        $cell
          .find('.edit_btn')
          .button({icons: ''})
          .off('click')
          .on('click', function(evt) {
            let pqTable = evt.target.closest('.pq-table').children;
            let lengthPqTable = pqTable.length;
            for (let i = 0; i < lengthPqTable; i++) {
              let previousRow = pqTable[i].classList;

              if (previousRow.contains('active')) {
                previousRow.remove('active');
              }
            }
            let currentRow = evt.target.closest('.pq-grid-row').classList;

            currentRow.add('active');
            that.actionEdit(ui);
          });
        /***
         * delete button
         * */
        $cell
          .find('.delete_btn')
          .button({icons: ''})
          .off('click')
          .on('click', function(evt) {
            that.actionDelete(ui);
          });
        /***
         * download button
         * */
        $cell
          .find('.download_btn')
          .button({icons: ''})
          .off('click')
          .on('click', function(evt) {
            that.actionDownload(ui);
          });
        /***
         * approve button
         * */
        $cell
          .find('.approve_btn')
          .button({icons: ''})
          .off('click')
          .on('click', function(evt) {
            that.actionApprove(ui);
          });
      };
    }

    if (Object.keys(gridConfig).length > 0) {
      Object.assign(columns, gridConfig);
    }

    return columns;
  }

  /**
   * buttons handler
   */
  pqGridActionButtons(data: object) {
    // hide button if row is deleted
    if (data['ui'].rowData.action == TransactionAction.DELETE) {
      return '';
    }


    // hide button if program is selected in FY program request
    if (data['ui'].rowData.fiscalyearprogramselectiondetailstatus == 1) {
      return '';
    }


    let view = '', edit = '', drop = '', download = '', approve = '';
    // delete button will be access if it has delete right and
    // here showing delete button in create or edit page because
    // document upload, and detail data upload need delete button though user does not have delete access
    if (this.currentUser.hasMenuActionPermission(environment.menuActions.delete) || this.getPage() == 'create' || this.getPage() == 'edit') {
      drop = '<button type="button" title="Delete" class="delete_btn ui-button-action"><i class="ic-close"></i></button>';
    }

    if (this.currentUser.hasMenuActionPermission(environment.menuActions.update)) {
      edit = `<button type="button" title="Edit" class="edit_btn ui-button-action"><i class="ic-edit"></i></button>`;
    }
    if (this.currentUser.hasMenuActionPermission(environment.menuActions.view)) {
      view = `<button type="button" title="View" class="view_btn ui-button-action"><i class="ic-visible"></i></button>`;
    }

    if (this.currentUser.hasMenuActionPermission(environment.menuActions.view)) {
      download = `<button type="button" title="Download" class="download_btn ui-button-action"><i class="ic-download"></i></button>`;
    }

    if (this.currentUser.hasMenuActionPermission(environment.menuActions.update)) {
      approve = `<button type="button" title="Approve" class="approve_btn ui-button-action"><i class="ic-checkmark-1"></i></button>`;
    }


    // hide view button if file is not image or pdf
    if (data['ui'].rowData.mimeType) {
      const ext = data['ui'].rowData.mimeType.split('/')[1];
      const isPdf: boolean = ext == fileExtensionType.PDF;
      const isImage: boolean = ext == fileExtensionType.JPEG || ext == fileExtensionType.JPG || ext == fileExtensionType.PNG || ext == fileExtensionType.GIF;
      view = isPdf || isImage ? view : '';
    }

    // three button case
    if (data['action'] == 'view-download-delete') {
      return view + download + drop;
    }
    // end three button case

    // two button case
    if (data['action'] == 'view-delete') {
      return view + drop;
    }
    if (data['action'] == 'view-edit') {
      return view + edit;
    }

    if (data['action'] == 'edit-delete') {
      return edit + drop;
    }

    if (data['action'] == 'view-download') {
      return view + download;
    }
    if (data['action'] == 'view-approve') {
      return view + approve;
    }
    // two button case end

    // only one button
    if (data['action'] == 'only-view') {
      return view;
    }

    if (data['action'] == 'only-edit') {
      return edit;
    }

    if (data['action'] == 'only-delete') {
      return drop;
    }
    // only one button case end

    return view + edit + drop;
  }


  open(content) {
    this.modalService
      .open(content, {
        ariaLabelledBy: 'modal-basic-title',
        windowClass: 'modal-bms',
      })
      .result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
      },
      (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  openLg(contentlg, size?) {
    this.modalService
      .open(contentlg, {
        ariaLabelledBy: 'modal-basic-title',
        windowClass: 'modal-bms',
        size: size ? size : 'lg',

      })
      .result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
      },
      (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  public getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  /**
   * Uc Words Sentence JS
   * @param str
   */
  public ucwords(str: string): string {
    let pattern = new RegExp('(?<= )[^\\s]|^.', 'g');
    return str.toLowerCase().replace(pattern, (a) => a.toUpperCase());
  }

  /**
   * page name
   */
  getPage(): string {
    return this.router.url.split('/').slice(4, 5).toString();
  }


  /**
   * fn. to switch languages
   * @param lang : preferred language ('np'|'en')
   */
  useLanguage(lang) {
    this.translate.use(lang);
    localStorage.setItem('lang', lang);
    const prev = this.router.url;
    this.router.navigate(['/featured']).then(data => {
      this.router.navigate([prev]);
    });
  }


  /**
   * to toggle side bar
   * */
  toggled($event) {
    let classesName = $event.target.closest('.app').classList;
    if (classesName.length > 2) {
      classesName.remove('toggled');
    } else {
      classesName.add('toggled');
    }
    const prev = this.router.url;
    this.router.navigate(['/featured']).then(data => {
      this.router.navigate([prev]);
    });
  }


  /**
   * Function that converts passed input text string into nepali unicode text
   * @param inputText Input Text
   */
  public convertToUnicode(inputText: string): string {
    //Converted text to be returned
    let outputText: string;
    //Map each input text character
    outputText = inputText.toString().split('').map((key) => {
      return unicodeMapping[key] || key;
    }).join('');
    //Replace converted values
    replacementRules.forEach((rule) => {
      outputText = outputText.replace(new RegExp(rule[0], 'g'), rule[1]);
    });
    return outputText;
  }

  /**
   * Function that converts passed input text string into nepali text
   * @param value
   */
  public convertToNepaliText(value: string): string {
    var mapObj = {
      0: '०',
      1: '१',
      2: '२',
      3: '३',
      4: '४',
      5: '५',
      6: '६',
      7: '७',
      8: '८',
      9: '९'
    };
    value = value.toString().replace(/0|1|2|3|4|5|6|7|8|9/gi, function(matched) {
      return mapObj[matched];
    });

    return value;
  }


}
