import {Injectable} from '@angular/core';
import {AuthService} from '../auth/auth.service';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent, HttpResponse,
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';
import {AlertifyService} from '../shared/service/alertifyjs/alertify.service';
import {AlertMessageService} from '../shared/components/alert-message/alert-message.service';

@Injectable({
  providedIn: 'root',
})
export class HttpErrorInterceptorService {
  /**
   *Error Exclude Paths
   */
  private excludeURLS: Array<string> = [
    `${environment.API_URL}oauth/token`,
  ];

  constructor(
    private authService: AuthService,
    private router: Router,
    private alertify: AlertifyService,
    private alertService: AlertMessageService) {
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          console.log(event.body.status == false);
          // console.log(event.body.data);
          if (event.body.status == false && event.body.data) {
            this.alertify.error(event.body.data);
            // this.alertService.show({message: event.body.data});
          }
        }
      }),
      catchError((err) => {
        if ([401, 403].indexOf(err.status) !== -1) {
          // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
          if (err.error.httpCode) {
            // if httpCode is preset, it means the action isn't authored for the current user
            // this.toast.error('You are not authorized for this action.');
            this.router.navigate(['/auth/login']);
            return;
          }
          this.authService.userLogout();
        }
        if ([400, 500].indexOf(err.status) !== -1) {
          if (!this.excludeURLS.includes(request.url)) {
            this.alertService.show(err.error);
          }
        }

        console.log(err);
        const error = err.error.message || err.error.error_description;
        return throwError(error);
      })
    );
  }
}
