import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent,} from '@angular/common/http';
import {Observable} from 'rxjs';
import {finalize} from 'rxjs/operators';
import {NgxSpinnerService} from 'ngx-spinner';
import {environment} from '../../environments/environment';
import {Router} from '@angular/router';
import {AuthService} from '../auth/auth.service';


@Injectable({
  providedIn: 'root',
})
export class HttpInterceptorService {

  /**
   * Access Token
   */
  private token: string;
  pendingRequestsCount = 0;

  /**
   *Token Exclude Paths
   */
  private excludeURLS: Array<string> = [
    `${environment.API_URL}oauth/token`,
  ];

  constructor(
    private authService: AuthService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {
  }

  /**
   * Appends JWT Token on each HttRequest
   * @param  {HttpRequest<any>} req
   * @param  {HttpHandler}      next
   * @return {Observable}
   */
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    /**
     * Fetch Access token
     */
    this.token = this.authService.getLoggedInToken();
    /**
     * Attach Token on Header if user already logged-in
     */

    if (!this.excludeURLS.includes(req.url) && this.authService.isUserLoggedIn()) {
      req = req.clone({
        setHeaders: {
          'accept-language': localStorage.getItem('lang'),
          'Authorization': `Bearer ${this.token}`,
        }
      });
    }
    // hide spinner if request for notification because every 10 second it will request for notifications
    const isNotification = req.url.includes('notification/getMyNotification');

    // Show Spinner
    this.pendingRequestsCount++;
    if (!isNotification) {
      this.spinner.show();
    }

    return next.handle(req).pipe(finalize(() => {
      this.pendingRequestsCount--;
      if (this.pendingRequestsCount < 1) {
        this.spinner.hide();
      }
    }));

  }
}
