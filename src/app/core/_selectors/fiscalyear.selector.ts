import {createSelector} from '@ngrx/store';
import {FiscalYearReducerState} from "../_reducers/fiscalyear.reducer";
import {RootReducerState} from "../reducer";

/**
 * selector
 */
export const getLoading = (state: FiscalYearReducerState) => state.loading;
export const getLoaded = (state: FiscalYearReducerState) => state.loaded;
export const getError = (state: FiscalYearReducerState) => state.error;
export const getData = (state: FiscalYearReducerState) => state.data;
export const getAData = (state: FiscalYearReducerState) => state.aData;

/**
 *
 * @param state
 * get state from root reducer
 */
export const getFiscalYearState = (state: RootReducerState) => state.fiscal_year;
export const getFiscalYearLoading = createSelector(getFiscalYearState, getLoading);
export const getFiscalYearLoaded = createSelector(getFiscalYearState, getLoaded);
export const getFiscalYearError = createSelector(getFiscalYearState, getError);
export const getFiscalYear = createSelector(getFiscalYearState, getData);
export const getAFiscalYear = createSelector(getFiscalYearState, getAData);
