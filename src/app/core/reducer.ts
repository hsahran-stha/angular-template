import {ActionReducerMap} from '@ngrx/store';
import {FiscalYearReducer, FiscalYearReducerState} from "./_reducers/fiscalyear.reducer";

export interface RootReducerState {
  fiscal_year: FiscalYearReducerState;
}


export const rootReducer: ActionReducerMap<RootReducerState> = {
  fiscal_year: FiscalYearReducer,
};
