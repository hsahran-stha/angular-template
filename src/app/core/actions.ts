import {Action as NgrxAction} from '@ngrx/store';

export interface SharedAction extends NgrxAction {
  payload?: any;
}
