import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {of} from 'rxjs';
import {FiscalYearModel} from "../../featured/setup/fiscal-year/model/fiscal-year.model";
import {
  FiscalYearRequestListAction,
  FiscalYearRequestListByIdAction, FiscalYearRequestListByIdFail,
  FiscalYearRequestListByIdSuccess,
  FiscalYearRequestListFail,
  FiscalYearRequestListSuccess,
  REQUEST_LIST_ACTION,
  REQUEST_LIST_BY_ID_ACTION
} from "../_actions/fiscalyear.action";
import {FiscalYearService} from "../../featured/setup/fiscal-year/fiscal-year.service";


@Injectable()
export class FiscalYearEffect {
  @Effect() loadData$ = this.actions$.pipe(
    ofType<FiscalYearRequestListAction>(REQUEST_LIST_ACTION),
    mergeMap(data => this.fiscalYearService.get().pipe(
      map((data: FiscalYearModel[]) => new FiscalYearRequestListSuccess({data})),
      catchError(() => of(new FiscalYearRequestListFail()))
      )
    )
  );

  @Effect() loadAData$ = this.actions$.pipe(
    ofType<FiscalYearRequestListByIdAction>(REQUEST_LIST_BY_ID_ACTION),
    mergeMap(data => this.fiscalYearService.getById(data.payload.id).pipe(
      map((data: FiscalYearModel) => new FiscalYearRequestListByIdSuccess({data})),
      catchError(() => of(new FiscalYearRequestListByIdFail()))
      )
    )
  );

  constructor(private actions$: Actions, private fiscalYearService: FiscalYearService) {
  }
}
