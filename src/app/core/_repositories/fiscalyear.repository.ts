import {FiscalYearRequestListAction} from "../_actions/fiscalyear.action";
import {RootReducerState} from "../reducer";
import {Store} from "@ngrx/store";
import {getFiscalYear} from "../_selectors/fiscalyear.selector";
import {catchError, map} from "rxjs/operators";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class FiscalYearRepository {
  constructor(public store: Store<RootReducerState>) {
  }

  public dispatch() {
    this.store.dispatch(new FiscalYearRequestListAction());
  }


  public getAll() {
    return new Promise((resolve, reject) => {
      this.store.select(getFiscalYear).pipe(
        map(data => {
          debugger;
          if (data) {
            resolve(data);
          } else {
            this.dispatch();
          }
        }),
        catchError(err => {
          reject(err);
          return err;
        })
      ).subscribe();
    })
  }
}
