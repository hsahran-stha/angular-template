import {FiscalYearModel} from "../../featured/setup/fiscal-year/model/fiscal-year.model";

export const REQUEST_LIST_ACTION = '{fiscal year} list_action';
export const REQUEST_LIST_SUCCESS = '{fiscal year} list_success';
export const REQUEST_LIST_FAIL = '{fiscal year} list_fail';

export const REQUEST_LIST_BY_ID_ACTION = '{fiscal year} list_by_id_action';
export const REQUEST_LIST_BY_ID_SUCCESS = '{fiscal year} list_by_id_success';
export const REQUEST_LIST_BY_ID_FAIL = '{fiscal year} list_by_id_fail';

export const REQUEST_CREATE_ACTION = '{fiscal year} create_action';
export const REQUEST_CREATE_SUCCESS = '{fiscal year} create_success';
export const REQUEST_CREATE_FAIL = '{fiscal year} create_fail';

export const REQUEST_DELETE_ACTION = '{fiscal year} delete_action';
export const REQUEST_DELETE_SUCCESS = '{fiscal year} delete_success';
export const REQUEST_DELETE_FAIL = '{fiscal year} delete_fail';

export const REQUEST_UPDATE_ACTION = '{fiscal year} update_action';
export const REQUEST_UPDATE_SUCCESS = '{fiscal year} update_success';
export const REQUEST_UPDATE_FAIL = '{fiscal year} update_fail';


export class FiscalYearRequestListAction {
  readonly type = REQUEST_LIST_ACTION;
}

export class FiscalYearRequestListSuccess {
  readonly type = REQUEST_LIST_SUCCESS;

  constructor(public payload?: { data: FiscalYearModel[] }) {
  }
}

export class FiscalYearRequestListFail {
  readonly type = REQUEST_LIST_FAIL;
}

export class FiscalYearRequestListByIdAction {
  readonly type = REQUEST_LIST_BY_ID_ACTION;

  constructor(public payload?: { id: number }) {
  }

}

export class FiscalYearRequestListByIdSuccess {
  readonly type = REQUEST_LIST_BY_ID_SUCCESS;

  constructor(public payload?: { data: FiscalYearModel }) {
  }
}

export class FiscalYearRequestListByIdFail {
  readonly type = REQUEST_LIST_BY_ID_FAIL;
}

export class FiscalYearCreateAction {
  readonly type = REQUEST_CREATE_ACTION;

  constructor(public payload?: { data: FiscalYearModel }) {
  }

}

export class FiscalYearCreateSuccess {
  readonly type = REQUEST_CREATE_SUCCESS;

  constructor(public payload?: object) {
  }
}

export class FiscalYearCreateFail {
  readonly type = REQUEST_CREATE_FAIL;

  constructor(public payload?: object) {
  }
}

export class FiscalYearDeleteAction {
  readonly type = REQUEST_DELETE_ACTION;

  constructor(public payload?: { id: number }) {
  }

}

export class FiscalYearDeleteSuccess {
  readonly type = REQUEST_DELETE_SUCCESS;

  constructor(public payload?: object) {
  }

}

export class FiscalYearDeleteFail {
  readonly type = REQUEST_DELETE_FAIL;

  constructor(public payload?: object) {
  }
}

export class FiscalYearUpdateAction {
  readonly type = REQUEST_UPDATE_ACTION;

  constructor(public payload?: { id: number, data: FiscalYearModel }) {
  }

}

export class FiscalYearUpdateSuccess {
  readonly type = REQUEST_UPDATE_SUCCESS;

  constructor(public payload?: object) {
  }

}

export class FiscalYearUpdateFail {
  readonly type = REQUEST_UPDATE_FAIL;

  constructor(public payload?: object) {
  }
}


