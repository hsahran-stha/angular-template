import {FiscalYearModel} from "../../featured/setup/fiscal-year/model/fiscal-year.model";
import {SharedAction} from "../actions";
import {REQUEST_LIST_ACTION, REQUEST_LIST_FAIL, REQUEST_LIST_SUCCESS} from "../_actions/fiscalyear.action";

export interface FiscalYearReducerState {
  loading: boolean;
  loaded: boolean;
  error: boolean;
  data: FiscalYearModel[] | undefined;
  aData: FiscalYearModel | object;
}


const initialState: FiscalYearReducerState = {
  loading: false,
  loaded: false,
  error: false,
  data: undefined,
  aData: {}
};

export function FiscalYearReducer(state = initialState, action: SharedAction): FiscalYearReducerState {
  switch (action.type) {
    case REQUEST_LIST_ACTION: {
      return {...state, loading: true};
    }
    case REQUEST_LIST_SUCCESS: {
      let data = action.payload.data;
      return {...state, loading: false, loaded: true, data};
    }
    case REQUEST_LIST_FAIL: {
      return {...state, error: true, loading: false};
    }

    default: {
      return state;
    }
  }
}

