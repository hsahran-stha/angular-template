import {Injectable, Injector} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {interval, throwError, of} from 'rxjs';
import {retryWhen, tap, flatMap} from 'rxjs/operators';
import {CurrentUserService} from './auth/current-user.service';
import {SettingsService} from './shared/service/setting.service';
import {LocalJsonService} from './shared/service/local-json.service';
import {FiscalYearsService} from './shared/service/init/fiscal-years.service';
import {Router} from '@angular/router';
import {environment} from '../environments/environment';


@Injectable({
  providedIn: 'root',
})
export class InitializerService {
  /**
   * Angular HttpClient Instance
   * @type {HttpClient}
   */
  private http: HttpClient;

  /**
   * CurrentUser Instance
   * @type {CurrentUser}
   */
  private currentUser: CurrentUserService;

  /**
   * Application Setting Instance
   * @type {Settings}
   */
  private setting: SettingsService;

  /**
   * Local Json Service Instance
   */
  private jsonService: LocalJsonService;

  /**
   * Fiscal Years Service
   */
  private fiscalYearsService: FiscalYearsService;

  /**
   * sHttp Headers
   * @type {HttpHeaders}
   */
  private headers: HttpHeaders;

  private appInitURL: string;

  private router: Router;

  constructor(public injector: Injector) {
    this.http = this.injector.get(HttpClient);
    this.currentUser = this.injector.get(CurrentUserService);
    this.setting = this.injector.get(SettingsService);
    this.jsonService = this.injector.get(LocalJsonService);
    this.fiscalYearsService = this.injector.get(FiscalYearsService);
    this.router = this.injector.get(Router);

    this.appInitURL = `${environment.API_URL}app/init`;
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
  }

  /**
   * Function that inits application state
   *
   * @param   {[type]}  data?:  data from backedn server
   *
   * @return  {Promise<any>}
   */
  public initApplication(data?: string): Promise<any> {
    /**
     * Check already data on window object
     */
    if (!data) {
      data = window['initData'];
    }
    if (data) {
      this.bootstrapApp(data);
      return new Promise((resolve) => resolve(true));
    }
    return this.fetchInitData();
  }

  /**
   * Fetch data for app bootstraping..
   * App tries upto 3 times again if backedn server failed to send response
   * @return {Promise<any>} [description]
   */
  private fetchInitData() {
    return new Promise((resolve, reject) => {
      /**
       * Check user is whether logged in or not
       */
      if (localStorage.getItem('access_token') != null) {
        this.headers = new HttpHeaders({
          // 'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('access_token')}`,
        });
      }

      /**
       * Set HttpOptions
       */
      let options = {headers: this.headers};
      /**
       * Call to API Server
       */
      this.http
        .get(this.appInitURL, options)
        .pipe(
          retryWhen((_) => {
            return interval(20000).pipe(
              tap((_) => {
                console.log('retrying..');
                this.headers = new HttpHeaders({
                  'Content-Type': 'application/json',
                });
                localStorage.removeItem('access_token');
              }),
              flatMap((count) =>
                count == 3 ? throwError('Giving up') : of(count)
              )
            );
          })
        )
        .subscribe(
          (response) => {
            if (response['status'] == true) {

              // password expiry
              if (response['data']['user'] && response['data']['user']['passwordExpired']) {
                resolve({code: 'password-expired'});
              }

              // no branch found
              console.log(response);
              if (response['data']['user'] && response['data']['user']['branches'].length == 0) {
                resolve({code: 'no-branch'});
              }

              this.bootstrapApp(response['data']);
            } else {
              console.log(
                'Sorry.Application failed to initialize. Message:' +
                response['message']
              );
            }
            resolve(true);
          },
          (errors) => {
            console.log(
              'Sorry.Application failed to initialize. Please contact to application developer.'
            );
            reject(false);
          }
        );
    });
  }

  /**
   * Bootstrap's application init data
   * @param {string} data
   */
  private bootstrapApp(data: string) {
    //If no data
    if (!data) {
      console.log('Empty app initialization data...');
      return false;
    }
    console.log(data['user']);
    if (data['user']) {

      /**
       * Set Current User Status
       */
      this.currentUser.assignCurrentUser(data['user']);

      console.log(data['user']['passwordExpired']);
      // check password expiry
      if (data['user']['passwordExpired']) {
        //Navigate to change password
        this.currentUser.set('passwordExpired', true);
        this.router.navigate([`/auth/password-change`]);
        return false;
      }
    }

    if (data['fiscalYears']) {
      /**
       * Set FiscalYears
       */
      this.fiscalYearsService.setFiscalYears(data['fiscalYears']);
    }


    if (data['applicationProfile']) {
      // set company profile
      this.currentUser.assignCompany(data['applicationProfile']);
    } else {
      localStorage.removeItem('application');
    }

  }
}
