import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DocumentTypeRoutingModule} from './document-type-routing.module';
import {DocumentTypeComponent} from './document-type/document-type.component';
import {SharedModule} from '../../../shared/shared.module';


@NgModule({
  declarations: [DocumentTypeComponent],
  imports: [
    CommonModule,
    SharedModule,
    DocumentTypeRoutingModule
  ],
  providers: []
})
export class DocumentTypeModule {
}
