export class DocumentTypeModel {
  constructor(
    public id: number,
    public name: string,
    public namelocal: string,
    public maxfilesizeinkb: string,
    public isimageallowed: string,
    public isimageallowedstatus: string,
    public isexcelallowed: string,
    public isexcelallowedstatus: string,
    public isslideallowed: string,
    public isslideallowedstatus: string,
    public isdocumentsallowed: string,
    public isdocumentsallowedstatus: string,
    public status: string,
    public entrydate: string,
    public entryuserid: string,
    public entryusername: string,
    public statuschangeddate: string,
    public statuschangeuserid: string,
  ) {
  }

}

export class DocumentTypeFormModel {
  constructor(
    public id: number,
    public name: string,
    public nameLocal: string,
    public maxFileSizeInKB: string,
    public isImageAllowed: boolean,
    public isExcelAllowed: boolean,
    public isSlidesAllowed: boolean,
    public isDocumentsAllowed: boolean,
    public status: string,
  ) {
  }

}

