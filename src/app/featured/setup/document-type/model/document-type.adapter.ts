import {Injectable} from '@angular/core';
import {Adapter} from 'src/app/core/adapter';
import {DocumentTypeFormModel, DocumentTypeModel} from './document-type.model';

@Injectable()
export class DocumentTypeAdapter implements Adapter<DocumentTypeModel> {
  adapt(item: any): DocumentTypeModel {
    return new DocumentTypeModel(
      item.id,
      item.name,
      item.namelocal,
      item.maxfilesizeinkb,
      item.isimageallowed,
      item.isimageallowedstatus,
      item.isexcelallowed,
      item.isexcelallowedstatus,
      item.isslidesallowed,
      item.isslidesallowedstatus,
      item.isdocumentsallowed,
      item.isdocumentsallowedstatus,
      item.status,
      item.createddate,
      item.entryuserid,
      item.entryusername,
      item.statuschangeddate,
      item.statuschangeuserid
    );
  }
}

export class DocumentTypeFormAdapter implements Adapter<DocumentTypeFormModel> {
  adapt(item: any): DocumentTypeFormModel {
    return new DocumentTypeFormModel(
      item.id,
      item.name,
      item.namelocal,
      item.maxfilesizeinkb,
      item.isimageallowed == 'true',
      item.isexcelallowed == 'true',
      item.isslideallowed == 'true',
      item.ispdfallowed == 'true',
      item.status,
    );
  }
}
