
import {Injector} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {SetupController} from '../setup.controller';
import {DocumentTypeService} from './document-type.service';


export class DocumentTypeController extends SetupController {
  public documentTypeService: DocumentTypeService;
  public columns;
  public fetchedData;
  public formStructure: FormGroup;
  public buttonAccess: object = {save: true, reset: true};
  public rowData: object;

  constructor(public injector: Injector) {
    super(injector);
    this.documentTypeService = this.injector.get(DocumentTypeService);
  }


}
