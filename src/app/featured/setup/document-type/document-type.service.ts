import {Injectable, Injector} from '@angular/core';
import {environment} from 'src/environments/environment';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {AppService} from '../../../shared/service/app.service';
import {DocumentTypeAdapter, DocumentTypeFormAdapter} from './model/document-type.adapter';
import {DocumentTypeModel} from './model/document-type.model';

@Injectable()
export class DocumentTypeService extends AppService {
  private moduleName = 'doctype';
  private getUrl = `${environment.API_URL}${this.moduleName}/list/`;
  private postUrl = `${environment.API_URL}${this.moduleName}/save`;
  private deleteUrl = `${environment.API_URL}${this.moduleName}/deletebyId/`;

  constructor(public injector: Injector, private adapter: DocumentTypeAdapter, private formAdapter: DocumentTypeFormAdapter) {
    super(injector);
  }

  get(status: number = -2): Observable<DocumentTypeModel[]> {
    return this.httpClient.get<DocumentTypeModel[]>(`${this.getUrl}${status}`).pipe(
      map((data: DocumentTypeModel[]) => {
        return data['data'].map((item: DocumentTypeModel) => this.adapter.adapt(item));
      }));
  }

  post(data: object): Observable<object> {
    return this.httpClient.post<object>(this.postUrl, this.formAdapter.adapt(data)).pipe();
  }

  put(data: object, id: number): Observable<object> {
    return this.httpClient.post<object>(`${this.postUrl}`, this.formAdapter.adapt(data)).pipe();
  }

  delete(id: number): Observable<object> {
    return this.httpClient.delete<object>(`${this.deleteUrl}${id}`).pipe();
  }

}
