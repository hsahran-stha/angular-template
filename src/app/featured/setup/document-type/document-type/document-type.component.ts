import {Component, Injector, OnInit} from '@angular/core';
import {DocumentTypeController} from '../document-type.controller';
import {Validators} from '@angular/forms';
import {catchError, map, take} from 'rxjs/operators';
import {DocumentTypeModel} from '../model/document-type.model';

@Component({
  selector: 'app-document-type',
  templateUrl: './document-type.component.html',
  styleUrls: ['./document-type.component.scss']
})
export class DocumentTypeComponent extends DocumentTypeController implements OnInit {

  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.manageColumn();
    this.initializeFormStructure();
    this.fetchData();
  }

  manageColumn(): void {
    this.columns = [
      this.translate.currentLang == 'np' ?
        this.manageEachColumn('name', 'namelocal', true):
        this.manageEachColumn('name', 'name', true),
      this.manageEachColumn('maxFileSizeInKb', 'maxfilesizeinkb', true),
      this.manageEachColumn('isImageAllowed', 'isimageallowed', true),
      this.manageEachColumn('isExcelAllowed', 'isexcelallowed', true),
      this.manageEachColumn('isSlideAllowed', 'isslideallowed', true),
      this.manageEachColumn('isPdfAllowed', 'isdocumentsallowed', true),
      this.manageEachColumn('status', 'status', true),
      this.manageEachColumn('entryUserName', 'entryusername', true),
      this.manageEachColumn('entryDate', 'entrydate', true),
      this.manageEachColumn('action', '', false, false, '', true, this),
    ];
  }

  actionView(ui): void {
    this.rowData = ui.rowData;
    this.patchValue();

    this.buttonAccess['save'] = false;
    this.buttonAccess['update'] = false;
    this.buttonAccess['delete'] = false;

    this.formStructure.disable();
    this.confirmDeletionService.drop();

  }

  actionEdit(ui): void {
    this.rowData = ui.rowData;
    this.patchValue();

    this.buttonAccess['update'] = true;
    this.buttonAccess['save'] = false;
    this.buttonAccess['delete'] = false;

    this.formStructure.enable();
    this.confirmDeletionService.drop();

  }

  actionDelete(ui): void {
    this.rowData = ui.rowData;
    this.patchValue();

    this.buttonAccess['save'] = false;
    this.buttonAccess['update'] = false;
    this.buttonAccess['delete'] = true;

    this.formStructure.disable();
    this.confirmDeletionService.show();
  }

  patchValue(): void {
    this.formStructure.patchValue({
      id: this.rowData['id'],
      name: this.rowData['name'],
      namelocal: this.rowData['namelocal'],
      maxfilesizeinkb: this.rowData['maxfilesizeinkb'],
      isimageallowed: this.rowData['isimageallowed'] == 'YES',
      isexcelallowed: this.rowData['isexcelallowed'] == 'YES',
      isslideallowed: this.rowData['isslideallowed'] == 'YES',
      ispdfallowed: this.rowData['isdocumentsallowed'] == 'YES',
      status: this.rowData['status'],
    });
  }

  initializeFormStructure(): void {
    this.formStructure = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      namelocal: ['', Validators.required],
      maxfilesizeinkb: ['', Validators.required],
      isimageallowed: ['', Validators.required],
      isexcelallowed: ['', Validators.required],
      isslideallowed: ['', Validators.required],
      ispdfallowed: ['', Validators.required],
      status: ['', Validators.required],
    });
  }

  onSubmit(): void {
    if (this.formStructure.invalid) {
      this.validateAllFormFields(this.formStructure);
      this.displayInvalidFormControls(this.formStructure);
      return;
    }

    // to show confirmation box
    let obj = {
      body: this.getCRUDSubmitConfMessage(this.formStructure.get('id').value),
      btnClass: 'btn-primary',
      alertClass: 'ic-save',
      buttonName: this.formStructure.get('id').value ? this.translate.instant('update') : this.translate.instant('save')
    };
    this.confirmDialogBoxService.show(obj);
    const processConfirmation$ = this.confirmDialogBoxService.getProcessConfirmation();
    processConfirmation$.pipe(take(1)).subscribe((val) => {
      if (val) {
        if (this.formStructure.get('id').value) {
          this.documentTypeService.put(this.formStructure.value, this.formStructure.get('id').value).subscribe((res) => {
            this.afterSubmitSuccess(res);
          });
        } else {
          this.documentTypeService.post(this.formStructure.value).subscribe((res) => {
            this.afterSubmitSuccess(res);
          });
        }
      }
    });
  }

  afterSubmitSuccess(res?: object): void {
    this.fetchData();
    this.alertify.message(res['message']);
    this.formStructure.reset();
    this.formStructure.patchValue({
      isimageallowed: '',
      isexcelallowed: '',
      isslideallowed: '',
      ispdfallowed: '',
      status: ''
    });
    this.buttonAccess['save'] = true;
    this.buttonAccess['update'] = false;
  }


  fetchData(): void {
    this.documentTypeService.get().pipe(
      map((data: DocumentTypeModel[]) => {
        this.fetchedData = data;
      }),
      catchError(err => {
        console.log(err);
        return err;
      })
    ).subscribe();

  }


  resetClicked($event): void {
    if ($event) {
      this.buttonAccess['save'] = true;
      this.buttonAccess['update'] = false;
      this.buttonAccess['delete'] = false;
      this.formStructure.reset();
      this.formStructure.enable();
      this.formStructure.patchValue({
        isimageallowed: '',
        isexcelallowed: '',
        isslideallowed: '',
        ispdfallowed: '',
        status: ''
      });
      this.confirmDeletionService.drop();

    }
  }

  deleteClicked($event): void {

    // to show confirmation box
    let obj = {
      body: this.getCRUDSubmitConfMessage(this.formStructure.get('id').value, true),
      btnClass: 'btn-danger',
      alertClass: 'ic-alert',
      buttonName: this.translate.instant('confirm')
    };
    this.confirmDialogBoxService.show(obj);
    const processConfirmation$ = this.confirmDialogBoxService.getProcessConfirmation();
    processConfirmation$.pipe(take(1)).subscribe((val) => {
      if (val) {
        this.documentTypeService.delete(this.rowData['id']).subscribe((res) => {
          this.fetchData();
          this.alertify.message(res['message']);
          this.confirmDeletionService.drop();
          this.formStructure.reset();
          this.formStructure.enable();
          this.formStructure.patchValue({
            isimageallowed: '',
            isexcelallowed: '',
            isslideallowed: '',
            ispdfallowed: '',
            status: ''
          });
          this.buttonAccess['delete'] = false;
          this.buttonAccess['save'] = true;
        });

      }
    });
  }

  ngOnDestroy() {
  }


}
