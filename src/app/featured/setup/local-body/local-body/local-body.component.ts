import {Component, OnInit, Injector, OnDestroy, ViewChild, ElementRef} from '@angular/core';
import {Validators} from '@angular/forms';
import {catchError, map, take} from 'rxjs/operators';
import {LocalBodyController} from '../local-body.controller';
import {Status} from '../../../../shared/enums/status';

@Component({
  selector: 'app-local-body',
  templateUrl: './local-body.component.html',
  styleUrls: ['./local-body.component.scss'],
})
export class LocalBodyComponent extends LocalBodyController implements OnInit, OnDestroy {
  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.manageColumn();
    this.manageDistrictColumn();
    this.initializeFormStructure();
    this.fetchData();
    this.fetchLocalBodyType();
  }

  manageColumn(): void {
    this.columns = [
      this.translate.currentLang == 'np' ?
        this.manageEachColumn('name', 'localbodynamelocal', true) :
        this.manageEachColumn('name', 'localbodyname', true),
      this.translate.currentLang == 'np' ?
        this.manageEachColumn('district', 'districtnamelocal', true) :
        this.manageEachColumn('district', 'districtname', true),

      this.manageEachColumn('status', 'status', true),
      this.manageEachColumn('entryUserName', 'entryusername', true),
      this.manageEachColumn('entryDate', 'createddate', true),
      this.manageEachColumn('action', '', false, false, '', true, this),
    ];
  }

  manageDistrictColumn(): void {
    this.districtColumns = [
      this.manageEachColumn('code', 'districtcode', true),
      this.translate.currentLang == 'np' ?
        this.manageEachColumn('district', 'districtnamelocal', true) :
        this.manageEachColumn('district', 'districtname', true)];
  }

  actionView(ui): void {
    this.rowData = ui.rowData;
    this.patchValue();

    this.buttonAccess['save'] = false;
    this.buttonAccess['update'] = false;
    this.buttonAccess['delete'] = false;

    this.formStructure.disable();
    this.confirmDeletionService.drop();

  }

  actionEdit(ui): void {
    this.rowData = ui.rowData;
    this.patchValue();

    this.buttonAccess['update'] = true;
    this.buttonAccess['save'] = false;
    this.buttonAccess['delete'] = false;

    this.formStructure.enable();
    this.formStructure.get('districtname').disable();
    this.confirmDeletionService.drop();

  }

  actionDelete(ui): void {
    this.rowData = ui.rowData;
    this.patchValue();

    this.buttonAccess['save'] = false;
    this.buttonAccess['update'] = false;
    this.buttonAccess['delete'] = true;

    this.formStructure.disable();
    this.confirmDeletionService.show();

  }

  patchValue(): void {
    this.formStructure.patchValue({
      id: this.rowData['id'],
      name: this.rowData['localbodyname'],
      localbodytypeid: this.rowData['localbodytypeid'],
      namelocal: this.rowData['localbodynamelocal'],
      districtid: this.rowData['districtid'],
      districtname: this.translate.currentLang == 'np' ? this.rowData['districtnamelocal'] : this.rowData['districtname'],
      status: this.rowData['status'],
    });
  }

  initializeFormStructure(): void {
    this.formStructure = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      localbodytypeid: ['', Validators.required],
      namelocal: ['', Validators.required],
      districtid: ['', Validators.required],
      districtname: [{value: '', disabled: true}],
      status: ['', Validators.required],
    });
  }

  onSubmit(): void {
    if (this.formStructure.invalid) {
      this.validateAllFormFields(this.formStructure);
      this.displayInvalidFormControls(this.formStructure);
      return;
    }

    // to show confirmation box
    let obj = {
      body: this.getCRUDSubmitConfMessage(this.formStructure.get('id').value),
      btnClass: 'btn-primary',
      alertClass: 'ic-save',
      buttonName: this.formStructure.get('id').value ? this.translate.instant('update') : this.translate.instant('save')
    };
    this.confirmDialogBoxService.show(obj);
    const processConfirmation$ = this.confirmDialogBoxService.getProcessConfirmation();
    processConfirmation$.pipe(take(1)).subscribe((val) => {
      if (val) {
        if (this.formStructure.get('id').value) {
          this.localBodyService.put(this.formStructure.getRawValue(), this.formStructure.get('id').value).subscribe((res) => {
            this.afterSubmitSuccess(res);
          });
        } else {
          this.localBodyService.post(this.formStructure.getRawValue()).subscribe((res) => {
            this.afterSubmitSuccess(res);
          });
        }
      }
    });
  }

  afterSubmitSuccess(res?: object): void {
    this.fetchData();
    this.formStructure.reset();
    this.formStructure.patchValue({status: ''});
    this.formStructure.patchValue({localbodytypeid: ''});
    this.alertify.message(res['message']);

    this.formStructure.get('districtname').disable();

    this.buttonAccess['save'] = true;
    this.buttonAccess['update'] = false;
  }


  fetchData(): void {
    this.fetchedData = [];
    this.remoteUrl = this.localBodyService.get();
  }

  fetchLocalBodyType(): void {
    this.localBodyService.getLocalBodyType().pipe(
      map((data: object[]) => {
        this.localBodyType = data;
      }),
      catchError(err => {
        return err;
      })
    ).subscribe();
  }

  @ViewChild('districtContent') districtContent: ElementRef;

  fetchDistrictData(): void {
    this.districtList = [];
    this.remoteDistrictUrl = this.districtService.get(Status.ACTIVE);
    this.openLg(this.districtContent);
  }


  resetClicked($event): void {
    if ($event) {
      this.buttonAccess['save'] = true;
      this.buttonAccess['update'] = false;
      this.buttonAccess['delete'] = false;
      this.formStructure.reset();
      this.formStructure.patchValue({status: ''});
      this.formStructure.patchValue({localbodytypeid: ''});


      this.formStructure.enable();
      this.formStructure.get('districtname').disable();
      this.confirmDeletionService.drop();
    }
  }

  deleteClicked($event): void {

    // to show confirmation box
    let obj = {
      body: this.getCRUDSubmitConfMessage(this.formStructure.get('id').value, true),
      btnClass: 'btn-danger',
      alertClass: 'ic-alert',
      buttonName: this.translate.instant('confirm')
    };
    this.confirmDialogBoxService.show(obj);
    const processConfirmation$ = this.confirmDialogBoxService.getProcessConfirmation();
    processConfirmation$.pipe(take(1)).subscribe((val) => {
      if (val) {
        this.localBodyService.delete(this.rowData['id']).subscribe((res) => {
          this.fetchData();
          this.confirmDeletionService.drop();
          this.formStructure.reset();
          this.formStructure.patchValue({status: ''});
          this.formStructure.patchValue({localbodytypeid: ''});
          this.alertify.message(res['message']);

          this.formStructure.enable();
          this.formStructure.get('districtname').disable();
          this.buttonAccess['delete'] = false;
          this.buttonAccess['save'] = true;
        });

      }
    });
  }

  ngOnDestroy() {
  }

  getSelectedDistrict($event, modal): void {
    console.log($event);
    this.formStructure.patchValue({
      districtid: $event.id,
      districtname: this.translate.currentLang == 'np' ? $event.districtnamelocal : $event.districtname,
    });
    this.modalService.dismissAll();
  }
}
