import {LocalBodyService} from './local-body.service';
import {Injector} from '@angular/core';
import {SetupController} from '../setup.controller';
import {FormGroup} from '@angular/forms';
import {DistrictService} from '../district/district.service';


export class LocalBodyController extends SetupController {
  public columns;
  public fetchedData;
  public remoteUrl;
  public remoteDistrictUrl;
  public localBodyType;
  public formStructure: FormGroup;
  public buttonAccess: object = {save: true, reset: true};
  public rowData: object;
  public localBodyService: LocalBodyService;
  public districtService: DistrictService;
  public districtList: object[];
  public districtColumns: object[];

  constructor(public injector: Injector) {
    super(injector);
    this.localBodyService = this.injector.get(LocalBodyService);
    this.districtService = this.injector.get(DistrictService);

  }


}
