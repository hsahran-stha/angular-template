import { TestBed } from '@angular/core/testing';

import { LocalBodyService } from './local-body.service';

describe('LocalBodyService', () => {
  let service: LocalBodyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LocalBodyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
