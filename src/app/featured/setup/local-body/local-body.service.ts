import {Injectable, Injector} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AppService} from '../../../shared/service/app.service';
import {environment} from '../../../../environments/environment';
import {LocalBodyAdapter, LocalBodyFormAdapter} from './model/local-body.adapter';
import {LocalBodyModel} from './model/local-body.model';

@Injectable()
export class LocalBodyService extends AppService {
  private moduleName = 'localbody';
  private getUrl = `${environment.API_URL}${this.moduleName}/list/`;
  private getLocalBodyTypeUrl = `${environment.API_URL}localBodyType/getAll/`;
  private getByDistrictUrl = `${environment.API_URL}localbody/district`;
  private postUrl = `${environment.API_URL}${this.moduleName}/save`;
  private deleteUrl = `${environment.API_URL}${this.moduleName}/deletebyId/`;

  constructor(public injector: Injector, private adapter: LocalBodyAdapter, private formAdapter: LocalBodyFormAdapter) {
    super(injector);
  }

  get(status: number = -2): string {
    return `${this.getUrl}${status}`;
  }

  getLocalBodyType(): Observable<object[]> {
    return this.httpClient.get<object[]>(this.getLocalBodyTypeUrl).pipe(
      map((data: object[]) => {
        return data['data'].map((item: object) => item);
      }));
  }

  post(data: object): Observable<object> {
    return this.httpClient.post<object>(this.postUrl, this.formAdapter.adapt(data)).pipe();
  }

  put(data: object, id: number): Observable<object> {
    return this.httpClient.post<object>(`${this.postUrl}`, this.formAdapter.adapt(data)).pipe();
  }

  delete(id: number): Observable<object> {
    return this.httpClient.delete<object>(`${this.deleteUrl}${id}`).pipe();
  }

  getById(id: number): Observable<LocalBodyModel> {
    return this.httpClient.get(`${this.getUrl}/${id}`).pipe(map(data => this.adapter.adapt(data)));
  }
  getByDistrict(id: number): Observable<object> {
    return this.httpClient.get(`${this.getByDistrictUrl}/${id}`).pipe(map(data => data['data']));
  }
}
