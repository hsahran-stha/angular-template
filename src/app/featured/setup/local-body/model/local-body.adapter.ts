import {LocalBodyFormModel, LocalBodyModel} from './local-body.model';
import {Injectable} from '@angular/core';
import {Adapter} from '../../../../core/adapter';

@Injectable()
export class LocalBodyAdapter implements Adapter<LocalBodyModel> {
  adapt(item: any): LocalBodyModel {
    return new LocalBodyModel(
      item.id,
      item.name,
      item.namelocal,
      item.districtid,
      item.districtname,
      item.status,
      item.entrydate,
      item.entryuserid,
      item.statuschangedate,
      item.statuschangeuserid
    );
  }
}

export class LocalBodyFormAdapter implements Adapter<LocalBodyFormModel> {
  adapt(item: any): LocalBodyFormModel {
    return new LocalBodyFormModel(
      item.id,
      item.localbodytypeid,
      item.name,
      item.namelocal,
      item.districtid,
      item.status,
    );
  }
}
