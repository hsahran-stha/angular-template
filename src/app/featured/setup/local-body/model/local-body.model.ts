export class LocalBodyModel {
  constructor(
    public id: number,
    public name: string,
    public namelocal: string,
    public districtid: number,
    public districtname: string,
    public status: number,
    public entrydate: number,
    public entryuserid: number,
    public statuschangedate: number,
    public statuschangeuserid: number,
  ) {
  }

}

export class LocalBodyFormModel {
  constructor(
    public id: number,
    public localBodyTypeId: number,
    public localBodyName: string,
    public localBodyNameLocal: string,
    public districtId: number,
    public status: number,
  ) {
  }

}
