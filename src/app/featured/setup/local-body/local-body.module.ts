import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LocalBodyRoutingModule} from './local-body-routing.module';
import {LocalBodyComponent} from './local-body/local-body.component';
import {SharedModule} from 'src/app/shared/shared.module';


@NgModule({
  declarations: [LocalBodyComponent],
  imports: [
    CommonModule, SharedModule,
    LocalBodyRoutingModule
  ],
  providers: [ ]
})
export class LocalBodyModule {
}
