import {Injector} from '@angular/core';

import {FeaturedController} from '../featured.controller';

export class SetupController extends FeaturedController {

  constructor(public injector: Injector) {
    super(injector);
  }


}
