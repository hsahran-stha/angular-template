import {FormGroup, Validators} from '@angular/forms';
import {Component, OnInit, Injector, OnDestroy} from '@angular/core';
import {take} from 'rxjs/operators';
import {DistrictController} from '../district.controller';

@Component({
  selector: 'app-district',
  templateUrl: './district.component.html',
  styleUrls: ['./district.component.scss']
})
export class DistrictComponent extends DistrictController implements OnInit, OnDestroy {

  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.fetchData();
    this.initializeFormStructure();
    this.manageColumn();
  }

  initializeFormStructure(): void {
    this.formStructure = this.formBuilder.group({
      id: [''],
      code: ['', Validators.required],
      name: ['', Validators.required],
      namelocal: ['', Validators.required],
      status: ['', Validators.required],
    });
  }

  manageColumn(): void {
    this.columns = [
      this.manageEachColumn('code', 'districtcode', true),
      this.translate.currentLang == 'np' ?
        this.manageEachColumn('district', 'districtnamelocal', true) :
        this.manageEachColumn('district', 'districtname', true),
      this.manageEachColumn('status', 'status', true),
      this.manageEachColumn('entryUserName', 'entryusername', true),
      this.manageEachColumn('entryDate', 'createddate', true),
      this.manageEachColumn('action', '', false, false, '', true, this),
    ];
  }

  patchValue(): void {
    this.formStructure.patchValue({
      id: this.rowData['id'],
      code: this.rowData['districtcode'],
      name: this.rowData['districtname'],
      namelocal: this.rowData['districtnamelocal'],
      status: this.rowData['status']
    });
  }

  actionView(ui): void {
    this.rowData = ui.rowData;
    this.patchValue();

    this.buttonAccess['save'] = false;
    this.buttonAccess['update'] = false;
    this.buttonAccess['delete'] = false;

    this.formStructure.disable();
    this.confirmDeletionService.drop();

  }

  actionEdit(ui): void {
    this.rowData = ui.rowData;
    this.patchValue();

    this.buttonAccess['update'] = true;
    this.buttonAccess['save'] = false;
    this.buttonAccess['delete'] = false;

    this.formStructure.enable();
    this.confirmDeletionService.drop();

  }

  actionDelete(ui): void {
    this.rowData = ui.rowData;
    this.patchValue();

    this.buttonAccess['save'] = false;
    this.buttonAccess['update'] = false;
    this.buttonAccess['delete'] = true;

    this.formStructure.disable();
    this.confirmDeletionService.show();

  }

  onSubmit(): void {
    if (this.formStructure.invalid) {
      this.validateAllFormFields(this.formStructure);
      this.displayInvalidFormControls(this.formStructure);
      return;
    }

    // to show confirmation box
    let obj = {
      body: this.getCRUDSubmitConfMessage(this.formStructure.get('id').value),
      btnClass: 'btn-primary',
      alertClass: 'ic-save',
      buttonName: this.formStructure.get('id').value ? this.translate.instant('update') : this.translate.instant('save')
    };
    this.confirmDialogBoxService.show(obj);
    const processConfirmation$ = this.confirmDialogBoxService.getProcessConfirmation();
    processConfirmation$.pipe(take(1)).subscribe((val) => {
      if (val) {
        if (this.formStructure.get('id').value) {
          this.districtService.put(this.formStructure.value, this.formStructure.get('id').value).subscribe((res) => {
            this.afterSubmitSuccess(res);
          });
        } else {
          this.districtService.post(this.formStructure.value).subscribe((res) => {
            this.afterSubmitSuccess(res);
          });
        }
      }
    });
  }

  afterSubmitSuccess(res?: object): void {
    this.fetchData();
    this.formStructure.reset();
    this.formStructure.patchValue({status: ''});
    this.alertify.message(res['message']);

    this.buttonAccess['save'] = true;
    this.buttonAccess['update'] = false;
  }


  fetchData(): void {
    this.fetchedData = [];
    this.remoteUrl = this.districtService.get();

  }

  resetClicked($event): void {
    if ($event) {
      this.buttonAccess['save'] = true;
      this.buttonAccess['update'] = false;
      this.buttonAccess['delete'] = false;
      this.formStructure.reset();
      this.formStructure.patchValue({status: ''});

      this.formStructure.enable();
      this.confirmDeletionService.drop();

    }
  }

  deleteClicked($event): void {

    // to show confirmation box
    let obj = {
      body: this.getCRUDSubmitConfMessage(this.formStructure.get('id').value, true),
      btnClass: 'btn-danger',
      alertClass: 'ic-alert',
      buttonName: this.translate.instant('confirm')
    };
    this.confirmDialogBoxService.show(obj);
    const processConfirmation$ = this.confirmDialogBoxService.getProcessConfirmation();
    processConfirmation$.pipe(take(1)).subscribe((val) => {
      if (val) {

        this.districtService.delete(this.rowData['id']).subscribe((res) => {
          this.fetchData();
          this.confirmDeletionService.drop();
          this.formStructure.reset();
          this.formStructure.patchValue({status: ''});
          this.alertify.message(res['message']);

          this.formStructure.enable();
          this.buttonAccess['delete'] = false;
          this.buttonAccess['save'] = true;
        });

      }
    });
  }

  ngOnDestroy() {
  }


}
