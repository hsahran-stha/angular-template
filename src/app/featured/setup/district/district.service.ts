import {Injectable, Injector} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AppService} from '../../../shared/service/app.service';
import {environment} from '../../../../environments/environment';
import {DistrictAdapter, DistrictFormAdapter} from './model/district.adapter';
import {DistrictModel} from './model/district.model';

@Injectable()
export class DistrictService extends AppService {
  private moduleName = `district`;
  private getUrl = `${environment.API_URL}${this.moduleName}/list/`;
  private postUrl = `${environment.API_URL}${this.moduleName}/save/`;
  private deleteUrl = `${environment.API_URL}${this.moduleName}/deletebyId/`;

  constructor(public injector: Injector, private adapter: DistrictAdapter, private formAdapter: DistrictFormAdapter) {
    super(injector);
  }

  get(status: number = -2): string {
    return `${this.getUrl}${status}`;
  }

  getNonPaginated(status: number = -2): Observable<object> {
    return this.httpClient.get<object>(`${this.getUrl}${status}`).pipe(map(d => d['data']));
  }

  post(data: object): Observable<object> {
    return this.httpClient.post<object>(this.postUrl, this.formAdapter.adapt(data)).pipe();
  }

  put(data: object, id: number): Observable<object> {
    return this.httpClient.post<object>(`${this.postUrl}`, this.formAdapter.adapt(data)).pipe();
  }

  delete(id: number): Observable<object> {
    return this.httpClient.delete<object>(`${this.deleteUrl}${id}`).pipe();
  }

  getById(id: number): Observable<DistrictModel> {
    return this.httpClient.get(`${this.getUrl}/${id}`).pipe(map(data => this.adapter.adapt(data)));
  }

}
