import {Injectable} from '@angular/core';
import {Adapter} from '../../../../core/adapter';
import {DistrictFormModel, DistrictModel} from './district.model';


@Injectable()
export class DistrictAdapter implements Adapter<DistrictModel> {
  adapt(item: any): DistrictModel {
    return new DistrictModel(
      item.id,
      item.code,
      item.name,
      item.namelocal,
      item.status,
      item.entrydate,
      item.entryuserid,
      item.statuschangedate,
      item.statuschangeuserid);
  }
}


export class DistrictFormAdapter implements Adapter<DistrictFormModel> {
  adapt(item: any): DistrictFormModel {
    return new DistrictFormModel(
      item.id,
      item.code,
      item.name,
      item.namelocal,
      item.status
    );
  }
}
