export class DistrictModel {
  constructor(
    public id: number,
    public code: number,
    public name: string,
    public namelocal: string,
    public status: number,
    public entrydate: number,
    public entryuserid: number,
    public statuschangedate: number,
    public statuschangeuserid: number,
  ) {
  }

}
export class DistrictFormModel {
  constructor(
    public id: number,
    public districtCode: number,
    public districtName: string,
    public districtNameLocal: string,
    public status: number,
  ) {
  }

}
