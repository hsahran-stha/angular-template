import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DistrictRoutingModule} from './district-routing.module';
import {DistrictComponent} from './district/district.component';
import {SharedModule} from 'src/app/shared/shared.module';


@NgModule({
  declarations: [DistrictComponent],
  imports: [
    CommonModule, SharedModule,
    DistrictRoutingModule
  ],
  providers: []

})
export class DistrictModule {
}
