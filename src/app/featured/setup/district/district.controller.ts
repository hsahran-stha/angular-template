import {Injector} from '@angular/core';
import {SetupController} from '../setup.controller';
import {FormGroup} from '@angular/forms';
import {DistrictService} from './district.service';


export class DistrictController extends SetupController {

  public columns;
  public fetchedData;
  public remoteUrl;
  public formStructure: FormGroup;
  public districtService: DistrictService;
  public buttonAccess: object = {save: true, reset: true};
  public rowData: object;

  constructor(public injector: Injector) {
    super(injector);
    this.districtService = this.injector.get(DistrictService);

  }


}
