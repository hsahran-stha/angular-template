import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {OfficeRoutingModule} from './office-routing.module';
import {OfficeComponent} from './office/office.component';
import {SharedModule} from '../../../shared/shared.module';


@NgModule({
  declarations: [OfficeComponent],
  imports: [
    CommonModule,
    SharedModule,
    OfficeRoutingModule
  ],
  providers: []

})
export class OfficeModule {
}
