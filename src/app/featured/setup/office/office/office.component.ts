import {OfficeController} from '../office.controller';
import {Component, AfterViewInit, OnInit, Injector, ViewChild, ElementRef, OnDestroy} from '@angular/core';
import {AbstractControl, FormControl, Validators} from '@angular/forms';
import {catchError, map, take} from 'rxjs/operators';
import {OfficeModel} from '../model/office.model';
import {Status} from '../../../../shared/enums/status';
import {OfficeEmailValidator} from '../../../../shared/validators/officeemail.validator';
import {NumberValidator} from '../../../../shared/validators/number.validator';

@Component({
  selector: 'app-office',
  templateUrl: './office.component.html',
  styleUrls: ['./office.component.scss']
})
export class OfficeComponent extends OfficeController implements OnInit, OnDestroy {

  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.manageColumn();
    this.manageLocalBodyColumn();
    this.initializeFormStructure();
    this.fetchData();
    this.fetchOfficeType();

  }

  actionView(ui): void {
    this.rowData = ui.rowData;
    this.patchValue();

    this.buttonAccess['save'] = false;
    this.buttonAccess['update'] = false;
    this.buttonAccess['delete'] = false;

    this.formStructure.disable();
    this.confirmDeletionService.drop();

  }

  actionEdit(ui): void {
    this.rowData = ui.rowData;
    this.patchValue();

    this.buttonAccess['update'] = true;
    this.buttonAccess['save'] = false;
    this.buttonAccess['delete'] = false;

    this.formStructure.enable();
    this.formStructure.get('localbodyname').disable();
    this.formStructure.get('district').disable();
    this.confirmDeletionService.drop();

  }

  actionDelete(ui): void {
    this.rowData = ui.rowData;
    this.patchValue();

    this.buttonAccess['save'] = false;
    this.buttonAccess['update'] = false;
    this.buttonAccess['delete'] = true;

    this.formStructure.disable();
    this.confirmDeletionService.show();
  }

  patchValue(): void {
    this.formStructure.patchValue({
      id: this.rowData['id'],
      projectcode: this.rowData['projectcode'],
      name: this.rowData['name'],
      namelocal: this.rowData['namelocal'],
      localbodyid: this.rowData['localbodyid'],
      localbodyname: this.translate.currentLang == 'np' ? this.rowData['localbodynamelocal'] : this.rowData['localbodyname'],
      officetypeid: parseInt(this.rowData['officeTypeId']),
      district: this.translate.currentLang == 'np' ? this.rowData['districtnamelocal'] : this.rowData['districtname'],
      status: this.rowData['status'],
      mobilenumber: this.rowData['mobileNumber'],
      email: this.rowData['email'],

    });
  }




  initializeFormStructure(): void {
    this.formStructure = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      namelocal: ['', Validators.required],
      projectcode: ['', Validators.required],
      officetypeid: ['', Validators.required],
      localbodyid: ['', Validators.required],
      mobilenumber: ['', [Validators.required, NumberValidator.commaSepNumber]],
      email: ['', [Validators.required, OfficeEmailValidator.commaSepEmail]],
      status: ['', Validators.required],
      localbodyname: [{value: '', disabled: true}, Validators.required],
      district: [{value: '', disabled: true}]
    });
    console.log('Mobile number',this.formStructure.get('mobilenumber').value);
  }

  manageColumn() {
    this.columns = [
      this.manageEachColumn('projectCode', 'projectcode', true),
      this.translate.currentLang == 'np' ?
        this.manageEachColumn('name', 'namelocal', true) :
        this.manageEachColumn('name', 'name', true),
      this.translate.currentLang == 'np' ?
        this.manageEachColumn('localBody', 'localbodynamelocal', true) :
        this.manageEachColumn('localBody', 'localbodyname', true),
      this.translate.currentLang == 'np' ?
        this.manageEachColumn('officeType', 'officeTypeNameLocal', true) :
        this.manageEachColumn('officeType', 'officeTypeName', true),
      this.translate.currentLang == 'np' ?
        this.manageEachColumn('district', 'districtnamelocal', true) :
        this.manageEachColumn('district', 'districtname', true),
      this.manageEachColumn('status', 'status', true),
      this.manageEachColumn('entryUserName', 'entryusername', true),
      this.manageEachColumn('entryDate', 'entrydate', true),
      this.manageEachColumn('action', '', false, false, '', true, this),
    ];
  }

  manageLocalBodyColumn() {
    this.localBodyColumns = [
      this.translate.currentLang == 'np' ?
        this.manageEachColumn('localBody', 'localbodynamelocal', true) :
        this.manageEachColumn('localBody', 'localbodyname', true),
      this.translate.currentLang == 'np' ?
        this.manageEachColumn('district', 'districtnamelocal', true) :
        this.manageEachColumn('district', 'districtname', true),
      this.translate.currentLang == 'np' ?
        this.manageEachColumn('type', 'localbodytypenamelocal', true) :
        this.manageEachColumn('district', 'localbodytypename', true),
    ];
  }

  onSubmit(): void {
    if (this.formStructure.invalid) {
      this.validateAllFormFields(this.formStructure);
      this.displayInvalidFormControls(this.formStructure);
      return;
    }

    // to show confirmation box
    let obj = {
      body: this.getCRUDSubmitConfMessage(this.formStructure.get('id').value),
      btnClass: 'btn-primary',
      alertClass: 'ic-save',
      buttonName: this.formStructure.get('id').value ? this.translate.instant('update') : this.translate.instant('save')
    };
    this.confirmDialogBoxService.show(obj);
    const processConfirmation$ = this.confirmDialogBoxService.getProcessConfirmation();
    processConfirmation$.pipe(take(1)).subscribe((val) => {
      if (val) {
        if (this.formStructure.get('id').value) {
          this.officeService.put(this.formStructure.getRawValue(), this.formStructure.get('id').value).subscribe((res) => {
            this.afterSubmitSuccess(res);

          });
        } else {
          this.officeService.post(this.formStructure.getRawValue()).subscribe((res) => {
            this.afterSubmitSuccess(res);
          });
        }
      }
    });
  }

  afterSubmitSuccess(res?: object): void {
    this.formStructure.reset();
    this.alertify.message(res['message']);
    this.formStructure.patchValue({officetypeid: ''});
    this.formStructure.patchValue({status: ''});
    this.fetchData();
    this.formStructure.enable();
    this.buttonAccess['update'] = false;
    this.buttonAccess['save'] = true;

    this.formStructure.get('localbodyname').disable();
    this.formStructure.get('district').disable();


  }


  fetchData(): void {
    this.officeService.get().pipe(
      map((data: OfficeModel[]) => {
        this.fetchedData = data;
      }),
      catchError(err => {
        console.log(err);
        return err;
      })
    ).subscribe();
  }

  fetchOfficeType(): void {
    this.officeService.getOfficeType().pipe(
      map((data: object[]) => {
        this.officeTypes = data;
        console.log(this.officeTypes);
      }),
      catchError(err => {
        return err;
      })
    ).subscribe();
  }


  @ViewChild('localBodyContent') localBodyContent: ElementRef;

  fetchLocalBodyData(): void {
    this.localBodyList = [];
    this.remoteLocalBodyUrl = this.localBodyService.get(Status.ACTIVE);
    this.openLg(this.localBodyContent);

  }

  resetClicked($event): void {
    if ($event) {
      this.buttonAccess['save'] = true;
      this.buttonAccess['update'] = false;
      this.buttonAccess['delete'] = false;

      this.formStructure.reset();
      this.formStructure.patchValue({officetypeid: ''});
      this.formStructure.patchValue({status: ''});


      this.formStructure.enable();

      this.formStructure.get('localbodyname').disable();
      this.formStructure.get('district').disable();

      this.confirmDeletionService.drop();

    }
  }

  deleteClicked($event): void {

    // to show confirmation box
    let obj = {
      body: this.getCRUDSubmitConfMessage(this.formStructure.get('id').value, true),
      btnClass: 'btn-danger',
      alertClass: 'ic-alert',
      buttonName: this.translate.instant('confirm')
    };
    this.confirmDialogBoxService.show(obj);
    const processConfirmation$ = this.confirmDialogBoxService.getProcessConfirmation();
    processConfirmation$.pipe(take(1)).subscribe((val) => {
      if (val) {
        this.officeService.delete(this.rowData['id']).subscribe((res) => {
          this.fetchData();

          this.formStructure.reset();
          this.formStructure.patchValue({officetypeid: ''});
          this.formStructure.patchValue({status: ''});
          this.alertify.message(res['message']);

          this.formStructure.enable();
          this.buttonAccess['save'] = true;
          this.buttonAccess['delete'] = false;
          this.confirmDeletionService.drop();
          this.formStructure.get('localbodyname').disable();
          this.formStructure.get('district').disable();


        });
      }
    });
  }

  getSelectedLocalData($event, modal): void {

    this.formStructure.patchValue({
      localbodyid: $event.id,
      localbodyname: this.translate.currentLang == 'np' ? $event.localbodynamelocal : $event.localbodyname,
      district: this.translate.currentLang == 'np' ? $event.districtnamelocal : $event.districtname,

    });

    modal.dismiss('Cross click');

  }


  ngOnDestroy() {
  }


}
