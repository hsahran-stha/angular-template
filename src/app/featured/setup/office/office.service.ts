import {Injectable, Injector} from '@angular/core';
import {environment} from 'src/environments/environment';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AppService} from '../../../shared/service/app.service';
import {OfficeAdapter, OfficeFormAdapter} from './model/office.adapter';
import {OfficeModel} from './model/office.model';

@Injectable()
export class OfficeService extends AppService {
  private moduleName = 'office';
  private getUrl = `${environment.API_URL}${this.moduleName}/list`;
  private getByTypeUrl = `${environment.API_URL}${this.moduleName}/list/code?code=null&officeTypeId=`;
  private postUrl = `${environment.API_URL}${this.moduleName}/save`;
  private deleteUrl = `${environment.API_URL}${this.moduleName}/deletebyId/`;
  private getByIdUrl = `${environment.API_URL}${this.moduleName}/`;
  private getOfficeTypeUrl = `${environment.API_URL}officeType/getAll`;

  constructor(public injector: Injector, private adapter: OfficeAdapter, private formAdapter: OfficeFormAdapter) {
    super(injector);
  }

  get(status: number = -2): Observable<OfficeModel[]> {
    return this.httpClient.get<OfficeModel[]>(`${this.getUrl}/status=${status}`).pipe(
      map((data: OfficeModel[]) => {
        return data['data'].map((item: OfficeModel) => this.adapter.adapt(item));
      }));
  }

  getByType(type: string): Observable<OfficeModel[]> {
    return this.httpClient.get<OfficeModel[]>(`${this.getByTypeUrl}${type}`).pipe(
      map((data: OfficeModel[]) => {
        return data['data'].map((item: OfficeModel) => this.adapter.adapt(item));
      }));
  }

  getOfficeType(): Observable<object[]> {
    return this.httpClient.get<object[]>(this.getOfficeTypeUrl).pipe(
      map((data: object[]) => {
        return data['data'].map((item: object) => item);
      }));
  }

  post(data: object): Observable<object> {
    return this.httpClient.post<object>(this.postUrl, this.formAdapter.adapt(data)).pipe();
  }

  put(data: object, id: number): Observable<object> {
    return this.httpClient.post<object>(`${this.postUrl}`, this.formAdapter.adapt(data)).pipe();
  }

  delete(id: number): Observable<object> {
    return this.httpClient.delete<object>(`${this.deleteUrl}${id}`).pipe();
  }

  getById(id: number): Observable<OfficeModel> {
    return this.httpClient.get(`${this.getByIdUrl}${id}`).pipe(map(data => this.adapter.adapt(data)));
  }

}
