import {OfficeService} from './office.service';
import {Injector} from '@angular/core';
import {SetupController} from '../setup.controller';
import {FormGroup} from '@angular/forms';
import {LocalBodyService} from '../local-body/local-body.service';


export class OfficeController extends SetupController {

  public officeService: OfficeService;
  public localBodyService: LocalBodyService;
  public columns;
  public fetchedData;
  public remoteLocalBodyUrl;
  public formStructure: FormGroup;
  public buttonAccess: object = {save: true, reset: true};
  public rowData: object;
  public officeTypes: object[];
  public localBodyList: object[];
  public localBodyColumns: object[];

  constructor(public injector: Injector) {
    super(injector);
    this.officeService = this.injector.get(OfficeService);
    this.localBodyService = this.injector.get(LocalBodyService);

  }


}
