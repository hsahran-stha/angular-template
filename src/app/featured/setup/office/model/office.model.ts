export class OfficeModel {
  constructor(
    public id: number,
    public name: string,
    public namelocal: string,
    public projectcode: string,
    public officeTypeId: number,
    public officeTypeName: string,
    public officeTypeNameLocal: string,
    public localbodyid: number,
    public localbodyname: string,
    public localbodynamelocal: string,
    public districtid: string,
    public districtname: string,
    public districtnamelocal: string,
    public mobileNumber: object[],
    public email: object[],
    public status: string,
    public entrydate: string,
    public entryuserid: number,
    public entryusername: string,
    public statuschangedate: number,
    public statuschangeuserid: number,
  ) {
  }

}

export class OfficeFormModel {
  constructor(
    public id: number,
    public officeName: string,
    public officeNameLocal: string,
    public projectCode: string,
    public officeTypeId: number,
    public localBodyId: number,
    public mobileNumber: string,
    public email: string,
    public status: string,
  ) {
  }

}
