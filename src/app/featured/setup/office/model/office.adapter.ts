import {Injectable} from '@angular/core';
import {Adapter} from '../../../../core/adapter';
import {OfficeFormModel, OfficeModel} from './office.model';


@Injectable()
export class OfficeAdapter implements Adapter<OfficeModel> {
  adapt(item: any): OfficeModel {
    return new OfficeModel(
      item.id,
      item.officename,
      item.officenamelocal,
      item.projectcode,
      item.officetypeid,
      item.officetypename,
      item.officetypenamelocal,
      item.localbodyid,
      item.localbodyname,
      item.localbodynamelocal,
      item.districtid,
      item.districtname,
      item.districtnamelocal,
      item.mobilenumber,
      item.email,
      item.status,
      item.createddate,
      item.entryuserid,
      item.entryusername,
      item.statuschangedate,
      item.statuschangeuserid
    );
  }
}

export class OfficeFormAdapter implements Adapter<OfficeFormModel> {
  adapt(item: any): OfficeFormModel {
    return new OfficeFormModel(
      item.id,
      item.name,
      item.namelocal,
      item.projectcode,
      item.officetypeid,
      item.localbodyid,
      item.mobilenumber.split(','),
      item.email.split(','),
      item.status,
    );
  }
}
