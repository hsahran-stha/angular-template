import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {environment} from '../../../../environments/environment';
import {ApplicationProfileListComponent} from './application-profile-list/application-profile-list.component';
import {ApplicationProfileFormComponent} from './application-profile-form/application-profile-form.component';

const routes: Routes = [
  {
    path: '', component: ApplicationProfileListComponent
  },
  {
    path: 'create', component: ApplicationProfileFormComponent,
    data: {menucode: environment.menuCodes.applicationProfileSetup}
  },
  {
    path: 'view/:id', component: ApplicationProfileFormComponent,
    data: {menucode: environment.menuCodes.applicationProfileSetup}
  },
  {
    path: 'edit/:id', component: ApplicationProfileFormComponent,
    data: {menucode: environment.menuCodes.applicationProfileSetup}
  },
  {
    path: 'delete/:id', component: ApplicationProfileFormComponent,
    data: {menucode: environment.menuCodes.applicationProfileSetup}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationProfileRoutingModule { }
