import {Injectable} from '@angular/core';
import {Adapter} from '../../../../core/adapter';
import {ApplicationProfileFormModel, ApplicationProfileModel} from './application-profile.model';


@Injectable()
export class ApplicationProfileAdapter implements Adapter<ApplicationProfileModel> {
  adapt(item: any): ApplicationProfileModel {
    return new ApplicationProfileModel(
      item.id,
      item.applicationName,
      item.applicationNameLocal,
      item.applicationEmail,
      item.applicationUrl,
      item.address,
      item.addressLocal,
      item.contactNumber,
      item.applicationStartDate,
      item.status,
      item.fullLogo,
      item.shortLogo,
      item.favIcon,
      item.entryDate,
      item.entryusername,
      item.entryuserid,
      item.statuschangedate,
      item.statuschangeuserid)

  }
}


export class ApplicationProfileFormAdapter implements Adapter<ApplicationProfileFormModel> {
  adapt(item: any): ApplicationProfileFormModel {
    return new ApplicationProfileFormModel(
      item.id,
      item.applicationName,
      item.applicationNameLocal,
      item.applicationEmail,
      item.applicationUrl,
      item.address,
      item.addressLocal,
      item.contactNumber,
      item.applicationStartDate,
      item.status,
      item.fullLogo,
      item.shortLogo,
      item.favIcon

    );
  }
}
