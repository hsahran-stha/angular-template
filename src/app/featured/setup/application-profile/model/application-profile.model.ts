export class ApplicationProfileModel {
  constructor(
    public id: number,
    public applicationName: number,
    public applicationNameLocal: string,
    public applicationEmail: string,
    public applicationUrl: string,
    public address: string,
    public addressLocal: string,
    public contactNumber: string,
    public applicationStartDate: string,
    public status: number,
    public fullLogo: string,
    public shortLogo: string,
    public favIcon: string,
    public entrydate: number,
    public entryusername: string,
    public entryUserId: number,
    public statusChangeDate: number,
    public statusChangeUserId: number,
  ) {
  }

}
export class ApplicationProfileFormModel {
  constructor(
    public id: number,
    public applicationName: string,
    public applicationNameLocal: string,
    public applicationEmail: string,
    public applicationUrl: string,
    public address: string,
    public addressLocal: string,
    public contactNumber: string,
    public applicationStartDate: string,
    public status: number,
    public fullLogo: string,
    public shortLogo: string,
    public favIcon: string
) {
  }

}
