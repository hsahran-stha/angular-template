import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ApplicationProfileRoutingModule} from './application-profile-routing.module';
import {SharedModule} from '../../../shared/shared.module';
import {ApplicationProfileService} from './application-profile.service';
import {ApplicationProfileAdapter, ApplicationProfileFormAdapter} from './model/application-profile.adapter';
import {ApplicationProfileListComponent} from './application-profile-list/application-profile-list.component';
import {ApplicationProfileFormComponent} from './application-profile-form/application-profile-form.component';


@NgModule({
  declarations: [ApplicationProfileListComponent, ApplicationProfileFormComponent],
  imports: [
    CommonModule, SharedModule,
    ApplicationProfileRoutingModule
  ],
  providers: [ApplicationProfileService, ApplicationProfileAdapter, ApplicationProfileFormAdapter]
})
export class ApplicationProfileModule {
}
