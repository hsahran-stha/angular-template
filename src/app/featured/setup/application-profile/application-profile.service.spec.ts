import { TestBed } from '@angular/core/testing';

import { ApplicationProfileService } from './application-profile.service';

describe('ApplicationProfileService', () => {
  let service: ApplicationProfileService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApplicationProfileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
