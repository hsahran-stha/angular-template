import {Injectable, Injector} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AppService} from '../../../shared/service/app.service';
import {ApplicationProfileAdapter, ApplicationProfileFormAdapter} from './model/application-profile.adapter';
import {ApplicationProfileModel} from './model/application-profile.model';

@Injectable()
export class ApplicationProfileService extends AppService {
  private moduleName = 'applicationProfile';
  private getUrl = `${environment.API_URL}${this.moduleName}/getAll`;
  private getByIdUrl = `${environment.API_URL}${this.moduleName}/getById`;
  private putUrl = `${environment.API_URL}${this.moduleName}/update`;
  private postUrl = `${environment.API_URL}${this.moduleName}/create`;
  private deleteUrl = `${environment.API_URL}${this.moduleName}/delete/`;

  constructor(public injector: Injector, private adapter: ApplicationProfileAdapter,
              private formAdapter: ApplicationProfileFormAdapter) {
    super(injector);
  }

  get(): Observable<ApplicationProfileModel[]> {
    return this.httpClient.get<ApplicationProfileModel[]>(this.getUrl).pipe(
      map((data: ApplicationProfileModel[]) => {
        return data['data'].map((item: ApplicationProfileModel) => this.adapter.adapt(item));
      }));
  }

  post(data: object): Observable<object> {
    return this.httpClient.post<object>(this.postUrl, this.formAdapter.adapt(data)).pipe();
  }

  put(data: object, id: number): Observable<object> {
    return this.httpClient.put<object>(`${this.putUrl}`, this.formAdapter.adapt(data)).pipe();
  }

  delete(id: number): Observable<object> {
    return this.httpClient.delete<object>(`${this.deleteUrl}${id}`).pipe();
  }

  getById(id: number): Observable<ApplicationProfileModel> {
    return this.httpClient.get(`${this.getByIdUrl}/${id}`).pipe(map(data => this.adapter.adapt(data['data'])));
  }
}


