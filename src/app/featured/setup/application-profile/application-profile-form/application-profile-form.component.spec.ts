import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationProfileFormComponent } from './application-profile-form.component';

describe('ApplicationProfileFormComponent', () => {
  let component: ApplicationProfileFormComponent;
  let fixture: ComponentFixture<ApplicationProfileFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApplicationProfileFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationProfileFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
