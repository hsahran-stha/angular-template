import {Component, ElementRef, Injector, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {Validators} from '@angular/forms';
import {ApplicationProfileController} from '../application-profile.controller';
import {catchError, filter, map, take} from 'rxjs/operators';
import {ApplicationProfileModel} from '../model/application-profile.model';
import {fileExtensionType} from '../../../../shared/enums/fileExtentionType';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-application-profile-form',
  templateUrl: './application-profile-form.component.html',
  styleUrls: ['./application-profile-form.component.scss']
})
export class ApplicationProfileFormComponent extends ApplicationProfileController implements OnInit, OnDestroy {

  constructor(public  injector: Injector, private dom: DomSanitizer) {
    super(injector);

    this.activatedRoute.params.subscribe(
      (params) => {
        if (Object.keys(params).length > 0) {
          this.routeId = parseInt(atob(params['id']));
        }
      }
    );
  }

  ngOnInit(): void {
    this.initializeFormStructure();

    if (this.routeId) {
      if (this.getPage() == 'view') {
        this.formStructure.disable();
        this.buttonAccess['save'] = false;
        this.buttonAccess['reset'] = false;
      }

      if (this.getPage() == 'edit') {
        this.buttonAccess['save'] = false;
        this.buttonAccess['reset'] = false;
        this.buttonAccess['update'] = true;
      }

      if (this.getPage() == 'delete') {
        this.formStructure.disable();
        this.buttonAccess['save'] = false;
        this.buttonAccess['reset'] = false;
        this.buttonAccess['delete'] = true;
      }

      this.fetchDataById();
    }
  }


  fetchDataById(): void {
    this.applicationProfileService.getById(this.routeId).pipe(
      map(data => {
        this.formStructure.patchValue(data);
        this.tempFaviconLogoFile = data['favIcon'];
        this.tempFullLogoFile = data['fullLogo'];
        this.tempShortLogoFile = data['shortLogo'];

      })
    ).subscribe();
  }

  initializeFormStructure(): void {
    this.formStructure = this.formBuilder.group({
      id: [''],
      applicationName: ['', Validators.required],
      applicationNameLocal: ['', Validators.required],
      applicationEmail: ['', [Validators.required, Validators.email]],
      applicationUrl: ['', Validators.required],
      address: ['', Validators.required],
      addressLocal: ['', Validators.required],
      contactNumber: ['', Validators.required],
      applicationStartDate: ['', Validators.required],
      fullLogo: ['', Validators.required],
      shortLogo: ['', Validators.required],
      favIcon: ['', Validators.required],
      status: ['', Validators.required],
    });


  }

  fetchData(): void {
    this.applicationProfileService.get().pipe(
      filter(data => data['status'] !== false),
      map((data: ApplicationProfileModel[]) => {
        this.fetchedData = data;
      }),
      catchError(err => {
        console.log(err);
        return err;
      })
    ).subscribe();

  }

  resetClicked($event): void {
    if ($event) {
      this.buttonAccess['save'] = true;
      this.buttonAccess['update'] = false;
      this.buttonAccess['delete'] = false;
      this.formStructure.reset();
      this.formStructure.enable();
      this.confirmDeletionService.drop();

    }
  }

  onSubmit(): void {
    if (this.formStructure.invalid) {
      this.validateAllFormFields(this.formStructure);
      this.displayInvalidFormControls(this.formStructure);
      return;
    }

    // to show confirmation box
    let obj = {
      body: this.getCRUDSubmitConfMessage(this.formStructure.get('id').value),
      btnClass: 'btn-primary',
      alertClass: 'ic-save',
      buttonName: this.formStructure.get('id').value ? this.translate.instant('update') : this.translate.instant('save')
    };
    this.confirmDialogBoxService.show(obj);
    const processConfirmation$ = this.confirmDialogBoxService.getProcessConfirmation();
    processConfirmation$.pipe(take(1)).subscribe((val) => {
      if (val) {
        this.formStructure.patchValue({
          applicationStartDate: this.translate.currentLang == 'np' ? this.nepaliDateConverter.BStoAD(this.formStructure.get('applicationStartDate').value) : this.formStructure.get('applicationStartDate').value,
        });
        if (this.formStructure.get('id').value) {
          this.applicationProfileService.put(this.formStructure.value, this.formStructure.get('id').value).subscribe((res) => {
            this.afterSubmitSuccess(res);
          }, error => {
            this.formStructure.patchValue({
              applicationStartDate: this.translate.currentLang == 'np' ? this.nepaliDateConverter.ADtoBS(this.formStructure.get('applicationStartDate').value) : this.formStructure.get('applicationStartDate').value,
            });
            return error;
          });
        } else {
          this.applicationProfileService.post(this.formStructure.value).subscribe((res) => {
            this.afterSubmitSuccess(res);
          }, error => {
            this.formStructure.patchValue({
              applicationStartDate: this.translate.currentLang == 'np' ? this.nepaliDateConverter.ADtoBS(this.formStructure.get('applicationStartDate').value) : this.formStructure.get('applicationStartDate').value,
            });
            return error;
          });
        }
      }
    });
  }

  afterSubmitSuccess(res?: object): void {
    this.formStructure.reset();
    this.alertify.message(res['message']);
    this.buttonAccess['save'] = true;
    this.buttonAccess['update'] = false;
    this.location.back();

  }

  deleteClicked($event): void {
    // to show confirmation box
    let obj = {
      body: this.getCRUDSubmitConfMessage(this.formStructure.get('id').value, true),
      btnClass: 'btn-danger',
      alertClass: 'ic-alert',
      buttonName: this.translate.instant('confirm')
    };
    this.confirmDialogBoxService.show(obj);
    const processConfirmation$ = this.confirmDialogBoxService.getProcessConfirmation();
    processConfirmation$.pipe(take(1)).subscribe((val) => {
      if (val) {
        this.applicationProfileService.delete(this.routeId).subscribe((res) => {
          this.fetchData();
          this.location.back();
          this.confirmDeletionService.drop();
          this.alertify.message(res['message']);
          this.formStructure.reset();
          this.formStructure.enable();
          this.buttonAccess['delete'] = false;
          this.buttonAccess['save'] = true;
        });

      }
    });
  }

  processCompanyLogoFile(imageInput: any, fieldName) {
    if (fieldName == 'shortLogo') {
      this.tempShortLogoFileName = ''; // current selected file name displays in text box
      this.tempShortLogoFile = ''; // current selected file visible in text box
    } else if (fieldName == 'fullLogo') {
      this.tempFullLogoFileName = ''; // current selected file name displays in text box
      this.tempFullLogoFile = ''; // current selected file visible in text box
    } else {
      this.tempFaviconLogoFileName = ''; // current selected file name displays in text box
      this.tempFaviconLogoFile = ''; // current selected file visible in text box
    }

    let file: File = imageInput.files[0];
    const reader = new FileReader();

    let fileExtension: string;

    if (!file) {
      return;
    }
    let maxfilesizeinkb: number = 30;
    if (fieldName == 'shortLogo') {
      maxfilesizeinkb = 80;
    } else if (fieldName == 'fullLogo') {
      maxfilesizeinkb = 100;
    }
    if (parseInt(String(file.size / 1024)) > parseInt(String(maxfilesizeinkb))) {
      this.alertify.error(this.translate.instant(`Size of selected file is ${parseInt(String(file.size / 1024))} KB. Maximum file size limit is ${maxfilesizeinkb} KB`));
      return;
    }

    fileExtension = file.name.split('.')[1].toLowerCase();
    if (fileExtension == fileExtensionType.JPG ||
      fileExtension == fileExtensionType.JPEG ||
      fileExtension == fileExtensionType.GIF ||
      fileExtension == fileExtensionType.PNG) {
      console.log('allowed to upload');
    } else {
      this.alertify.error(this.translate.instant('documentType.extensionValidation'));
      return;
    }

    reader.addEventListener('load', (event: any) => {
      let selectedShortLogoFile = new FileSnippet(event.target.result, file);
      if (fieldName == 'shortLogo') {
        this.tempShortLogoFile = selectedShortLogoFile.src;
        this.tempShortLogoFileName = selectedShortLogoFile.file.name;
      } else if (fieldName == 'fullLogo') {
        this.tempFullLogoFile = selectedShortLogoFile.src;
        this.tempFullLogoFileName = selectedShortLogoFile.file.name;
      } else {
        this.tempFaviconLogoFile = selectedShortLogoFile.src;
        this.tempFaviconLogoFileName = selectedShortLogoFile.file.name;
      }

      if (fieldName == 'shortLogo') {
        this.formStructure.patchValue({shortLogo: this.tempShortLogoFile});
      } else if (fieldName == 'fullLogo') {
        this.formStructure.patchValue({fullLogo: this.tempFullLogoFile});

      } else {
        this.formStructure.patchValue({favIcon: this.tempFaviconLogoFile});
      }

    });
    reader.readAsDataURL(file);
  }


  @ViewChild('contentimglg') contentimglg: ElementRef;

  showImage(filename) {
    if (this.getPage() != 'view' && this.getPage() != 'delete') {
      return;
    }
    this.displayImage = this.dom.bypassSecurityTrustUrl(filename);
    this.openLg(this.contentimglg);
  }

  ngOnDestroy() {
  }


}

class FileSnippet {
  constructor(public src: string, public file: File) {
  }
}
