import {Component, Injector, OnInit} from '@angular/core';
import {ApplicationProfileController} from '../application-profile.controller';
import {filter, map} from 'rxjs/operators';


@Component({
  selector: 'app-application-profile-list',
  templateUrl: './application-profile-list.component.html',
  styleUrls: ['./application-profile-list.component.scss']
})
export class ApplicationProfileListComponent extends ApplicationProfileController implements OnInit {

  constructor(public  injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.manageColumn();
    this.fetchData();

  }

  manageColumn(): void {
    this.columns = [
      this.translate.currentLang == 'np' ?
        this.manageEachColumn('applicationNameLocal', 'applicationNameLocal', true) :
        this.manageEachColumn('applicationName', 'applicationName', true),
      this.manageEachColumn('applicationEmail', 'applicationEmail', true),
      this.manageEachColumn('applicationUrl', 'applicationUrl', true),
      this.translate.currentLang == 'np' ?
        this.manageEachColumn('addressLocal', 'addressLocal', true) :
        this.manageEachColumn('address', 'address', true),
      this.manageEachColumn('contactNo', 'contactNumber', true),
      this.manageEachColumn('applicationStartDate', 'applicationStartDate', true),
      this.manageEachColumn('status', 'status', true),
      this.manageEachColumn('entryUserName', 'entryusername', true),
      this.manageEachColumn('entryDate', 'entrydate', true),
      this.manageEachColumn('action', '', false, false, '', true, this),
    ];
  }

  fetchData(): void {
    this.applicationProfileService.get().pipe(
      filter(data => data['status'] !== false),
      map(data => {
        this.fetchedData = data;
      })
    ).subscribe();
  }


  redirectToCreate(): void {
    this.router.navigate(['create'], {relativeTo: this.activatedRoute});
  }

  actionView(ui): void {
    this.rowData = ui.rowData;
    this.router.navigate(['view', btoa(this.rowData['id'])], {relativeTo: this.activatedRoute});
  }

  actionEdit(ui): void {
    this.rowData = ui.rowData;
    this.router.navigate(['edit', btoa(this.rowData['id'])], {relativeTo: this.activatedRoute});
  }

  actionDelete(ui): void {
    this.rowData = ui.rowData;
    this.router.navigate(['delete', btoa(this.rowData['id'])], {relativeTo: this.activatedRoute});
  }


}
