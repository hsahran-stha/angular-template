import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationProfileListComponent } from './application-profile-list.component';

describe('ApplicationProfileListComponent', () => {
  let component: ApplicationProfileListComponent;
  let fixture: ComponentFixture<ApplicationProfileListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApplicationProfileListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationProfileListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
