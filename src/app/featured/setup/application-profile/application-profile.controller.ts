import {Injector} from '@angular/core';
import {SetupController} from '../setup.controller';
import {FormGroup} from '@angular/forms';
import {ApplicationProfileService} from './application-profile.service';
import {SafeUrl} from '@angular/platform-browser';


export class ApplicationProfileController extends SetupController {

  public columns;
  public fetchedData;
  public formStructure: FormGroup;
  public applicationProfileService: ApplicationProfileService;
  public buttonAccess: object = {save: true, exit: true};
  public rowData: object;


  public tempShortLogoFile: string;
  public tempShortLogoFileName: string;
  public tempFullLogoFile: string;
  public tempFullLogoFileName: string;
  public tempFaviconLogoFile: string;
  public tempFaviconLogoFileName: string;

  public displayImage: SafeUrl;


  constructor(public injector: Injector) {
    super(injector);
    this.applicationProfileService = this.injector.get(ApplicationProfileService);
  }


}
