import {Injectable} from '@angular/core';
import {Adapter} from '../../../../core/adapter';
import {FiscalYearFormModel, FiscalYearModel} from './fiscal-year.model';


@Injectable()
export class FiscalYearAdapter implements Adapter<FiscalYearModel> {
  adapt(item: any): FiscalYearModel {
    return new FiscalYearModel(
      item.id,
      item.fiscalyear,
      item.startdate,
      item.enddate,
      item.status,
      item.entrydate,
      item.entryuserid,
      item.entryusername,
      item.statuschangedate,
      item.statuschangeuserid);
  }
}

export class FiscalYearFormAdapter implements Adapter<FiscalYearFormModel> {
  adapt(item: any): FiscalYearFormModel {
    return new FiscalYearFormModel(
      item.id,
      item.fiscalyear,
      item.startdate,
      item.enddate,
      item.status,
    );
  }
}
