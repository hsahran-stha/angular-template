export class FiscalYearModel {
  constructor(
    public id: number,
    public fiscalyear: number,
    public startdate: string,
    public enddate: string,
    public status: number,
    public entrydate: number,
    public entryuserid: number,
    public entryusername: string,
    public statuschangedate: number,
    public statuschangeuserid: number,
  ) {
  }

}


export class FiscalYearFormModel {
  constructor(
    public id: number,
    public fiscal_year: number,
    public start_date: string,
    public end_date: string,
    public status: string,
  ) {
  }

}


