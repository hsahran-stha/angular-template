import {Injector} from '@angular/core';
import {SetupController} from '../setup.controller';
import {FormGroup} from '@angular/forms';
import {FiscalYearService} from './fiscal-year.service';

export class FiscalYearController extends SetupController {

  public columns;
  public fetchedData;
  public formStructure: FormGroup;
  public fiscalYearService: FiscalYearService;
  public buttonAccess: object = {save: true, reset: true};
  public rowData: object;

  constructor(public injector: Injector) {
    super(injector);
    this.fiscalYearService = this.injector.get(FiscalYearService);

  }

}
