import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FiscalYearRoutingModule} from './fiscal-year-routing.module';
import {FiscalYearComponent} from './fiscal-year/fiscal-year.component';
import {SharedModule} from '../../../shared/shared.module';
import {EffectsModule} from "@ngrx/effects";
import {FiscalYearEffect} from "../../../core/_effects/fiscalyear.effect";


@NgModule({
  declarations: [FiscalYearComponent],
  imports: [
    CommonModule,
    SharedModule,
    FiscalYearRoutingModule,
    // EffectsModule.forFeature([FiscalYearEffect])
  ],
  providers: []
})
export class FiscalYearModule {
}
