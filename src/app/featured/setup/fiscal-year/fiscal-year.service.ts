import {Injectable, Injector} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AppService} from '../../../shared/service/app.service';
import {environment} from '../../../../environments/environment';
import {FiscalYearModel} from './model/fiscal-year.model';
import {FiscalYearAdapter, FiscalYearFormAdapter} from './model/fiscal-year.adapter';

@Injectable()
export class FiscalYearService extends AppService {
  private moduleName = 'fiscalyear';
  private getUrl = `${environment.API_URL}${this.moduleName}/list/-2`;
  private postUrl = `${environment.API_URL}${this.moduleName}/save`;
  private deleteUrl = `${environment.API_URL}${this.moduleName}/deletebyid/`;

  constructor(public injector: Injector, private adapter: FiscalYearAdapter,
              private formAdapter: FiscalYearFormAdapter) {
    super(injector);
  }

  get(): Observable<FiscalYearModel[]> {
    return this.httpClient.get<FiscalYearModel[]>(this.getUrl).pipe(
      map((data: FiscalYearModel[]) => {
        return data['data'].map((item: FiscalYearModel) => this.adapter.adapt(item));
      }));
  }

  post(data: object): Observable<object> {
    return this.httpClient.post<object>(this.postUrl, this.formAdapter.adapt(data)).pipe();
  }

  put(data: object, id: number): Observable<object> {
    return this.httpClient.post<object>(`${this.postUrl}`, this.formAdapter.adapt(data)).pipe();
  }

  delete(id: number): Observable<object> {
    return this.httpClient.delete<object>(`${this.deleteUrl}${id}`).pipe();
  }

  getById(id: number): Observable<FiscalYearModel> {
    return this.httpClient.get(`${this.getUrl}/${id}`).pipe(map(data => this.adapter.adapt(data)));
  }

}
