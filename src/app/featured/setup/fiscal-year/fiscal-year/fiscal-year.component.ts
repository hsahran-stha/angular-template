import {Component, Injector, OnDestroy, OnInit} from '@angular/core';
import {FiscalYearController} from '../fiscal-year.controller';
import {catchError, filter, map, take} from 'rxjs/operators';
import {FiscalYearModel} from '../model/fiscal-year.model';
import {Validators} from '@angular/forms';
import {FiscalYearValidator} from '../../../../shared/validators/fiscalyear.validator';
import {getFiscalYear} from "../../../../core/_selectors/fiscalyear.selector";
import {FiscalYearRequestListAction} from "../../../../core/_actions/fiscalyear.action";
import {FiscalYearRepository} from "../../../../core/_repositories/fiscalyear.repository";
import {parseJson} from "@angular/cli/utilities/json-file";

@Component({
  selector: 'app-fiscal-year',
  templateUrl: './fiscal-year.component.html',
  styleUrls: ['./fiscal-year.component.scss']
})
export class FiscalYearComponent extends FiscalYearController implements OnInit, OnDestroy {

  constructor(public injector: Injector, private fiscalYearRepository: FiscalYearRepository) {
    super(injector);

  }

  ngOnInit(): void {
    this.manageColumn();
    this.initializeFormStructure();
    this.fetchData();
  }

  manageColumn(): void {
    this.columns = [
      this.manageEachColumn('fiscalYear', 'fiscalyear', true),
      this.manageEachColumn('startDate', 'startdate', true),
      this.manageEachColumn('endDate', 'enddate', true),
      this.manageEachColumn('status', 'status', true),
      this.manageEachColumn('entryUserName', 'entryusername', true),
      this.manageEachColumn('entryDate', 'entrydate', true),
      this.manageEachColumn('action', '', false, false, '', true, this),
    ];
  }

  actionView(ui): void {
    this.rowData = ui.rowData;
    this.patchValue();

    this.buttonAccess['save'] = false;
    this.buttonAccess['update'] = false;
    this.buttonAccess['delete'] = false;

    this.formStructure.disable();
    this.confirmDeletionService.drop();

  }

  actionEdit(ui): void {
    this.rowData = ui.rowData;
    this.patchValue();

    this.buttonAccess['update'] = true;
    this.buttonAccess['save'] = false;
    this.buttonAccess['delete'] = false;

    this.formStructure.enable();
    this.confirmDeletionService.drop();

  }

  actionDelete(ui): void {
    this.rowData = ui.rowData;
    this.patchValue();

    this.buttonAccess['save'] = false;
    this.buttonAccess['update'] = false;
    this.buttonAccess['delete'] = true;

    this.formStructure.disable();
    this.confirmDeletionService.show();
  }

  patchValue(): void {
    this.formStructure.patchValue({
      id: this.rowData['id'],
      fiscalyear: this.rowData['fiscalyear'],
      startdate: this.rowData['startdate'],
      enddate: this.rowData['enddate'],
      status: this.rowData['status'],
    });
  }

  initializeFormStructure(): void {
    this.formStructure = this.formBuilder.group({
      id: [''],
      fiscalyear: ['', [Validators.required, FiscalYearValidator.validText]],
      startdate: ['', Validators.required,],
      enddate: ['', Validators.required],
      status: ['', Validators.required],
    });
  }

  onSubmit(): void {
    if (this.formStructure.invalid) {
      this.validateAllFormFields(this.formStructure);
      this.displayInvalidFormControls(this.formStructure);
      return;
    }

    // to show confirmation box
    let obj = {
      body: this.getCRUDSubmitConfMessage(this.formStructure.get('id').value),
      btnClass: 'btn-primary',
      alertClass: 'ic-save',
      buttonName: this.formStructure.get('id').value ? this.translate.instant('update') : this.translate.instant('save')
    };
    this.confirmDialogBoxService.show(obj);
    const processConfirmation$ = this.confirmDialogBoxService.getProcessConfirmation();
    processConfirmation$.pipe(take(1)).subscribe((val) => {
      if (val) {
        this.formStructure.patchValue({
          startdate: this.translate.currentLang == 'np' ? this.nepaliDateConverter.BStoAD(this.formStructure.get('startdate').value) : this.formStructure.get('startdate').value,
          enddate: this.translate.currentLang == 'np' ? this.nepaliDateConverter.BStoAD(this.formStructure.get('enddate').value) : this.formStructure.get('enddate').value,
        });
        if (this.formStructure.get('id').value) {
          this.fiscalYearService.put(this.formStructure.value, this.formStructure.get('id').value)
            .subscribe((res) => {
              this.afterSubmitSuccess(res);
            });
        } else {
          this.fiscalYearService.post(this.formStructure.value)
            .subscribe((res) => {
              this.afterSubmitSuccess(res);
            });
        }
      }
    });
  }

  afterSubmitSuccess(res?: object): void {
    this.fetchData();
    this.alertify.message(res['message']);
    this.formStructure.reset();
    this.formStructure.patchValue({status: ''});
    this.buttonAccess['save'] = true;
    this.buttonAccess['update'] = false;
  }


  fetchData(): void {
    // this.store.dispatch(new FiscalYearRequestListAction());
    //
    // this.store.select(getFiscalYear).pipe(
    //   map(data => {
    //     if (data) {
    //       alert('here');
    //       this.fetchedData = JSON.parse(JSON.stringify(data))
    //     }
    //   })
    // ).subscribe();
    this.fiscalYearRepository.getAll().then(data => {
      this.fetchedData = JSON.parse(JSON.stringify(data));
    });

    // this.fiscalYearService.get().pipe(
    //   filter(data => data['status'] !== false),
    //   map((data: FiscalYearModel[]) => {
    //     this.fetchedData = data;
    //   }),
    //   catchError(err => {
    //     console.log(err);
    //     return err;
    //   })
    // ).subscribe();

  }

  resetClicked($event): void {
    if ($event) {
      this.buttonAccess['save'] = true;
      this.buttonAccess['update'] = false;
      this.buttonAccess['delete'] = false;
      this.formStructure.reset();
      this.formStructure.patchValue({status: ''});
      this.formStructure.enable();
      this.confirmDeletionService.drop();

    }
  }

  deleteClicked($event): void {

    // to show confirmation box
    let obj = {
      body: this.getCRUDSubmitConfMessage(this.formStructure.get('id').value, true),
      btnClass: 'btn-danger',
      alertClass: 'ic-alert',
      buttonName: this.translate.instant('confirm')
    };
    this.confirmDialogBoxService.show(obj);
    const processConfirmation$ = this.confirmDialogBoxService.getProcessConfirmation();
    processConfirmation$.pipe(take(1)).subscribe((val) => {
      if (val) {
        this.fiscalYearService.delete(this.rowData['id']).subscribe((res) => {
          this.fetchData();
          this.alertify.message(res['message']);
          this.confirmDeletionService.drop();
          this.formStructure.reset();
          this.formStructure.patchValue({status: ''});
          this.formStructure.enable();
          this.buttonAccess['delete'] = false;
          this.buttonAccess['save'] = true;
        });

      }
    });
  }

  ngOnDestroy() {
  }

}
