import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SetupComponent} from './setup/setup.component';
import {PermissionsGuard} from '../../guards/permission.guard';

const routes: Routes = [
  {
    path: '',
    component: SetupComponent,
    children: [
      {
        path: 'office',
        canActivateChild: [PermissionsGuard],
        loadChildren: () =>
          import('./office/office.module')
            .then((m) => m.OfficeModule),
      },

      {
        path: 'fiscal-year',
        canActivateChild: [PermissionsGuard],
        loadChildren: () =>
          import('./fiscal-year/fiscal-year.module')
            .then((m) => m.FiscalYearModule),
      },
      {
        path: 'district',
        canActivateChild: [PermissionsGuard],
        loadChildren: () =>
          import('./district/district.module')
            .then((m) => m.DistrictModule),
      },
      {
        path: 'local-body',
        canActivateChild: [PermissionsGuard],
        loadChildren: () =>
          import('./local-body/local-body.module')
            .then((m) => m.LocalBodyModule),
      },
      {
        path: 'document-type',
        canActivateChild: [PermissionsGuard],
        loadChildren: () =>
          import('./document-type/document-type.module')
            .then((m) => m.DocumentTypeModule),
      },
      {
        path: 'application-profile',
        canActivateChild: [PermissionsGuard],
        loadChildren: () =>
          import('./application-profile/application-profile.module')
            .then((m) => m.ApplicationProfileModule),
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SetupRoutingModule {
}
