import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FeaturedRoutingModule} from './featured-routing.module';
import {FeaturedComponent} from './featured/featured.component';
import {SystemHeaderComponent} from './layouts/system-header/system-header.component';
import {SystemSidebarComponent} from './layouts/system-sidebar/system-sidebar.component';
import {SharedModule} from '../shared/shared.module';
import {OfficeService} from './setup/office/office.service';
import {OfficeAdapter, OfficeFormAdapter} from './setup/office/model/office.adapter';
import {FiscalYearService} from './setup/fiscal-year/fiscal-year.service';
import {FiscalYearAdapter, FiscalYearFormAdapter} from './setup/fiscal-year/model/fiscal-year.adapter';
import {LocalBodyService} from './setup/local-body/local-body.service';
import {LocalBodyAdapter, LocalBodyFormAdapter} from './setup/local-body/model/local-body.adapter';
import {DistrictService} from './setup/district/district.service';
import {DistrictAdapter, DistrictFormAdapter} from './setup/district/model/district.adapter';
import {EffectsModule} from "@ngrx/effects";
import {FiscalYearEffect} from "../core/_effects/fiscalyear.effect";


@NgModule({
  declarations: [
    FeaturedComponent,
    SystemHeaderComponent,
    SystemSidebarComponent],
  imports: [
    CommonModule,
    SharedModule,
    FeaturedRoutingModule,
    EffectsModule.forFeature([FiscalYearEffect])

  ],
  providers: [OfficeService, OfficeAdapter, OfficeFormAdapter,
    FiscalYearService, FiscalYearAdapter, FiscalYearFormAdapter,
    LocalBodyService, LocalBodyAdapter, LocalBodyFormAdapter,
    DistrictService, DistrictAdapter, DistrictFormAdapter]
})
export class FeaturedModule {
}
