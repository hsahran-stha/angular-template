import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FeaturedComponent} from './featured/featured.component';
import {AuthGuard} from '../guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    component: FeaturedComponent,
    children: [
      {
        path: '', redirectTo: 'dashboard', pathMatch: 'full',
      },
      {
        path: 'dashboard',
        loadChildren: () =>
          import('./dashboard/dashboard.module')
            .then((m) => m.DashboardModule),
      },
      {
        path: 'setup',
        loadChildren: () =>
          import('./setup/setup.module')
            .then((m) => m.SetupModule),
      },
      {
        path: 'security',
        loadChildren: () =>
          import('./security/security.module')
            .then((m) => m.SecurityModule),
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeaturedRoutingModule {
}
