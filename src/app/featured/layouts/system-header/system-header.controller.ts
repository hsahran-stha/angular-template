import {Injector} from '@angular/core';

import {FeaturedController} from '../../featured.controller';
import {SystemHeaderService} from './system-header.service';

export class SystemHeaderController extends FeaturedController {
  public systemHeaderService: SystemHeaderService;
  public routeId: number;
  public columns;
  public fetchedData;
  public notificationDetails: object[];

  constructor(public injector: Injector) {
    super(injector);
    this.systemHeaderService = this.injector.get(SystemHeaderService);
  }


}
