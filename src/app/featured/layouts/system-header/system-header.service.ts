import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {Observable, Subject} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SystemHeaderService {
  private getUrl: string = `${environment.API_URL}notification/getMyNotifications/`;
  private seenUrl: string = `${environment.API_URL}notification/seen/`;
  private countNotificationUrl: string = `${environment.API_URL}notification/getMyNotificationCount`;
  private getNotificationDetailUrl: string = `${environment.API_URL}notification/getMyNotifications/2`;
  private countSubject = new Subject();

  constructor(private httpClient: HttpClient) {
  }


  get(id: number = -2): Observable<object[]> {
    return this.httpClient.get<object[]>(`${this.getUrl}${id}`).pipe(
      map((data: object[]) => {
        return data['data'].map((item: object) => item);
      }));
  }

  seenNotification(id: number): Observable<object> {
    return this.httpClient.post(`${this.seenUrl}${id}`, {}).pipe();
  }

  countNotification(): Observable<object> {
    return this.httpClient.get(this.countNotificationUrl).pipe(map(data => data['data']));
  }

  getNotificationDetail(): Observable<object> {
    return this.httpClient.get(this.getNotificationDetailUrl).pipe(map(data => data['data']));
  }

  changePwd(data: any) {
    let url = environment.API_URL + 'password/new';
    return this.httpClient.post<any>(url, data).pipe();
  }

  getCount(): Observable<any> {
    return this.countSubject.asObservable();
  }

  setCount(message): void {
    this.countSubject.next(message);
  }
}
