import {Component, ElementRef, Injector, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormGroup, Validators} from '@angular/forms';
import {catchError, map} from 'rxjs/operators';
import {SystemHeaderController} from './system-header.controller';

@Component({
  selector: 'app-system-header',
  templateUrl: './system-header.component.html',
  styleUrls: ['./system-header.component.scss'],
})
export class SystemHeaderComponent extends SystemHeaderController implements OnInit, OnDestroy {

  // FormGroup Declaration
  changePwdForm: FormGroup;

  // pwd mismatch flag
  pwdMismatch: boolean = false;

  /**
   * flag to check new pwd and confirm pwd
   */
  isPasswordSame: boolean;

  /**
   * flag to check old pwd is correct
   */
  invalidOldPwd: boolean = false;

  /**
   * invlaid old pwd msg
   */
  invalidOldMsg: string;

  public timerOfNotificationCount;

  constructor(public inj: Injector) {
    super(inj);
  }

  ngOnInit(): void {
    this.fetchNotificationCount();
    this.timerOfNotificationCount = setInterval(() => {
      this.fetchNotificationCount();
    }, 10000);
    this.systemHeaderService.getCount().pipe(
      map(data => {
        this.fetchNotificationCount();
      })
    ).subscribe();
    this.manageColumn();
    this.changePwdForm = this.formBuilder.group({
        password: ['', Validators.required],
        oldPassword: ['', Validators.required],
        retypePassword: ['', Validators.compose([Validators.required])],
      },
    );
  }

  manageColumn(): void {
    this.columns = [
      this.manageEachColumn('isviewed', 'isviewed', true),
      this.manageEachColumn('title', 'title', true),
      this.manageEachColumn('message', 'message', true),
    ];
  }


  /**
   * fn to handle password change
   */
  public onPasswordChange(): void {
    if (this.changePwdForm.invalid) {
      this.validateAllFormFields(this.changePwdForm);
      this.displayInvalidFormControls(this.changePwdForm);
      return;
    }

    let pwd = this.changePwdForm.get('password').value;
    let retype = this.changePwdForm.get('retypePassword').value;
    if (pwd !== retype) {
      this.pwdMismatch = true;
      setTimeout(() => {
        this.pwdMismatch = false;
      }, 2000);
      return;
    }

    this.systemHeaderService.changePwd(this.changePwdForm.value).subscribe(
      (response) => {
        if (response.status) {
          this.alertify.success(response.message);
          this.modalService.dismissAll();
          this.authService.userLogout();
        } else {
          this.invalidOldPwd = true;
          this.invalidOldMsg = response.message;
          setTimeout(() => {
            this.invalidOldPwd = false;
          }, 2000);
        }
      }, (error) => {
        this.alertify.error(error);
      }
    );
  }

  checkPassword(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];
      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }
      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({mustMatch: true});
        this.isPasswordSame = (matchingControl.status == 'VALID');
      } else {
        matchingControl.setErrors(null);
        this.isPasswordSame = (matchingControl.status == 'VALID');
      }
    };
  }

  fetchNotificationCount(): void {
    this.systemHeaderService.countNotification().pipe(
      map(data => {
        this.notificationCount = data['unseencount'];
        this.fetchNotifications();

      })
    ).subscribe();
  }

  @ViewChild('notification') notification: ElementRef;

  showNotification($event): void {

    this.systemHeaderService.seenNotification($event['notificatiionid']).subscribe((res) => {
      if (res['status']) {
        this.fetchedData = [$event];
        this.openLg(this.notification, 'md modal-notification');
        this.fetchNotificationCount();
      }
    });


  }

  fetchNotifications(): void {
    this.systemHeaderService.getNotificationDetail().pipe(
      map((data: object[]) => {
        if (this.translate.currentLang == 'np') {
          data.forEach(i => {
            i['notificationdatetime'] = this.nepaliDateConverter.ADtoBS(i['notificationdatetime']);
          });
        }
        this.notificationDetails = data;
      }),
      catchError(err => {
        return err;
      })
    ).subscribe();
  }

  ngOnDestroy() {
    clearInterval(this.timerOfNotificationCount);
  }

}
