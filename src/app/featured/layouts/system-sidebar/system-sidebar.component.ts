import {Component, Injector, OnInit} from '@angular/core';
import {AppController} from '../../../app.controller';
import {FormGroup} from '@angular/forms';
import {Menu} from '../../../shared/models/auth/menu';
import {map} from 'rxjs/operators';

declare var $;

@Component({
  selector: 'app-system-sidebar',
  templateUrl: './system-sidebar.component.html',
  styleUrls: ['./system-sidebar.component.scss'],
})
export class SystemSidebarComponent extends AppController implements OnInit {
  public allMenus;
  public searchForm: FormGroup;

  constructor(private inj: Injector) {
    super(inj);
  }

  ngOnInit(): void {
    this.fetchMenus();
    this.searchForm = this.formBuilder.group({
      searchBox: ['']
    });
  }

  /**
   * Fetch Static Menus
   */
  public fetchMenus() {
    if (this.currentUser.hasCurrentBranch()) {
      const branchId: number = this.currentUser.getCurrentBranchID();
      this.authService.fetchMenus(branchId).pipe(
        map(data => {
          this.allMenus = data;
          this.currentUser.assignMenus(this.allMenus);
        })
      ).subscribe();
    }
  }


  /**
   * Search Menu By Code
   * @param $event
   */
  public searchMenuByCode($event) {
    let searchCode = this.ucwords($event.target.value);
    $('li').each(function() {
      if (searchCode == '') {
        $(this).css('visibility', 'visible');
        $(this).fadeIn(100, 'swing');
      } else if ($(this).text().search(new RegExp(searchCode, 'i')) < 0) {
        $(this).css('visibility', 'hidden');
        $(this).fadeOut(100, 'swing');
      } else {
        $(this).css('visibility', 'visible');
        $(this).fadeIn(100, 'swing');
      }
    });

  }


  public navigateToMenu(menu: Menu) {
    if (menu && menu.url) {
      this.currentUser.setCurrentMenu(menu);
    }
  }

  public navigateToDashboard(): void {
    this.router.navigate(['featured/dashboard']);
  }

}
