import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from 'src/environments/environment';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  private getUrl: string = `${environment.API_URL}dashboard/getData`;
  private getProgramRequestCountsByGrantTypeUrl: string = `${environment.API_URL}dashboard/fetchProgramRequestCountsByGrantType`;
  private getLatestProgramRequestsUrl: string = `${environment.API_URL}dashboard/fetchLatestProgramRequests/8`;
  private getLatestProgramAppropriationsUrl: string = `${environment.API_URL}dashboard/fetchLatestProgramAppropriations/8`;

  constructor(private httpClient: HttpClient) {
  }


  get(): Observable<object[]> {
    return this.httpClient.get<object[]>(`${this.getUrl}`).pipe(
      map((data: object[]) => {
        return data['data'];
      }));
  }

  getProgramRequestCountsByGrantType(): Observable<object[]> {
    return this.httpClient.get<object[]>(`${this.getProgramRequestCountsByGrantTypeUrl}`).pipe(
      map((data: object[]) => {
        return data['data'];
      })
    );
  }

  getLatestProgramRequests(): Observable<object[]> {
    return this.httpClient.get<object[]>(`${this.getLatestProgramRequestsUrl}`).pipe(
      map((data: object[]) => {
        return data['data'];
      })
    );
  }

  getLatestProgramAppropriations(): Observable<object[]> {
    return this.httpClient.get<object[]>(`${this.getLatestProgramAppropriationsUrl}`).pipe(
      map((data: object[]) => {
        return data['data'];
      })
    );
  }

}
