import {AfterViewInit, Component, Injector, OnInit} from '@angular/core';
import {Chart} from '../../../../assets/chartjs/dist/chart.js';
import {DashboardService} from '../dashboard.service';
import {map} from 'rxjs/operators';
import {FeaturedController} from '../../featured.controller';

declare var $;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends FeaturedController implements OnInit, AfterViewInit {

  public fetchedData;

  public latestProgramAppropriationsColumns;
  public latestProgramRequestsColumns;
  public fetchedLatestProgramAppropriations;
  public fetchedLatestProgramRequests;

  constructor(public injector: Injector, private dashboardService: DashboardService) {
    super(injector);
  }

  ngOnInit(): void {
    this.manageColumns();
  }

  dashboardDataSetup(): void {
    let className = ['cardbg-blue', 'cardbg-purple-20', 'cardbg-yellow', 'cardbg-green', 'cardbg-purple'];
    let iconName = ['settings', 'add-box-filled', 'add-box-filled', 'user', 'add-box-filled'];
    this.dashboardService.get().pipe(
      map((data: object[]) => {

        this.fetchedData = [];
        let index = 0;
        for (let i of Object.keys(data)) {

          if (i == 'totalgranttransferedamount' || i == 'totalrequestedamount' || i=='totalusers') {
            continue;
          }
          if (this.currentUser.isLocalBranch() && i == 'totalrequestedlocalbodies') {
            continue;
          }
          if ((this.currentUser.isHeadBranch() || this.currentUser.isMinistryBranch()) && i == 'totalappropriatedamount') {
            continue;
          }

          this.fetchedData.push({
            title: i,
            count: data[i],
            className: className[index],
            iconName: iconName[index],
          });
          index++;
        }
        if (this.currentUser.isHeadBranch() || this.currentUser.isMinistryBranch()) {
          this.getLatestProgramRequests();
        }
        if (this.currentUser.isLocalBranch()) {
          this.getLatestProgramAppropriations();
        }
      })
    ).subscribe();
  }


  ngAfterViewInit() {
    // piechart
    let that = this;
    const pieChart: any = document.getElementById('pie-chart');
    this.dashboardService.getProgramRequestCountsByGrantType().pipe(
      map((data: object[]) => {
        let labels = [];
        let counts = [];
        data.forEach(i => {
          counts.push(i['total']);
          if (this.translate.currentLang == 'np') {
            labels.push(i['namelocal']);
          } else {
            labels.push(i['name']);
          }
        });
        var piechart = new Chart(pieChart, {
          type: 'doughnut',
          data: {
            datasets: [
              {
                data: counts,
                backgroundColor: ['green', 'red', 'gray'],
              },
            ],

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: labels,
          },
          options: {
            responsive: true,
            animation: {
              onComplete: function(animation) {
                var firstSet = animation.chart.config.data.datasets[0].data;
                if (firstSet.length == 0) {
                  const ctx: any = pieChart.getContext('2d');
                  ctx.font = '12px sans-serif';
                  ctx.textAlign = 'center';
                  ctx.textBaseline = 'middle';
                  ctx.fillText(that.translate.instant('noRowsForPieChart'), 180, 160);
                  ctx.fillText(that.translate.instant('noRows'), 180, 180);
                }
              }
            }
          },
        });

        this.dashboardDataSetup();

      })
    ).subscribe();


  }

  manageColumns(): void {
    this.latestProgramRequestsColumns = [
      this.manageEachColumn('fiscalYear', 'fiscalyear', true),
      this.manageEachColumn('localBodyLevel', 'localbodynamelocal', true),
      this.manageEachColumn('concernedMinistry', 'concernedministerynamelocal', true, false, 'string', false, false, {
        hidden: true
      }),
      this.manageEachColumn('grantType', 'granttypenamelocal', true),
      this.manageEachColumn('programName', 'programname', true),
      this.manageEachColumn('demandAmount', 'requestamount', true, false, 'float'),
    ];

    this.latestProgramAppropriationsColumns = [
      this.manageEachColumn('fiscalYear', 'fiscalyear', true),
      this.manageEachColumn('localBodyLevel', 'localbodynamelocal', true, false, 'float', false, false, {
        hidden: true
      }),
      this.manageEachColumn('concernedMinistry', 'concernedministerynamelocal', true),
      this.manageEachColumn('grantType', 'granttypenamelocal', true),
      this.manageEachColumn('programName', 'programname', true),
      this.manageEachColumn('demandAmount', 'requestamount', true, false, 'float', false, false, {
        hidden: true
      }),
      this.manageEachColumn('appropriationAmount', 'appropriatedamount', true, false, 'float'),
    ];
  }

  getLatestProgramRequests(): void {
    this.dashboardService.getLatestProgramRequests().pipe(
      map(data => {
        this.fetchedLatestProgramRequests = data;
      })
    ).subscribe();
  }

  getLatestProgramAppropriations(): void {
    this.dashboardService.getLatestProgramAppropriations().pipe(
      map(data => {
        this.fetchedLatestProgramAppropriations = data;
      })
    ).subscribe();
  }


}
