import {Injector} from '@angular/core';

import {FeaturedController} from '../featured.controller';

export class SecurityController extends FeaturedController {

  constructor(public injector: Injector) {
    super(injector);
  }


}
