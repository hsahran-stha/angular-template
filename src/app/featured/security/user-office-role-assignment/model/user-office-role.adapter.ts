import {Adapter} from '../../../../core/adapter';
import {Grid, UserOfficeRoleFormModel, UserOfficeRoleModel} from './user-office-role.model';
import {Injectable} from '@angular/core';

@Injectable()
export class UserOfficeRoleAdapter implements Adapter<UserOfficeRoleModel> {
  adapt(item: any): UserOfficeRoleModel {
    return new UserOfficeRoleModel(
      item.id,
      item.effectdate,
      item.effecttilldate,
      item.userName,
      item.userId,
      item.status,
      item.entrydate,
      item.entryuserid,
      item.entryusername,
      item.statuschangeddate,
      item.statuschangeuserid,
    );
  }
}

export class UserOfficeRoleFormAdapter implements Adapter<UserOfficeRoleFormModel> {
  adapt(item: any): UserOfficeRoleFormModel {
    const subItem: [] = item.right;
    const gridItems = [];
    if (subItem) {
      subItem.forEach((d: any) => {
        let i: Grid = new Grid(
          d.officeId,
          d.roleId
        );
        gridItems.push(i);
      });
    }
    const effectTillDate = item.effectTillDate == '' ? null : item.effectTillDate;
    return new UserOfficeRoleFormModel(
      item.id,
      item.effectDate,
      effectTillDate,
      item.userId,
      item.status,
      gridItems,
    );
  }
}
