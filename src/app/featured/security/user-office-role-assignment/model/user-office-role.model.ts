export class UserOfficeRoleModel {
  constructor(
    public id: number,
    public effectDate: string,
    public effectTillDate: string,
    public user: string,
    public userId: number,
    public status: number,
    public entrydate: string,
    public entryuserid: number,
    public entryusername: string,
    public statuschangeddate: string,
    public statuschangeuserid: number,
  ) {
  }

}

export class UserOfficeRoleFormModel {
  constructor(
    public id: number,
    public effectDate: string,
    public effectTillDate: string,
    public userId: number,
    public status: string,
    public right: Grid[]
  ) {
  }

}

export class Grid {
  constructor(
    public officeId: number,
    public roleId: number,
  ) {


  }

}

