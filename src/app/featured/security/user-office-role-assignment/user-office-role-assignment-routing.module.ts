import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserOfficeRoleListComponent} from './user-office-role-list/user-office-role-list.component';
import {UserOfficeRoleFormComponent} from './user-office-role-form/user-office-role-form.component';
import {environment} from '../../../../environments/environment';

const routes: Routes = [
  {
    path: '', component: UserOfficeRoleListComponent
  },
  {
    path: 'create', component: UserOfficeRoleFormComponent,
    data: {menucode: environment.menuCodes.userRoleOfficeSetup}
  },
  {
    path: 'view/:id', component: UserOfficeRoleFormComponent,
    data: {menucode: environment.menuCodes.userRoleOfficeSetup}
  },
  {
    path: 'edit/:id', component: UserOfficeRoleFormComponent,
    data: {menucode: environment.menuCodes.userRoleOfficeSetup}
  },
  {
    path: 'delete/:id', component: UserOfficeRoleFormComponent,
    data: {menucode: environment.menuCodes.userRoleOfficeSetup}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserOfficeRoleAssignmentRoutingModule {
}
