import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UserOfficeRoleAssignmentRoutingModule} from './user-office-role-assignment-routing.module';
import {UserOfficeRoleListComponent} from './user-office-role-list/user-office-role-list.component';
import {UserOfficeRoleFormComponent} from './user-office-role-form/user-office-role-form.component';
import {SharedModule} from '../../../shared/shared.module';
import {UserOfficeRoleService} from './user-office-role.service';
import {UserOfficeRoleAdapter, UserOfficeRoleFormAdapter} from './model/user-office-role.adapter';


@NgModule({
  declarations: [UserOfficeRoleListComponent, UserOfficeRoleFormComponent],
  imports: [
    CommonModule,
    SharedModule,
    UserOfficeRoleAssignmentRoutingModule
  ],
  providers: [UserOfficeRoleService, UserOfficeRoleAdapter, UserOfficeRoleFormAdapter]

})
export class UserOfficeRoleAssignmentModule {
}
