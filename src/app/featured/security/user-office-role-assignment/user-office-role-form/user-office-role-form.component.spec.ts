import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserOfficeRoleFormComponent } from './user-office-role-form.component';

describe('UserOfficeRoleFormComponent', () => {
  let component: UserOfficeRoleFormComponent;
  let fixture: ComponentFixture<UserOfficeRoleFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserOfficeRoleFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserOfficeRoleFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
