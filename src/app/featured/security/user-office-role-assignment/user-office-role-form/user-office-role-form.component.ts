import {Component, ElementRef, Injector, OnInit, ViewChild} from '@angular/core';
import {UserOfficeRoleController} from '../user-office-role.controller';
import {Validators} from '@angular/forms';
import {catchError, filter, map, take} from 'rxjs/operators';
import {OfficeModel} from '../../../setup/office/model/office.model';
import {RoleModel} from '../../role/model/role.model';
import {Status} from '../../../../shared/enums/status';

@Component({
  selector: 'app-user-office-role-form',
  templateUrl: './user-office-role-form.component.html',
  styleUrls: ['./user-office-role-form.component.scss']
})
export class UserOfficeRoleFormComponent extends UserOfficeRoleController implements OnInit {

  constructor(public injector: Injector) {
    super(injector);
    this.activatedRoute.params.subscribe(
      (params) => {
        if (Object.keys(params).length > 0) {
          this.routeId = parseInt(atob(params['id']));
        }
      }
    );
  }

  ngOnInit(): void {
    this.initializeFormStructure();
    this.manageColumns();

    if (this.routeId) {
      if (this.getPage() == 'view') {
        this.formStructure.disable();
        this.buttonAccess['save'] = false;
        this.buttonAccess['reset'] = false;
      }

      if (this.getPage() == 'edit') {
        this.buttonAccess['save'] = false;
        this.buttonAccess['reset'] = false;
        this.buttonAccess['update'] = true;
      }

      if (this.getPage() == 'delete') {
        this.formStructure.disable();
        this.buttonAccess['save'] = false;
        this.buttonAccess['reset'] = false;
        this.buttonAccess['delete'] = true;
      }

      this.fetchDataById();
    }
  }

  fetchDataById(): void {
    this.userOfficeRoleService.getById(this.routeId).pipe(
      map(data => {
        this.patchDetails(data);
      })
    ).subscribe();
  }

  patchDetails(data?: object): void {
    if (data == null) {
      this.roleOfficeAssignList = [];
      this.formStructure.patchValue({effectDate: '', effectTillDate: ''});
      return;
    }

    this.formStructure.patchValue(data);
    this.formStructure.patchValue({
      effectDate: data['effectDate'] && this.translate.currentLang == 'np' ? this.nepaliDateConverter.ADtoBS(data['effectDate']) : data['effectDate'],
      effectTillDate: data['effectTillDate'] && this.translate.currentLang == 'np' ? this.nepaliDateConverter.ADtoBS(data['effectTillDate']) : data['effectTillDate']
    });
    this.roleOfficeAssignList = [];
    const assignedList = data['grid'];
    for (let i = 0; i < assignedList.length; i++) {
      let obj = {
        officeId: assignedList[i]['officeId'],
        roleId: assignedList[i]['roleId'],
        officeCode: assignedList[i]['projectCode'],
        officeName: this.translate.currentLang == 'np' ? assignedList[i]['officeNameLocal'] : assignedList[i]['officeName'],
        roleName: assignedList[i]['roleName'],
      };
      this.roleOfficeAssignList.push(obj);
    }
  }

  initializeFormStructure(): void {
    this.formStructure = this.formBuilder.group({
      id: [''],
      userId: ['', Validators.required],
      userName: [{value: '', disabled: true}, Validators.required],
      effectDate: ['', Validators.required],
      status: ['', Validators.required],
      effectTillDate: [''],
      right: ['']
    });

    this.entryPadForm = this.formBuilder.group({
      officeCode: [{value: '', disabled: true}, Validators.required],
      officeId: ['', Validators.required],
      officeName: [{value: '', disabled: true}, Validators.required],
      roleId: ['', Validators.required],
      roleName: [{value: '', disabled: true}, Validators.required],
    });
  }

  manageColumns() {
    let that = this;
    this.roleOfficeColumns = [
      this.manageEachColumn('officeCode', 'officeCode', true),
      this.manageEachColumn('officeName', 'officeName', true),
      this.manageEachColumn('role', 'roleName', true),
      this.getPage() == 'create' || this.getPage() == 'edit'
        ? this.manageEachColumn('action', '', false, false, '', true, this, {
          render: function(ui) {
            return '<button type="button" class="delete_btn ui-button-action"><i class="ic-close"></i></button>';
          },
          postRender: function(ui) {
            var grid = this,
              $cell = grid.getCell(ui);
            let selectedRow = ui.rowData;
            $cell
              .find('.delete_btn')
              .button({icons: ''})
              .off('click')
              .on('click', function(evt) {
                that.roleOfficeAssignList = that.roleOfficeAssignList.filter(
                  function(el: any) {
                    return el.pq_ri != selectedRow.pq_ri;
                  }
                );
              });
          },
        })
        : {hidden: true},
    ];

    this.remoteUserUrl = this.userService.get(Status.ACTIVE);
    this.userColumns = [
      this.manageEachColumn('userName', 'username', true),
      this.manageEachColumn('fullName', 'fullnamelocal', true),
    ];

    this.officeColumns = [
      this.manageEachColumn('projectCode', 'projectcode', true),
      this.manageEachColumn('name', 'name', true),
      this.manageEachColumn('nameLocal', 'namelocal', true),
    ];
    this.roleColumns = [
      this.manageEachColumn('name', 'name', true)
    ];
  }

  getSelectedUserData($event): void {
    console.log($event);
    this.formStructure.patchValue({
      userId: $event.id,
      userName: `${$event.username}`
    });
    this.checkExisting();
    this.modalService.dismissAll();
  }

  getSelectedOfficeData($event): void {
    console.log($event);
    this.entryPadForm.patchValue({
      officeId: $event.id,
      officeName: this.translate.currentLang == 'np' ? $event.namelocal : $event.name,
      officeCode: $event.projectcode,
    });
    this.modalService.dismissAll();
  }

  getSelectedRoleData($event): void {
    this.entryPadForm.patchValue({
      roleId: $event.id,
      roleName: $event.name,
    });
    this.modalService.dismissAll();
  }

  @ViewChild('officeListModal') officeListModal: ElementRef;

  fetchOfficeData(): void {
    this.officeService.get(Status.ACTIVE).pipe(
      filter(data => data['status'] !== false),
      map((data: OfficeModel[]) => {
        this.fetchedOfficeData = data;
        this.openLg(this.officeListModal);
      })
    ).subscribe();
  }

  @ViewChild('roleListModal') roleListModal: ElementRef;

  fetchRoleData(): void {
    this.roleService.get(Status.ACTIVE).pipe(
      filter(data => data['status'] !== false),
      map((data: RoleModel[]) => {
        this.fetchedRoleData = data;
        this.openLg(this.roleListModal);
      })
    ).subscribe();
  }


  addMapping(): void {
    if (this.entryPadForm.invalid) {
      this.validateAllFormFields(this.entryPadForm);
      this.displayInvalidFormControls(this.entryPadForm);
      return;
    }

    if (this.validateDuplicate()) {
      this.alertify.error(this.translate.instant('duplicateEntry'));
      return;
    }

    this.roleOfficeAssignList.push(this.entryPadForm.getRawValue());
    this.entryPadForm.reset();
  }

  /**
   * fn to validate duplicacy
   */
  validateDuplicate() {
    let officeExists: boolean =
      this.roleOfficeAssignList.some(
        (el: any) => el.officeId === this.entryPadForm.get('officeId').value
      );

    let roleExists: boolean =
      this.roleOfficeAssignList.some(
        (el: any) => el.roleId === this.entryPadForm.get('roleId').value
      );

    if (officeExists && roleExists) {
      return 'both';
    } else {
      return false;
    }
  }

  onSubmit(): void {
    if (this.formStructure.invalid) {
      this.validateAllFormFields(this.formStructure);
      this.displayInvalidFormControls(this.formStructure);
      return;
    }

    // to show confirmation box
    let obj = {
      body: this.getCRUDSubmitConfMessage(this.formStructure.get('id').value),
      btnClass: 'btn-primary',
      alertClass: 'ic-save',
      buttonName: this.formStructure.get('id').value ? this.translate.instant('update') : this.translate.instant('save')
    };
    this.confirmDialogBoxService.show(obj);
    const processConfirmation$ = this.confirmDialogBoxService.getProcessConfirmation();
    processConfirmation$.pipe(take(1)).subscribe((val) => {
      if (val) {
        this.formStructure.get('right').setValue(this.roleOfficeAssignList);
        this.formStructure.patchValue({
          effectDate: this.translate.currentLang == 'np' ? this.nepaliDateConverter.BStoAD(this.formStructure.get('effectDate').value) : this.formStructure.get('effectDate').value,
          effectTillDate: this.translate.currentLang == 'np' ? this.nepaliDateConverter.BStoAD(this.formStructure.get('effectTillDate').value) : this.formStructure.get('effectTillDate').value,
        });
        if (this.formStructure.get('id').value) {

          this.userOfficeRoleService.put(this.formStructure.value, this.formStructure.get('id').value).subscribe((res) => {
              this.afterSubmitSuccess(res);
            }, (error => {
              this.formStructure.patchValue({
                effectDate: this.translate.currentLang == 'np' ? this.nepaliDateConverter.ADtoBS(this.formStructure.get('effectDate').value) : this.formStructure.get('effectDate').value,
                effectTillDate: this.translate.currentLang == 'np' ? this.nepaliDateConverter.ADtoBS(this.formStructure.get('effectTillDate').value) : this.formStructure.get('effectTillDate').value,
              });
              return error;
            })
          );
        } else {
          this.userOfficeRoleService.post(this.formStructure.value).subscribe((res) => {
              this.afterSubmitSuccess(res);
            }, (error => {
              this.formStructure.patchValue({
                effectDate: this.translate.currentLang == 'np' ? this.nepaliDateConverter.ADtoBS(this.formStructure.get('effectDate').value) : this.formStructure.get('effectDate').value,
                effectTillDate: this.translate.currentLang == 'np' ? this.nepaliDateConverter.ADtoBS(this.formStructure.get('effectTillDate').value) : this.formStructure.get('effectTillDate').value,
              });
              return error;
            })
          );
        }
      }
    });
  }

  afterSubmitSuccess(res?: object): void {
    this.formStructure.reset();
    this.alertify.message(res['message']);
    this.buttonAccess['save'] = true;
    this.buttonAccess['update'] = false;
    this.location.back();

  }

  resetClicked($event): void {
    if ($event) {
      this.buttonAccess['save'] = true;
      this.buttonAccess['update'] = false;
      this.buttonAccess['delete'] = false;
      this.formStructure.reset();
      this.formStructure.enable();
      this.confirmDeletionService.drop();
    }
  }

  deleteClicked($event): void {

    // to show confirmation box
    let obj = {
      body: this.getCRUDSubmitConfMessage(this.formStructure.get('id').value, true),
      btnClass: 'btn-danger',
      alertClass: 'ic-alert',
      buttonName: this.translate.instant('confirm')
    };
    this.confirmDialogBoxService.show(obj);
    const processConfirmation$ = this.confirmDialogBoxService.getProcessConfirmation();
    processConfirmation$.pipe(take(1)).subscribe((val) => {
      if (val) {
        this.userOfficeRoleService.delete(this.routeId).subscribe((res) => {
          this.location.back();
          this.confirmDeletionService.drop();
          this.alertify.message(res['message']);
          this.formStructure.reset();
          this.formStructure.enable();
          this.buttonAccess['delete'] = false;
          this.buttonAccess['save'] = true;
        });

      }
    });
  }

  checkExisting(): void {
    const id = this.formStructure.get('userId').value;
    this.userOfficeRoleService.checkExisting(id).pipe(
      map((data: object) => {
          if (data) {
            data['id'] = null;
          } // As it is for new record so reset id
          this.patchDetails(data);
        }
      )
    )
      .subscribe();
  }

}
