import {Component, Injector, OnInit} from '@angular/core';
import {UserOfficeRoleController} from '../user-office-role.controller';
import {catchError, filter, map} from 'rxjs/operators';
import {UserOfficeRoleModel} from '../model/user-office-role.model';

@Component({
  selector: 'app-user-office-role-list',
  templateUrl: './user-office-role-list.component.html',
  styleUrls: ['./user-office-role-list.component.scss']
})
export class UserOfficeRoleListComponent extends UserOfficeRoleController implements OnInit {

  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.manageColumn();
    this.fetchData();
  }

  manageColumn(): void {
    this.columns = [
      this.manageEachColumn('userName', 'user', true),
      this.manageEachColumn('effectDate', 'effectDate', true),
      this.manageEachColumn('effectiveTill', 'effectTillDate', true),
      this.manageEachColumn('status', 'status', true),
      this.manageEachColumn('entryUserName', 'entryusername', true),
      this.manageEachColumn('entryDate', 'entrydate', true),
      this.manageEachColumn('action', '', false, false, '', true, this),
    ];
  }

  fetchData(): void {
    this.userOfficeRoleService.get().pipe(
      filter(data => data['status'] !== false),
      map((data: UserOfficeRoleModel[]) => {
        this.fetchedData = data;
      }),
      catchError(err => {
        console.log(err);
        return err;
      })
    ).subscribe();
  }

  redirectToCreate(): void {
    this.router.navigate(['create'], {relativeTo: this.activatedRoute});
  }

  actionView(ui): void {
    this.rowData = ui.rowData;
    this.router.navigate(['view', btoa(this.rowData['id'])], {relativeTo: this.activatedRoute});
  }

  actionEdit(ui): void {
    this.rowData = ui.rowData;
    this.router.navigate(['edit', btoa(this.rowData['id'])], {relativeTo: this.activatedRoute});
  }

  actionDelete(ui): void {
    this.rowData = ui.rowData;
    this.router.navigate(['delete', btoa(this.rowData['id'])], {relativeTo: this.activatedRoute});
  }


}
