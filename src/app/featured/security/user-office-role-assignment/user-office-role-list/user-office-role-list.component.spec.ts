import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserOfficeRoleListComponent } from './user-office-role-list.component';

describe('UserOfficeRoleListComponent', () => {
  let component: UserOfficeRoleListComponent;
  let fixture: ComponentFixture<UserOfficeRoleListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserOfficeRoleListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserOfficeRoleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
