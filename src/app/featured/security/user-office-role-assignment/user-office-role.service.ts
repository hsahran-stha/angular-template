import {Injectable, Injector} from '@angular/core';
import {environment} from 'src/environments/environment';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AppService} from '../../../shared/service/app.service';
import {UserOfficeRoleAdapter, UserOfficeRoleFormAdapter} from './model/user-office-role.adapter';
import {UserOfficeRoleModel} from './model/user-office-role.model';


@Injectable()
export class UserOfficeRoleService extends AppService {
  private moduleName = 'user_office_assignment';
  private getUrl = `${environment.API_URL}${this.moduleName}/fetch`;
  private getByIdUrl = `${environment.API_URL}${this.moduleName}/get/`;
  private postUrl = `${environment.API_URL}${this.moduleName}/create`;
  private putUrl = `${environment.API_URL}${this.moduleName}/edit`;
  private deleteUrl = `${environment.API_URL}${this.moduleName}/delete/`;
  private checkExistingUrl = `${environment.API_URL}${this.moduleName}/get/user/`;

  constructor(public injector: Injector, private adapter: UserOfficeRoleAdapter, private formAdapter: UserOfficeRoleFormAdapter) {
    super(injector);
  }

  get(): Observable<UserOfficeRoleModel[]> {
    return this.httpClient.get<UserOfficeRoleModel[]>(this.getUrl).pipe(
      map((data: UserOfficeRoleModel[]) => {
        return data['data'].map((item: UserOfficeRoleModel) => this.adapter.adapt(item));
      }));
  }

  post(data: object): Observable<object> {
    return this.httpClient.post<object>(this.postUrl, this.formAdapter.adapt(data)).pipe();
  }

  put(data: object, id: number): Observable<object> {
    return this.httpClient.post<object>(`${this.putUrl}`, this.formAdapter.adapt(data)).pipe();
  }

  delete(id: number): Observable<object> {
    return this.httpClient.delete<object>(`${this.deleteUrl}${id}`).pipe();
  }

  getById(id: number): Observable<UserOfficeRoleModel> {
    return this.httpClient.get(`${this.getByIdUrl}${id}`).pipe(map(data => data['data']));
  }


  checkExisting(id: string): any {
    const url = `${this.checkExistingUrl}${id}`;
    return this.httpClient.get<object>(url).pipe(map(data => data['data']));
  }

}
