import {Injector} from '@angular/core';
import {SecurityController} from '../security.controller';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UserOfficeRoleService} from './user-office-role.service';
import {UserService} from '../user/user.service';
import {OfficeService} from '../../setup/office/office.service';
import {RoleService} from '../role/role.service';


export class UserOfficeRoleController extends SecurityController {

  public columns;
  public fetchedData;
  public fetchedOfficeData;
  public fetchedRoleData;
  public remoteUrl;
  public remoteUserUrl;
  public formStructure: FormGroup;
  public entryPadForm: FormGroup;
  public userOfficeRoleService: UserOfficeRoleService;
  public officeService: OfficeService;
  public userService: UserService;
  public roleService: RoleService;
  public buttonAccess: object = {save: true, exit: true};
  public rowData: object;
  public roleOfficeAssignList: object[] = [];
  public roleOfficeColumns: object[];
  public userColumns: object[];
  public officeColumns: object[];
  public roleColumns: object[];

  constructor(public injector: Injector) {
    super(injector);
    this.userOfficeRoleService = this.injector.get(UserOfficeRoleService);
    this.userService = this.injector.get(UserService);
    this.officeService = this.injector.get(OfficeService);
    this.roleService = this.injector.get(RoleService);

  }


}
