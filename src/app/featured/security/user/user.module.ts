import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UserRoutingModule} from './user-routing.module';
import {UserListComponent} from './user-list/user-list.component';
import {UserFormComponent} from './user-form/user-form.component';
import {SharedModule} from '../../../shared/shared.module';

@NgModule({
  declarations: [UserListComponent, UserFormComponent],
  imports: [
    CommonModule, SharedModule,
    UserRoutingModule
  ],
  providers: []
})
export class UserModule {
}
