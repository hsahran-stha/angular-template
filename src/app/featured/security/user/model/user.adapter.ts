import {Injectable} from '@angular/core';
import {Adapter} from '../../../../core/adapter';
import {UserFormModel, UserModel} from './user.model';


@Injectable()
export class UserAdapter implements Adapter<UserModel> {
  adapt(item: any): UserModel {
    return new UserModel(
      item.id,
      item.username,
      item.email,
      item.fullname,
      item.fullname,
      item.image,
      item.address,
      item.address,
      item.status,
      item.gender,
      item.phonenumber,
      item.entrydate,
      item.entryuserid,
      item.statuschangedate,
      item.statuschangeuserid);
  }
}

export class UserFormAdapter implements Adapter<UserFormModel> {
  adapt(item: any): UserFormModel {
    let ids = '';
    if (item.roleIds) {
      item.roleIds.forEach((i) => {
        console.log(i);
        ids += i['id'] + ',';
      });
      ids = ids.slice(0, -1);
      console.log(ids);
    }
    return new UserFormModel(
      item.id,
      item.username,
      item.email,
      item.fullname,
      item.image,
      item.address,
      item.mobileno,
      item.status,
      item.gender,
      item.imageAction,
      ids,
      2,
      1,
      1
    );
  }
}
