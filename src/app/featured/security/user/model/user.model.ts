export class UserModel {
  constructor(
    public id: number,
    public username: string,
    public email: string,
    public fullname: string,
    public fullnamelocal: string,
    public image: string,
    public address: string,
    public addresslocal: string,
    public status: number,
    public gender: number,
    public mobileno: string,
    public entrydate: number,
    public entryuserid: number,
    public statuschangedate: number,
    public statuschangeuserid: number,
  ) {
  }

}


export class UserFormModel {
  constructor(
    public id: number,
    public userName: string,
    public email: string,
    public fullName: string,
    public image: string,
    public address: string,
    public phoneNumber: string,
    public status: string,
    public gender: number,
    public imageAction: number,
    public roleIds: string,
    public userType: number,
    public languageOption: number,
    public themeOption: number,
  ) {
  }

}
