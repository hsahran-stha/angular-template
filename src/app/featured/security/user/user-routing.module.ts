import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserListComponent} from './user-list/user-list.component';
import {UserFormComponent} from './user-form/user-form.component';
import {environment} from '../../../../environments/environment';

const routes: Routes = [
  {
    path: '', component: UserListComponent
  },
  {
    path: 'create', component: UserFormComponent,
    data: {menucode: environment.menuCodes.userSetup}
  },
  {
    path: 'view/:id', component: UserFormComponent,
    data: {menucode: environment.menuCodes.userSetup}
  },
  {
    path: 'edit/:id', component: UserFormComponent,
    data: {menucode: environment.menuCodes.userSetup}
  },
  {
    path: 'delete/:id', component: UserFormComponent,
    data: {menucode: environment.menuCodes.userSetup}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {
}
