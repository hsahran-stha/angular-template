import {Component, Injector, OnInit} from '@angular/core';
import {UserController} from '../user.controller';
import {Validators} from '@angular/forms';
import {filter, map, take} from 'rxjs/operators';
import {TransactionAction} from '../../../../shared/enums/transactionAction';
import {environment} from '../../../../../environments/environment';
import {RoleModel} from '../../role/model/role.model';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent extends UserController implements OnInit {

  constructor(public  injector: Injector) {
    super(injector);
    this.activatedRoute.params.subscribe(
      (params) => {
        if (Object.keys(params).length > 0) {
          this.routeId = parseInt(atob(params['id']));
        }
      }
    );
  }

  ngOnInit(): void {
    this.initializeFormStructure();
    this.manageColumn();
    this.fetchRoleData();

    if (this.routeId) {
      if (this.getPage() == 'view') {
        this.formStructure.disable();
        this.buttonAccess['save'] = false;
        this.buttonAccess['reset'] = false;
      }

      if (this.getPage() == 'edit') {
        this.buttonAccess['save'] = false;
        this.buttonAccess['reset'] = false;
        this.buttonAccess['update'] = true;
      }

      if (this.getPage() == 'delete') {
        this.formStructure.disable();
        this.buttonAccess['save'] = false;
        this.buttonAccess['reset'] = false;
        this.buttonAccess['delete'] = true;
      }

      this.fetchDataById();
    }
  }

  fetchDataById(): void {
    this.userService.getById(this.routeId).pipe(
      map(data => {
        console.log(data);
        this.formStructure.patchValue(data);
        this.imageSrc = data['image']
          ? environment.API_URL + data['image']
          : '';
      })
    ).subscribe();
  }


  initializeFormStructure(): void {
    this.formStructure = this.formBuilder.group({
      id: [''],
      fullname: ['', Validators.required],
      gender: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      image: [''],
      address: ['', Validators.required],
      mobileno: ['', Validators.required],
      status: ['', Validators.required],
      imageAction: [TransactionAction.ADD],
      roleIds: [''],
    });

    this.roleEntryPad = this.formBuilder.group({
      role: ['', Validators.required],
    });
  }

  fetchRoleData(): void {
    this.roleService.get().pipe(
      filter(data => data['status'] !== false),
      map((data: RoleModel[]) => {
        const list = data;
        for (let i = 0; i < list.length; i++) {
          let obj = {
            id: list[i]['id'],
            itemName: list[i]['name'],
            name: list[i]['name'],
          };
          this.dropdownList.push(obj);
        }
      })
    ).subscribe();
  }

  manageColumn(): void {
    this.roleColumns = [
      this.manageEachColumn('id', 'id', true, true, 'string', false, false, {width: '20%'}),
      this.manageEachColumn('name', 'name', true)
    ];
  }

  resetClicked($event): void {
    if ($event) {
      this.buttonAccess['save'] = true;
      this.buttonAccess['update'] = false;
      this.buttonAccess['delete'] = false;
      this.formStructure.reset();
      this.formStructure.enable();
      this.confirmDeletionService.drop();

    }
  }

  onSubmit(): void {
    console.log(this.activatedRoute);
    if (this.formStructure.invalid) {
      this.validateAllFormFields(this.formStructure);
      this.displayInvalidFormControls(this.formStructure);
      return;
    }

    // to show confirmation box
    let obj = {
      body: this.getCRUDSubmitConfMessage(this.formStructure.get('id').value),
      btnClass: 'btn-primary',
      alertClass: 'ic-save',
      buttonName: this.formStructure.get('id').value ? this.translate.instant('update') : this.translate.instant('save')
    };
    this.confirmDialogBoxService.show(obj);

    const processConfirmation$ = this.confirmDialogBoxService.getProcessConfirmation();
    processConfirmation$.pipe(take(1)).subscribe((val) => {
      if (val) {
        this.formStructure.get('roleIds').setValue(this.roleList);
        if (this.formStructure.get('id').value) {
          this.userService.put(this.createFormData(this.userService.formatData(this.formStructure.value)), this.formStructure.get('id').value).subscribe((res) => {
            this.afterSubmitSuccess(res);
          });
        } else {
          this.userService.post(this.createFormData(this.userService.formatData(this.formStructure.value))).subscribe((res) => {
            this.afterSubmitSuccess(res);
          });
        }
      }
    });
  }

  afterSubmitSuccess(res?: object): void {
    this.formStructure.reset();
    this.alertify.message(res['message']);
    this.buttonAccess['save'] = true;
    this.buttonAccess['update'] = false;
    this.location.back();

  }

  public handleFileChange(e) {
    let maxfilesizeinkb: number = 200;

    if (
      e.target.files[0].type !== 'image/png' &&
      e.target.files[0].type !== 'image/jpg' &&
      e.target.files[0].type !== 'image/jpeg'
    ) {
      return;
    } else if (parseInt(String(e.target.files[0].size / 1024)) > parseInt(String(maxfilesizeinkb))) {
      this.alertify.error(this.translate.instant(`Size of selected file is ${parseInt(String(e.target.files[0].size / 1024))} KB. Maximum file size limit is ${maxfilesizeinkb} KB`));
      return;
    }
    this.formStructure.get('image').setValue(e.target.files[0]);
    this.formStructure.get('imageAction').setValue(TransactionAction.ADD);
    this.imageSelected = e.target.files[0].name;


    if (e.target.files && e.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.imageSrc = e.target.result;
      };
      reader.readAsDataURL(e.target.files[0]);
    }
  }

  dataURItoBlob(dataURI) {
    console.log(dataURI);

    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0) {
      byteString = atob(dataURI.split(',')[1]);
    } else {
      byteString = unescape(dataURI.split(',')[1]);
    }

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    console.log(new Blob([ia], {type: mimeString}));


    return new Blob([ia], {type: mimeString});
  }

  handleFileRemove() {
    this.formStructure.get('image').setValue('');
    this.formStructure.get('imageAction').setValue(TransactionAction.DELETE);
  }

  deleteClicked($event): void {

    // to show confirmation box
    let obj = {
      body: this.getCRUDSubmitConfMessage(this.formStructure.get('id').value, true),
      btnClass: 'btn-danger',
      alertClass: 'ic-alert',
      buttonName: this.translate.instant('confirm')
    };
    this.confirmDialogBoxService.show(obj);
    const processConfirmation$ = this.confirmDialogBoxService.getProcessConfirmation();
    processConfirmation$.pipe(take(1)).subscribe((val) => {
      if (val) {
        this.userService.delete(this.routeId).subscribe((res) => {
          this.location.back();
          this.alertify.message(res['message']);

          this.confirmDeletionService.drop();
          this.formStructure.reset();
          this.formStructure.enable();
          this.buttonAccess['delete'] = false;
          this.buttonAccess['save'] = true;
        });

      }
    });
  }


  /**
   * handle role selected
   * @param item : selected item
   */
  onRoleSelect(item: any) {
    if (this.selectedRole.some(e => e.id == item.id)) {
      return;
    }
    this.selectedRole.push(item);
    this.roleList = this.selectedRole;
  }

  /**
   * * handle role deselect
   * @param item : deselected item
   */
  OnRoleDeSelect(item: any): void {
    this.selectedRole = this.selectedRole.filter(
      (element) => element.id != item['id']
    );
    this.roleList = this.selectedRole;
  }

  /**
   * handle select all role
   * @param items : all items
   */
  onSelectAll(items: any) {
    this.selectedRole = items;
    this.roleList = this.selectedRole;
  }

  /**
   * handle filter select all role
   * @param items : all items
   */
  onFilterSelectAll(items: any) {
    if (this.roleList.length == 0) {
      this.selectedRole = items;
    } else {
      items.forEach((item) => {
        this.selectedRole.push(item);
      });
    }
    this.roleList = this.selectedRole;
  }

  /**
   * handle deselect all item
   * @param items :all deselected item
   */
  onDeSelectAll(items: any) {
    this.selectedRole = [];
    this.roleList = this.selectedRole;
  }

  /**
   * handle filter deselect all item
   * @param items :all deselected item
   */
  onFilterDeSelectAll(items: any) {
    this.selectedRole = [];
    this.roleList = this.selectedRole;
  }


}
