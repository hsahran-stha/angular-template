import {Component, Injector, OnInit} from '@angular/core';
import {UserController} from '../user.controller';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent extends UserController implements OnInit {

  constructor(public  injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.manageColumn();
    this.fetchData();

  }

  manageColumn(): void {
    this.columns = [
      this.translate.currentLang == 'np' ?
        this.manageEachColumn('fullName', 'fullnamelocal', true) :
        this.manageEachColumn('fullName', 'fullname', true),
      this.manageEachColumn('userName', 'username', true),
      this.manageEachColumn('email', 'email', true),
      this.translate.currentLang == 'np' ?
        this.manageEachColumn('address', 'addresslocal', true) :
        this.manageEachColumn('address', 'address', true),
      this.manageEachColumn('status', 'status', true),
      this.manageEachColumn('entryUserName', 'entryusername', true),
      this.manageEachColumn('entryDate', 'entrydate', true),
      this.manageEachColumn('action', '', false, false, '', true, this),
    ];
  }

  fetchData(): void {
    this.fetchedData = [];
    this.remoteUrl = this.userService.get();
  }

  redirectToCreate(): void {
    this.router.navigate(['create'], {relativeTo: this.activatedRoute});
  }

  actionView(ui): void {
    this.rowData = ui.rowData;
    this.router.navigate(['view', btoa(this.rowData['id'])], {relativeTo: this.activatedRoute});
  }

  actionEdit(ui): void {
    this.rowData = ui.rowData;
    this.router.navigate(['edit', btoa(this.rowData['id'])], {relativeTo: this.activatedRoute});
  }

  actionDelete(ui): void {
    this.rowData = ui.rowData;
    this.router.navigate(['delete', btoa(this.rowData['id'])], {relativeTo: this.activatedRoute});
  }


}
