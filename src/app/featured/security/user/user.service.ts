import {Injectable, Injector} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AppService} from '../../../shared/service/app.service';
import {UserAdapter, UserFormAdapter} from './model/user.adapter';
import {UserFormModel, UserModel} from './model/user.model';

@Injectable()
export class UserService extends AppService {

  private getUrl = `${environment.API_URL}user/getAllPaginated/`;
  private postUrl = `${environment.API_URL}user/save`;
  private putUrl = `${environment.API_URL}user/update`;
  private deleteUrl = `${environment.API_URL}user/delete/`;
  private getByIdUrl = `${environment.API_URL}user/get/`;

  constructor(public injector: Injector, private adapter: UserAdapter, private formAdapter: UserFormAdapter) {
    super(injector);
  }

  get(status: number = -2): string {
    return `${this.getUrl}${status}`;
  }

  formatData(data: object): UserFormModel {
    return this.formAdapter.adapt(data);
  }

  post(data: object): Observable<object> {
    return this.httpClient.post<object>(this.postUrl, data).pipe();
  }

  put(data: object, id: number): Observable<object> {
    return this.httpClient.post<object>(`${this.putUrl}`, data).pipe();
  }

  delete(id: number): Observable<object> {
    return this.httpClient.delete<object>(`${this.deleteUrl}${id}`).pipe();
  }

  getById(id: number): Observable<UserModel> {
    return this.httpClient.get(`${this.getByIdUrl}${id}`).pipe(map(data => this.adapter.adapt(data['data'])));
  }
}
