import {Injector} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {SecurityController} from '../security.controller';
import {UserService} from './user.service';
import {RoleService} from '../role/role.service';

export class UserController extends SecurityController {

  public columns;
  public roleColumns;
  public fetchedData;
  public remoteUrl;
  public formStructure: FormGroup;
  public roleEntryPad: FormGroup;
  public userService: UserService;
  public roleService: RoleService;
  public buttonAccess: object = {save: true, exit: true};
  public rowData: object;
  public imageSelected: any;
  public imageSrc: string = '';

  public roleList: object[];

  /**
   * multi selected array of role
   */
  public dropdownList = [];
  public selectedDropdownList = [];
  public selectedItems = [];
  public dropdownSettings: any = {
    singleSelection: false,
    text: this.translate.instant('roleList'),
    searchPlaceholderText: this.translate.instant('search'),
    selectAllText: this.translate.instant('selectAll'),
    unSelectAllText: this.translate.instant('unSelectAll'),
    enableSearchFilter: true,
    classes: 'custom-class',
  };
  public selectedRole = [];

  constructor(public injector: Injector) {
    super(injector);
    this.userService = this.injector.get(UserService);
    this.roleService = this.injector.get(RoleService);

  }

}
