import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SecurityRoutingModule} from './security-routing.module';
import {SecurityComponent} from './security/security.component';
import {UserService} from './user/user.service';
import {UserAdapter, UserFormAdapter} from './user/model/user.adapter';
import {RoleService} from './role/role.service';
import {RoleAdapter, RoleFormAdapter} from './role/model/role.adapter';


@NgModule({
  declarations: [SecurityComponent],
  imports: [
    CommonModule,
    SecurityRoutingModule
  ],
  providers: [UserService, UserAdapter, UserFormAdapter,
    RoleService, RoleAdapter, RoleFormAdapter]
})
export class SecurityModule {
}
