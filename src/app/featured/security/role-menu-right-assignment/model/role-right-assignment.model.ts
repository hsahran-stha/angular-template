export class RoleRightAssignmentModel {
  constructor(
    public id: number,
    public effectDate: string,
    public effectTillDate: string,
    public role: string,
    public roleId: number,
    public status: number,
    public entrydate: string,
    public entryuserid: number,
    public entryusername: string,
    public statuschangeddate: string,
    public statuschangeuserid: number,
  ) {
  }

}export class RoleRightAssignmentFormModel {
  constructor(
    public id: number,
    public effectDate: string,
    public effectTillDate: string,
    public roleId: number,
    public status: string,
    public right: Grid[]
  ) {
  }

}

export class Grid {
  constructor(
    public menuId: number,
    public menuCode: number,
    public menuName: number,
    public isView: boolean,
    public isUpdate: boolean,
    public isDelete: boolean,
    public isApprove: boolean,
    public isCreate: boolean,
  ) {


  }

}
