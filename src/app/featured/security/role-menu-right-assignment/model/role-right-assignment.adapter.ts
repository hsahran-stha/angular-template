import {Adapter} from '../../../../core/adapter';
import {Injectable} from '@angular/core';
import {Grid, RoleRightAssignmentFormModel, RoleRightAssignmentModel} from './role-right-assignment.model';

@Injectable()
export class RoleRightAssignmentAdapter implements Adapter<RoleRightAssignmentModel> {
  adapt(item: any): RoleRightAssignmentModel {
    return new RoleRightAssignmentModel(
      item.rolemenurightassignmentmasterid,
      item.effectdate,
      item.effecttilldate,
      item.role,
      item.roleid,
      item.status,
      item.entrydate,
      item.entryuserid,
      item.entryusername,
      item.statuschangeddate,
      item.statuschangeuserid,
    );
  }
}

export class RoleRightAssignmentFormAdapter implements Adapter<RoleRightAssignmentFormModel> {
  adapt(item: any): RoleRightAssignmentFormModel {
    const subItem: [] = item.right;
    const gridItems = [];
    if (subItem) {
      subItem.forEach((d: any) => {
        let i: Grid = new Grid(
          d.menuId,
          d.menuCode,
          d.menuName,
          d.isview,
          d.isupdate,
          d.isdelete,
          d.isapprove,
          d.iscreate,
        );
        gridItems.push(i);
      });
    }

    return new RoleRightAssignmentFormModel(
      item.id,
      item.effectDate,
      item.effectTillDate,
      item.roleId,
      item.status,
      gridItems,
    );
  }
}
