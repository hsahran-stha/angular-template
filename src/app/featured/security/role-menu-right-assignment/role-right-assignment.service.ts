import {Injectable, Injector} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AppService} from '../../../shared/service/app.service';
import {RoleRightAssignmentAdapter, RoleRightAssignmentFormAdapter} from './model/role-right-assignment.adapter';
import {RoleRightAssignmentModel} from './model/role-right-assignment.model';

@Injectable({
  providedIn: 'root'
})
export class RoleRightAssignmentService extends AppService {

  private moduleName = 'role_right';
  private menuModuleName = 'menu';
  private getMenuUrl = `${environment.API_URL}${this.menuModuleName}/fetch`;
  private getUrl = `${environment.API_URL}${this.moduleName}/fetch_index`;
  private getByIdUrl = `${environment.API_URL}${this.moduleName}/get/`;
  private getByRoleIdUrl = `${environment.API_URL}${this.moduleName}/get/role/`;
  private postUrl = `${environment.API_URL}${this.moduleName}/save`;
  private putUrl = `${environment.API_URL}${this.moduleName}/edit`;
  private deleteUrl = `${environment.API_URL}${this.moduleName}/delete/`;
  private checkExistingUrl = `${environment.API_URL}${this.moduleName}/get/role/`;

  constructor(public injector: Injector, private adapter: RoleRightAssignmentAdapter, private formAdapter: RoleRightAssignmentFormAdapter) {
    super(injector);
  }

  get(): Observable<RoleRightAssignmentModel[]> {
    return this.httpClient.get<RoleRightAssignmentModel[]>(this.getUrl).pipe(
      map((data: RoleRightAssignmentModel[]) => {
        return data['data'].map((item: RoleRightAssignmentModel) => this.adapter.adapt(item));
      }));
  }

  getMenu(): Observable<any[]> {
    return this.httpClient.get<any[]>(this.getMenuUrl).pipe(
      map((data: any[]) => {
        return data['data'].map((item: any) => item);
      }));
  }

  post(data: object): Observable<object> {
    return this.httpClient.post<object>(this.postUrl, this.formAdapter.adapt(data)).pipe();
  }

  put(data: object, id: number): Observable<object> {
    return this.httpClient.post<object>(`${this.putUrl}`, this.formAdapter.adapt(data)).pipe();
  }

  delete(id: number): Observable<object> {
    return this.httpClient.delete<object>(`${this.deleteUrl}${id}`).pipe();
  }

  getById(id: number): Observable<RoleRightAssignmentModel> {
    return this.httpClient.get(`${this.getByIdUrl}${id}`).pipe(map(data => data['data']));
  }

  getByRoleId(id: number): Observable<RoleRightAssignmentModel> {
    return this.httpClient.get(`${this.getByRoleIdUrl}${id}`).pipe(map(data => data['data']));
  }

  checkExisting(id: string): any {
    const url = `${this.checkExistingUrl}${id}`;
    return this.httpClient.get<object>(url).pipe(map(data => data['data']));
  }
}
