import {Component, ElementRef, Injector, OnInit, ViewChild} from '@angular/core';
import {RoleRightAssignmentController} from '../role-right-assignment.controller';
import {filter, map, take} from 'rxjs/operators';
import {Validators} from '@angular/forms';
import {RoleModel} from '../../role/model/role.model';
import {Status} from '../../../../shared/enums/status';

@Component({
  selector: 'app-role-right-assignment-form',
  templateUrl: './role-right-assignment-form.component.html',
  styleUrls: ['./role-right-assignment-form.component.scss']
})
export class RoleRightAssignmentFormComponent extends RoleRightAssignmentController implements OnInit {

  constructor(public injector: Injector) {
    super(injector);
    this.activatedRoute.params.subscribe(
      (params) => {
        if (Object.keys(params).length > 0) {
          this.routeId = parseInt(atob(params['id']));
        }
      }
    );

  }

  ngOnInit(): void {
    this.initializeFormStructure();
    this.manageColumns();

    if (this.routeId) {
      if (this.getPage() == 'view') {
        this.formStructure.disable();
        this.buttonAccess['save'] = false;
        this.buttonAccess['reset'] = false;
      }

      if (this.getPage() == 'edit') {
        this.buttonAccess['save'] = false;
        this.buttonAccess['reset'] = false;
        this.buttonAccess['update'] = true;
      }

      if (this.getPage() == 'delete') {
        this.formStructure.disable();
        this.buttonAccess['save'] = false;
        this.buttonAccess['reset'] = false;
        this.buttonAccess['delete'] = true;
      }

      this.fetchDataById();
    }
  }

  fetchDataById(): void {
    this.roleRightAssignmentService.getById(this.routeId).pipe(
      map(data => {
        this.patchDetails(data);
      })
    ).subscribe();
  }

  patchDetails(data?: object): void {
    if (data == null) {
      this.roleRightList = [];
      this.formStructure.patchValue({effectDate: '', effectTillDate: ''});
      return;
    }

    this.formStructure.patchValue(data);
    this.formStructure.patchValue({
      effectDate: data['effectDate'] && this.translate.currentLang == 'np' ? this.nepaliDateConverter.ADtoBS(data['effectDate']) : data['effectDate'],
      effectTillDate: data['effectTillDate'] && this.translate.currentLang == 'np' ? this.nepaliDateConverter.ADtoBS(data['effectTillDate']) : data['effectTillDate']
    });
    this.roleRightList = [];
    const assignedList = data['grid'];
    for (let i = 0; i < assignedList.length; i++) {
      console.log(assignedList[i]);

      let obj = {
        iscreate: assignedList[i]['iscreate'],
        isdelete: assignedList[i]['isdelete'],
        isupdate: assignedList[i]['isudpate'],
        isview: assignedList[i]['isview'],
        isapprove: assignedList[i]['isapprove'],
        menuName: this.translate.currentLang == 'np' ? assignedList[i]['processNameLocal'] : assignedList[i]['processName'],
        menuId: assignedList[i]['processId'],
        menuCode: assignedList[i]['processCode'],
      };
      this.roleRightList.push(obj);
      console.log(this.roleRightList);
    }
  }

  initializeFormStructure(): void {
    this.formStructure = this.formBuilder.group({
      id: [''],
      roleId: ['', Validators.required],
      role: [{value: '', disabled: true}, Validators.required],
      effectDate: ['', Validators.required],
      status: ['', Validators.required],
      effectTillDate: [''],
      right: ['']
    });
    this.entryPadForm = this.formBuilder.group({
      menuId: ['', Validators.required],
      menuCode: [{value: '', disabled: true}, Validators.required],
      menuName: [{value: '', disabled: true}, Validators.required],
      iscreate: [false],
      isview: [false],
      isupdate: [false],
      isdelete: [false],
      isapprove: [false]
    });

  }

  manageColumns() {
    let that = this;
    this.menuRightColumns = [
      this.manageEachColumn('processCode', 'menuCode'),
      this.manageEachColumn('processName', 'menuName', true, true, 'string', false, false, {width: '35%'}),
      this.manageEachColumn('add', 'iscreate', false, false, 'boolean', false, this, {
        render: function(ui) {
          if (ui.rowData.iscreate) {
            if (that.getPage() == 'view' || that.getPage() == 'delete') {
              return '<input type="checkbox"  checked class="chkbox" disabled />';
            } else {
              return '<input type="checkbox" class="chkbox" checked/>';
            }
          } else {
            if (that.getPage() == 'view' || that.getPage() == 'delete') {
              return '<input type="checkbox"  class="chkbox" disabled/>';
            }
            return '<input type="checkbox"  class="chkbox"/>';
          }
        },
        postRender: function(ui) {
          var grid = this,
            $cell = grid.getCell(ui);
          let selectedRow = ui.rowData;
          //delete button
          $cell
            .find('.chkbox')
            .off('click')
            .on('click', function(evt) {
              selectedRow.iscreate = evt.target.checked;
            });
        },
      }),
      this.manageEachColumn('edit', 'isupdate', false, false, 'boolean', false, this, {
        render: function(ui) {
          if (ui.rowData.isupdate) {
            if (that.getPage() == 'view' || that.getPage() == 'delete') {
              return '<input type="checkbox"  checked class="chkbox" disabled />';
            } else {
              return '<input type="checkbox" class="chkbox" checked/>';
            }
          } else {
            if (that.getPage() == 'view' || that.getPage() == 'delete') {
              return '<input type="checkbox"  class="chkbox" disabled/>';
            }
            return '<input type="checkbox"  class="chkbox"/>';
          }
        },
        postRender: function(ui) {
          var grid = this,
            $cell = grid.getCell(ui);
          let selectedRow = ui.rowData;
          //delete button
          $cell
            .find('.chkbox')
            .off('click')
            .on('click', function(evt) {
              selectedRow.isupdate = evt.target.checked;
            });
        },
      }),
      this.manageEachColumn('delete', 'isdelete', false, false, 'boolean', false, this, {
        render: function(ui) {
          if (ui.rowData.isdelete) {
            if (that.getPage() == 'view' || that.getPage() == 'delete') {
              return '<input type="checkbox"  checked class="chkbox" disabled />';
            } else {
              return '<input type="checkbox" class="chkbox" checked/>';
            }
          } else {
            if (that.getPage() == 'view' || that.getPage() == 'delete') {
              return '<input type="checkbox"  class="chkbox" disabled/>';
            }
            return '<input type="checkbox"  class="chkbox"/>';
          }
        },
        postRender: function(ui) {
          var grid = this,
            $cell = grid.getCell(ui);
          let selectedRow = ui.rowData;
          //delete button
          $cell
            .find('.chkbox')
            .off('click')
            .on('click', function(evt) {
              selectedRow.isdelete = evt.target.checked;
            });
        },
      }),
      this.manageEachColumn('view', 'isview', false, false, 'boolean', false, this, {
        render: function(ui) {
          if (ui.rowData.isview) {
            if (that.getPage() == 'view' || that.getPage() == 'delete') {
              return '<input type="checkbox"  checked class="chkbox" disabled />';
            } else {
              return '<input type="checkbox" class="chkbox" checked/>';
            }
          } else {
            if (that.getPage() == 'view' || that.getPage() == 'delete') {
              return '<input type="checkbox"  class="chkbox" disabled/>';
            }
            return '<input type="checkbox"  class="chkbox"/>';
          }
        },
        postRender: function(ui) {
          var grid = this,
            $cell = grid.getCell(ui);
          let selectedRow = ui.rowData;
          //delete button
          $cell
            .find('.chkbox')
            .off('click')
            .on('click', function(evt) {
              selectedRow.isview = evt.target.checked;
            });
        },
      }),
      this.manageEachColumn('editAfterApprove', 'isapprove', false, false, 'boolean', false, this, {
        render: function(ui) {
          if (ui.rowData.isapprove) {
            if (that.getPage() == 'view' || that.getPage() == 'delete') {
              return '<input type="checkbox"  checked class="chkbox" disabled />';
            } else {
              return '<input type="checkbox" class="chkbox" checked/>';
            }
          } else {
            if (that.getPage() == 'view' || that.getPage() == 'delete') {
              return '<input type="checkbox"  class="chkbox" disabled/>';
            }
            return '<input type="checkbox"  class="chkbox"/>';
          }
        },
        postRender: function(ui) {
          var grid = this,
            $cell = grid.getCell(ui);
          let selectedRow = ui.rowData;
          //delete button
          $cell
            .find('.chkbox')
            .off('click')
            .on('click', function(evt) {
              selectedRow.isapprove = evt.target.checked;
            });
        },
      }),
      this.getPage() == 'edit' || this.getPage() == 'create'
        ?
        this.manageEachColumn('action', '', false, false, '', false, this, {
          render: function(ui) {
            return '<button type="button" class="delete_btn ui-button-action"><i class="ic-close"></i></button>';
          },
          postRender: function(ui) {
            var grid = this,
              $cell = grid.getCell(ui);
            let selectedRow = ui.rowData;
            //delete button
            $cell
              .find('.delete_btn')
              .button({icons: ''})
              .off('click')
              .on('click', function(evt) {
                that.roleRightList = that.roleRightList.filter(function(el) {
                  return el.pq_ri != selectedRow.pq_ri;
                });
              });
          },
        })
        : {hidden: true},
    ];

    this.menuColumns = [
      this.manageEachColumn('menuCode', 'code', true),
      this.translate.currentLang == 'np' ?
        this.manageEachColumn('name', 'namelocal', true) :
        this.manageEachColumn('name', 'name', true),
    ];

    this.roleColumns = [
      this.manageEachColumn('role', 'name', true)
    ];
  }

  getSelectedRoleData($event): void {
    this.formStructure.patchValue({
      roleId: $event.id,
      role: $event.name,
    });
    this.checkExisting();
    this.modalService.dismissAll();
  }

  @ViewChild('roleListModal') roleListModal: ElementRef;

  fetchRoleData(): void {
    this.roleService.get(Status.ACTIVE).pipe(
      filter(data => data['status'] !== false),
      map((data: RoleModel[]) => {
        this.fetchedRoleData = data;
        this.openLg(this.roleListModal);

      })
    ).subscribe();
  }

  @ViewChild('menuListModal') menuListModal: ElementRef;

  fetchMenuData(): void {
    this.roleRightAssignmentService.getMenu().pipe(
      filter(data => data['status'] !== false),
      map((data: any[]) => {
        this.fetchedMenuData = data;
        this.openLg(this.menuListModal);
      })
    ).subscribe();
  }


  getSelectedMenuData($event): void {
    this.entryPadForm.patchValue({
      menuId: $event.id,
      menuCode: $event.code,
      menuName: this.translate.currentLang == 'np' ? $event.namelocal : $event.name,
    });
    this.modalService.dismissAll();
  }

  addMapping(): void {

    if (this.entryPadForm.invalid) {
      this.validateAllFormFields(this.entryPadForm);
      this.displayInvalidFormControls(this.entryPadForm);
      return;
    }

    if (this.roleRightList.some((el) => el.menuId === this.entryPadForm.get('menuId').value)) {
      this.alertify.error(this.translate.instant('duplicateEntry'));
      return;
    }

    this.roleRightList.push(this.entryPadForm.getRawValue());
    this.entryPadForm.patchValue({iscreate: false, isview: false, isupdate: false, isdelete: false, isapprove: false});
  }


  onSubmit(): void {
    if (this.formStructure.invalid) {
      this.validateAllFormFields(this.formStructure);
      this.displayInvalidFormControls(this.formStructure);
      return;
    }

    // to show confirmation box
    let obj = {
      body: this.getCRUDSubmitConfMessage(this.formStructure.get('id').value),
      btnClass: 'btn-primary',
      alertClass: 'ic-save',
      buttonName: this.formStructure.get('id').value ? this.translate.instant('update') : this.translate.instant('save')
    };
    this.confirmDialogBoxService.show(obj);
    const processConfirmation$ = this.confirmDialogBoxService.getProcessConfirmation();
    processConfirmation$.pipe(take(1)).subscribe((val) => {
      if (val) {
        this.formStructure.patchValue({
          effectDate: this.translate.currentLang == 'np' ? this.nepaliDateConverter.BStoAD(this.formStructure.get('effectDate').value) : this.formStructure.get('effectDate').value,
          effectTillDate: this.translate.currentLang == 'np' ? this.nepaliDateConverter.BStoAD(this.formStructure.get('effectTillDate').value) : this.formStructure.get('effectTillDate').value,
        });
        this.formStructure.get('right').setValue(this.roleRightList);
        if (this.formStructure.get('id').value) {
          this.roleRightAssignmentService.put(this.formStructure.value, this.formStructure.get('id').value).subscribe((res) => {
              this.afterSubmitSuccess(res);
            }, (error => {
              this.formStructure.patchValue({
                effectDate: this.translate.currentLang == 'np' ? this.nepaliDateConverter.ADtoBS(this.formStructure.get('effectDate').value) : this.formStructure.get('effectDate').value,
                effectTillDate: this.translate.currentLang == 'np' ? this.nepaliDateConverter.ADtoBS(this.formStructure.get('effectTillDate').value) : this.formStructure.get('effectTillDate').value,
              });
              return error;
            })
          );
        } else {
          this.roleRightAssignmentService.post(this.formStructure.value).subscribe((res) => {
            this.afterSubmitSuccess(res);
          }, (error => {
            this.formStructure.patchValue({
              effectDate: this.translate.currentLang == 'np' ? this.nepaliDateConverter.ADtoBS(this.formStructure.get('effectDate').value) : this.formStructure.get('effectDate').value,
              effectTillDate: this.translate.currentLang == 'np' ? this.nepaliDateConverter.ADtoBS(this.formStructure.get('effectTillDate').value) : this.formStructure.get('effectTillDate').value,
            });
            return error;
          }));
        }
      }
    });
  }

  afterSubmitSuccess(res?: object): void {
    this.formStructure.reset();
    this.alertify.message(res['message']);
    this.buttonAccess['save'] = true;
    this.buttonAccess['update'] = false;
    this.location.back();

  }

  resetClicked($event): void {
    if ($event) {
      this.buttonAccess['save'] = true;
      this.buttonAccess['update'] = false;
      this.buttonAccess['delete'] = false;
      this.formStructure.reset();
      this.formStructure.enable();
      this.confirmDeletionService.drop();
    }
  }

  deleteClicked($event): void {

    // to show confirmation box
    let obj = {
      body: this.getCRUDSubmitConfMessage(this.formStructure.get('id').value, true),
      btnClass: 'btn-danger',
      alertClass: 'ic-alert',
      buttonName: this.translate.instant('confirm')
    };
    this.confirmDialogBoxService.show(obj);
    const processConfirmation$ = this.confirmDialogBoxService.getProcessConfirmation();
    processConfirmation$.pipe(take(1)).subscribe((val) => {
      if (val) {
        this.roleRightAssignmentService.delete(this.routeId).subscribe((res) => {
          this.location.back();
          this.confirmDeletionService.drop();
          this.alertify.message(res['message']);
          this.formStructure.reset();
          this.formStructure.enable();
          this.buttonAccess['delete'] = false;
          this.buttonAccess['save'] = true;
        });

      }
    });
  }

  checkExisting(): void {
    const id = this.formStructure.get('roleId').value;
    this.roleRightAssignmentService.checkExisting(id).pipe(
      map((data: object) => {
          if (data) {
            data['id'] = null;
          } // As it is for new record so reset id
          this.patchDetails(data);
        }
      )
    ).subscribe();
  }
}

