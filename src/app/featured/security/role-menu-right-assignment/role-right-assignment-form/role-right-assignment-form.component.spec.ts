import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleRightAssignmentFormComponent } from './role-right-assignment-form.component';

describe('RoleRightAssignmentFormComponent', () => {
  let component: RoleRightAssignmentFormComponent;
  let fixture: ComponentFixture<RoleRightAssignmentFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoleRightAssignmentFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleRightAssignmentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
