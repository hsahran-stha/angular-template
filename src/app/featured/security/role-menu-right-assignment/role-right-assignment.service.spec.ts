import { TestBed } from '@angular/core/testing';

import { RoleRightAssignmentService } from './role-right-assignment.service';

describe('RoleRightAssignmentService', () => {
  let service: RoleRightAssignmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RoleRightAssignmentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
