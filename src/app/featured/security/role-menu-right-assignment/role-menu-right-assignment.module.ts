import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoleMenuRightAssignmentRoutingModule } from './role-menu-right-assignment-routing.module';
import { RoleRightAssignmentFormComponent } from './role-right-assignment-form/role-right-assignment-form.component';
import { RoleRightAssignmentListComponent } from './role-right-assignment-list/role-right-assignment-list.component';
import {SharedModule} from '../../../shared/shared.module';
import {RoleRightAssignmentAdapter, RoleRightAssignmentFormAdapter} from './model/role-right-assignment.adapter';
import {RoleRightAssignmentService} from './role-right-assignment.service';


@NgModule({
  declarations: [RoleRightAssignmentFormComponent, RoleRightAssignmentListComponent],
  imports: [
    CommonModule,SharedModule,
    RoleMenuRightAssignmentRoutingModule
  ],
  providers:[RoleRightAssignmentService,RoleRightAssignmentAdapter,RoleRightAssignmentFormAdapter]
})
export class RoleMenuRightAssignmentModule { }
