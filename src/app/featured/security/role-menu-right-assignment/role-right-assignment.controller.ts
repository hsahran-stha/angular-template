import {Injector} from '@angular/core';
import {SecurityController} from '../security.controller';
import {FormGroup} from '@angular/forms';
import {RoleService} from '../role/role.service';
import {RoleRightAssignmentService} from './role-right-assignment.service';


export class RoleRightAssignmentController extends SecurityController {

    public columns;
    public fetchedData;
    public fetchedRoleData;
    public fetchedMenuData;
    public fetchedRoleAssignData;
    public remoteUrl;

    public roleRightList: any = [];
    public formStructure: FormGroup;
    public entryPadForm: FormGroup;
    public roleRightAssignmentService: RoleRightAssignmentService;
    public roleService: RoleService;
    public buttonAccess: object = {save: true, exit: true};
    public rowData: object;
    public roleRightAssignList: object[] = [];
    public roleOfficeColumns: object[];
    public menuColumns: object[];
    public menuRightColumns: object[];
    public roleColumns: object[];

    constructor(public injector: Injector) {
        super(injector);
        this.roleRightAssignmentService = this.injector.get(RoleRightAssignmentService);
        this.roleService = this.injector.get(RoleService);


    }


}
