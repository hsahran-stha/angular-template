import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RoleRightAssignmentListComponent} from './role-right-assignment-list/role-right-assignment-list.component';
import {RoleRightAssignmentFormComponent} from './role-right-assignment-form/role-right-assignment-form.component';
import {environment} from '../../../../environments/environment';

const routes: Routes = [
  {
    path: '', component: RoleRightAssignmentListComponent
  },
  {
    path: 'create', component: RoleRightAssignmentFormComponent,
    data: {menucode: environment.menuCodes.roleMenuRightSetup}
  },
  {
    path: 'view/:id', component: RoleRightAssignmentFormComponent,
    data: {menucode: environment.menuCodes.roleMenuRightSetup}
  },
  {
    path: 'edit/:id', component: RoleRightAssignmentFormComponent,
    data: {menucode: environment.menuCodes.roleMenuRightSetup}
  },
  {
    path: 'delete/:id', component: RoleRightAssignmentFormComponent,
    data: {menucode: environment.menuCodes.roleMenuRightSetup}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoleMenuRightAssignmentRoutingModule {
}
