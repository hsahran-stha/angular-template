import {Component, Injector, OnInit} from '@angular/core';
import {catchError, filter, map} from 'rxjs/operators';
import {RoleRightAssignmentController} from '../role-right-assignment.controller';
import {RoleRightAssignmentModel} from '../model/role-right-assignment.model';

@Component({
  selector: 'app-role-right-assignment-list',
  templateUrl: './role-right-assignment-list.component.html',
  styleUrls: ['./role-right-assignment-list.component.scss']
})
export class RoleRightAssignmentListComponent extends RoleRightAssignmentController implements OnInit {


  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.manageColumn();
    this.fetchData();
  }

  manageColumn(): void {
    this.columns = [
      this.manageEachColumn('role', 'role', true),
      this.manageEachColumn('effectDate', 'effectDate', true),
      this.manageEachColumn('effectiveTill', 'effectTillDate', true),
      this.manageEachColumn('status', 'status', true),
      this.manageEachColumn('entryUserName', 'entryusername', true),
      this.manageEachColumn('entryDate', 'entrydate', true),
      this.manageEachColumn('action', '', false, false, '', true, this),
    ];
  }

  fetchData(): void {
    this.roleRightAssignmentService.get().pipe(
      filter(data => data['status'] !== false),
      map((data: RoleRightAssignmentModel[]) => {
        this.fetchedData = data;
      }),
      catchError(err => {
        console.log(err);
        return err;
      })
    ).subscribe();
  }

  redirectToCreate(): void {
    this.router.navigate(['create'], {relativeTo: this.activatedRoute});
  }

  actionView(ui): void {
    this.rowData = ui.rowData;
    console.log(this.rowData);
    this.router.navigate(['view', btoa(this.rowData['id'])], {relativeTo: this.activatedRoute});
  }

  actionEdit(ui): void {
    this.rowData = ui.rowData;
    this.router.navigate(['edit', btoa(this.rowData['id'])], {relativeTo: this.activatedRoute});
  }

  actionDelete(ui): void {
    this.rowData = ui.rowData;
    this.router.navigate(['delete', btoa(this.rowData['id'])], {relativeTo: this.activatedRoute});
  }

}
