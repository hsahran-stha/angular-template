import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleRightAssignmentListComponent } from './role-right-assignment-list.component';

describe('RoleRightAssignmentListComponent', () => {
  let component: RoleRightAssignmentListComponent;
  let fixture: ComponentFixture<RoleRightAssignmentListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoleRightAssignmentListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleRightAssignmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
