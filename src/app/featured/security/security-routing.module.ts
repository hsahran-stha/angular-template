import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SecurityComponent} from './security/security.component';
import {PermissionsGuard} from '../../guards/permission.guard';

const routes: Routes = [
  {
    path: '',
    component: SecurityComponent,
    children: [
      {
        path: 'role',
        canActivateChild: [PermissionsGuard],
        loadChildren: () =>
          import('./role/role.module')
            .then((m) => m.RoleModule),
      },
      {
        path: 'user',
        canActivateChild: [PermissionsGuard],
        loadChildren: () =>
          import('./user/user.module')
            .then((m) => m.UserModule),
      },
      {
        path: 'user-office-role',
        canActivateChild: [PermissionsGuard],
        loadChildren: () =>
          import('./user-office-role-assignment/user-office-role-assignment.module')
            .then((m) => m.UserOfficeRoleAssignmentModule),
      },
      {
        path: 'role-right-assignment',
        loadChildren: () =>
          import('./role-menu-right-assignment/role-menu-right-assignment.module')
            .then((m) => m.RoleMenuRightAssignmentModule),
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SecurityRoutingModule {
}
