import {Injectable} from '@angular/core';
import {Adapter} from '../../../../core/adapter';
import {RoleFormModel, RoleModel} from './role.model';


@Injectable()
export class RoleAdapter implements Adapter<RoleModel> {
  adapt(item: any): RoleModel {
    return new RoleModel(
      item.id,
      item.role,
      item.officeTypeId,
      item.officeTypeName,
      item.officeTypeNameNp,
      item.status,
      item.entryDate,
      item.entryuserid,
      item.entryusername,
      item.statuschangedate,
      item.statuschangeuserid);
  }
}

export class RoleFormAdapter implements Adapter<RoleFormModel> {
  adapt(item: any): RoleFormModel {
    return new RoleFormModel(
      item.id,
      item.name,
      item.status,
      item.officetypeid,
      item.officetypename,
    );
  }
}
