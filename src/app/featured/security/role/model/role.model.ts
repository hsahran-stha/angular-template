export class RoleModel {
  constructor(
    public id: number,
    public name: string,
    public officeTypeId: number,
    public officeTypeName: string,
    public officeTypeNameLocal: string,
    public status: number,
    public entrydate: number,
    public entryuserid: number,
    public entryusername: string,
    public statuschangedate: string,
    public statuschangeuserid: number,
  ) {
  }
}


export class RoleFormModel {
  constructor(
    public roleId: number,
    public role: string,
    public status: string,
    public officeTypeId: number,
    public officeTypeName: string,
  ) {
  }
}

