import {Component, Injector, OnInit} from '@angular/core';
import {Validators} from '@angular/forms';
import {catchError, filter, map, take} from 'rxjs/operators';
import {RoleController} from '../role.controller';
import {RoleModel} from '../model/role.model';
import {Status} from '../../../../shared/enums/status';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent extends RoleController implements OnInit {

  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.manageColumn();
    this.initializeFormStructure();
    this.fetchData();
    this.fetchOffice();
  }

  manageColumn(): void {
    this.columns = [
      this.manageEachColumn('name', 'name', true),
      this.translate.currentLang == 'np' ?
        this.manageEachColumn('officeType', 'officeTypeNameLocal', true) :
        this.manageEachColumn('officeType', 'officeTypeName', true),
      this.manageEachColumn('status', 'status', true),
      this.manageEachColumn('entryUserName', 'entryusername', true),
      this.manageEachColumn('entryDate', 'entrydate', true),
      this.manageEachColumn('action', '', false, false, '', true, this),
    ];
  }

  actionView(ui): void {
    this.rowData = ui.rowData;
    this.patchValue();

    this.buttonAccess['save'] = false;
    this.buttonAccess['update'] = false;
    this.buttonAccess['delete'] = false;

    this.formStructure.disable();
    this.confirmDeletionService.drop();

  }

  actionEdit(ui): void {
    this.rowData = ui.rowData;
    this.patchValue();

    this.buttonAccess['update'] = true;
    this.buttonAccess['save'] = false;
    this.buttonAccess['delete'] = false;

    this.formStructure.enable();
    this.confirmDeletionService.drop();

  }

  actionDelete(ui): void {
    this.rowData = ui.rowData;
    this.patchValue();

    this.buttonAccess['save'] = false;
    this.buttonAccess['update'] = false;
    this.buttonAccess['delete'] = true;

    this.formStructure.disable();
    this.confirmDeletionService.show();
  }

  patchValue(): void {
    this.formStructure.patchValue({
      id: this.rowData['id'],
      name: this.rowData['name'],
      officetypeid: this.rowData['officeTypeId'],
      status: this.rowData['status'],
    });
  }

  initializeFormStructure(): void {
    this.formStructure = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      officetypeid: ['', Validators.required],
      status: ['', Validators.required],
    });
  }

  fetchOffice(): void {
    this.officeService.getOfficeType().pipe(
      map((data: object[]) => {
        this.officeTypes = data;
        console.log('Office Types', this.officeTypes);
      }),
      catchError(err => {
        return err;
      })
    ).subscribe();
  }

  onSubmit(): void {
    if (this.formStructure.invalid) {
      this.validateAllFormFields(this.formStructure);
      this.displayInvalidFormControls(this.formStructure);
      return;
    }

    // to show confirmation box
    let obj = {
      body: this.getCRUDSubmitConfMessage(this.formStructure.get('id').value),
      btnClass: 'btn-primary',
      alertClass: 'ic-save',
      buttonName: this.formStructure.get('id').value ? this.translate.instant('update') : this.translate.instant('save')
    };
    this.confirmDialogBoxService.show(obj);
    const processConfirmation$ = this.confirmDialogBoxService.getProcessConfirmation();
    processConfirmation$.pipe(take(1)).subscribe((val) => {
      if (val) {
        if (this.formStructure.get('id').value) {
          this.roleService.put(this.formStructure.value, this.formStructure.get('id').value).subscribe((res) => {
            this.afterSubmitSuccess(res);
          });
        } else {
          this.roleService.post(this.formStructure.value).subscribe((res) => {
            this.afterSubmitSuccess(res);
          });
        }
      }
    });
  }

  afterSubmitSuccess(res?: object): void {
    this.fetchData();
    this.alertify.message(res['message']);
    this.formStructure.reset();
    this.formStructure.patchValue({status: ''});

    this.buttonAccess['save'] = true;
    this.buttonAccess['update'] = false;
  }


  fetchData(): void {
    this.roleService.get().pipe(
      filter(data => data['status'] !== false),
      map((data: RoleModel[]) => {
        this.fetchedData = data;
      }),
      catchError(err => {
        console.log(err);
        return err;
      })
    ).subscribe();

  }


  resetClicked($event): void {
    if ($event) {
      this.buttonAccess['save'] = true;
      this.buttonAccess['update'] = false;
      this.buttonAccess['delete'] = false;
      this.formStructure.reset();
      this.formStructure.patchValue({status: ''});

      this.formStructure.enable();
      this.confirmDeletionService.drop();

    }
  }

  deleteClicked($event): void {

    // to show confirmation box
    let obj = {
      body: this.getCRUDSubmitConfMessage(this.formStructure.get('id').value, true),
      btnClass: 'btn-danger',
      alertClass: 'ic-alert',
      buttonName: this.translate.instant('confirm')
    };
    this.confirmDialogBoxService.show(obj);
    const processConfirmation$ = this.confirmDialogBoxService.getProcessConfirmation();
    processConfirmation$.pipe(take(1)).subscribe((val) => {
      if (val) {
        this.roleService.delete(this.rowData['id']).subscribe((res) => {
          this.fetchData();
          this.alertify.message(res['message']);

          this.confirmDeletionService.drop();
          this.formStructure.reset();
          this.formStructure.patchValue({status: ''});

          this.formStructure.enable();
          this.buttonAccess['delete'] = false;
          this.buttonAccess['save'] = true;
        });

      }
    });
  }

  ngOnDestroy() {
  }

}
