import {Injectable, Injector} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AppService} from '../../../shared/service/app.service';
import {RoleAdapter, RoleFormAdapter} from './model/role.adapter';
import {RoleModel} from './model/role.model';

@Injectable()
export class RoleService extends AppService {

  private getUrl = `${environment.API_URL}role/fetch/`;
  private postUrl = `${environment.API_URL}role/save`;
  private putUrl = `${environment.API_URL}role/update`;
  private deleteUrl = `${environment.API_URL}role/delete/`;
  private getByIdUrl = `${environment.API_URL}role/get/`;

  constructor(public injector: Injector, private adapter: RoleAdapter, private formAdapter: RoleFormAdapter) {
    super(injector);
  }

  get(status: number = -2): Observable<RoleModel[]> {
    return this.httpClient.get<RoleModel[]>(`${this.getUrl}${status}`).pipe(
      map((data: RoleModel[]) => {
        return data['data'].map((item: RoleModel) => this.adapter.adapt(item));
      }));
  }

  post(data: object): Observable<object> {
    return this.httpClient.post<object>(this.postUrl, this.formAdapter.adapt(data)).pipe();
  }

  put(data: object, id: number): Observable<object> {
    return this.httpClient.put<object>(`${this.putUrl}`, this.formAdapter.adapt(data)).pipe();
  }

  delete(id: number): Observable<object> {
    return this.httpClient.delete<object>(`${this.deleteUrl}${id}`).pipe();
  }

  getById(id: number): Observable<RoleModel> {
    return this.httpClient.get(`${this.getByIdUrl}${id}`).pipe(map(data => this.adapter.adapt(data)));
  }
}
