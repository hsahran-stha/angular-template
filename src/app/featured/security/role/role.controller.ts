import {Injector} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {SecurityController} from '../security.controller';
import {RoleService} from './role.service';
import {OfficeService} from '../../setup/office/office.service';

export class RoleController extends SecurityController {

  public columns;
  public fetchedData;
  public formStructure: FormGroup;
  public roleService: RoleService;
  public officeService: OfficeService;
  public buttonAccess: object = {save: true, reset: true};
  public rowData: object;
  public officeTypes: object[];


  constructor(public injector: Injector) {
    super(injector);
    this.roleService = this.injector.get(RoleService);
    this.officeService = this.injector.get(OfficeService);

  }

}
