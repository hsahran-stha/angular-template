import {Injector} from '@angular/core';

import {AppController} from '../app.controller';
import {Menu} from '../shared/models/auth/menu';

export class FeaturedController extends AppController {
  public notificationCount: number = 0;

  constructor(public injector: Injector) {
    super(injector);

    this.checkNavigationStateData();
  }


  /**
   * Get CRUD Submit Title
   * @param id
   * @param isDelete
   */
  public getCRUDSubmitTitle(id: number = null, isDelete: boolean = false): string {
    if (isDelete) {
      return 'Delete Title';
    }
    if (id) {
      return 'Edit Title';
    }
    return 'Save Title';
  }

  /**
   * Get CRUD Submit Message
   * @param id
   * @param isDelete
   * @param isDraft
   */
  public getCRUDSubmitConfMessage(id: number = null, isDelete: boolean = false, isDraft: boolean = false): string {
    if (isDelete) {
      return 'Delete Confirmation';
    }
    if (id) {
      return 'Edit Confirmation';
    }

    if (isDraft) {
      return 'Draft Confirmation';
    }

    return 'Save Confirmation';
  }

  /**
   * Check Navigation State Data
   */
  public checkNavigationStateData() {
    this.activatedRoute.data.subscribe(data => {
      if (data['menucode']) {
        if (this.currentUser.getMenus()) {
          //Set Current Menu
          this.currentUser.setMenuByCode(data.menucode);
        }
      } else {
        this.currentUser.setMenuByURL(this.router.url);
      }
    });
  }


}
