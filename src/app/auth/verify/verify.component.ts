import {Component, Injector, OnInit} from '@angular/core';
import {AbstractControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {AuthController} from '../auth-controller';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss']
})
export class VerifyComponent extends AuthController implements OnInit {

  public setPasswordForm: FormGroup;

  // Verification token
  private token: string;

  // Enable Form For Password Change
  public enableForm: boolean = false;

  public pwdMismatch: boolean = false;


  constructor(public injector: Injector) {
    super(injector);
    // Get Token and Verify Token with Server
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['token']) {
        this.token = params['token'];
        this.verifyToken(this.token);
      } else {
        this.redirectToLogin();
      }
    });
  }

  ngOnInit(): void {
    this.setPasswordForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.pattern('^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$')]],
      confirmPassword: ['', Validators.required],
    });
  }

  /**
   * Fn. to submit
   */
  public onSubmit() {
    if (this.setPasswordForm.invalid) {
      this.validateAllFormFields(this.setPasswordForm);
      this.displayInvalidFormControls(this.setPasswordForm);
      return;
    }

    let pwd = this.setPasswordForm.get('password').value;
    let retype = this.setPasswordForm.get('confirmPassword').value;
    if (pwd !== retype) {
      this.pwdMismatch = true;
      setTimeout(() => {
        this.pwdMismatch = false;
      }, 2000);
      return;
    }

    //API Call For Verify Token
    let data: Object = this.setPasswordForm.value;
    data['token'] = this.token;
    this.authService.setLoginPassword(data).subscribe(response => {
      if (response['status']) {
        this.router.navigate(['/auth/login']).then(() => {
          this.alertify.success(response['message']);
        });
      }
    });
  }

  /**
   * Fn. to Verify Token
   */
  public verifyToken(token: string) {
    // Verify Token
    this.authService.verifyRegistrationToken(token).subscribe(response => {
      if (response['status']) {
        this.enableForm = true;
      } else {
        this.redirectToLogin();
      }
    }, error => {
      if (error) {
        this.redirectToLogin();
      }
    });
  }

  private redirectToLogin() {
    this.router.navigate(['/auth/login']).then(() => {
      this.alertify.error('Invalid Request!!');
    });
  }

  patternValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return null;
      }
      const regex = new RegExp('^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$');
      const valid = regex.test(control.value);
      // alert(valid)
      return valid ? null : {invalidPassword: true};
    };
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.setPasswordForm.controls;
  }
}
