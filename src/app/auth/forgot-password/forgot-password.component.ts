import {Component, OnInit, Injector, ViewChild} from '@angular/core';
import {AuthController} from '../auth-controller';
import {FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent extends AuthController implements OnInit {
  // FormGroup Declaration
  forgotPwdForm: FormGroup;

  /**
   * response data after forgot password request
   */
  responseData: any;

  showMessage: boolean = false;

  invalidUsername: boolean = false;
  invalidMsg: string;

  @ViewChild('requestSuccess') requestSuccessMod;

  constructor(public inj: Injector) {
    super(inj);
  }

  ngOnInit(): void {
    this.forgotPwdForm = this.formBuilder.group({
      email: ['', Validators.required],
      username: ['', Validators.required],
    });
  }

  /**
   * Fn. to submit
   */
  public onSubmit() {
    if (this.forgotPwdForm.invalid) {
      this.validateAllFormFields(this.forgotPwdForm);
      this.displayInvalidFormControls(this.forgotPwdForm);
      return;
    }
    this.authService.resetPassword(this.forgotPwdForm.value).subscribe(
      (response) => {
        if (response['status']) {
          this.responseData = response;
          this.open(this.requestSuccessMod);
          this.router.navigate(['auth/login']);
        } else {
          this.invalidUsername = true;
          this.alertify.error(response['message']);
        }
      },
      (error) => {
        this.alertify.error(error);
      }
    );
  }

  cancelBtnClick() {
    this.modalService.dismissAll();
  }
}
