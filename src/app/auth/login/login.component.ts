import {Component, OnInit, Injector} from '@angular/core';
import {FormGroup, Validators} from '@angular/forms';
import {AuthController} from '../auth-controller';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent extends AuthController implements OnInit {
  // FormGroup declaration
  loginForm: FormGroup;
  forgotPasswordForm: FormGroup;
  hasHttpError: boolean = false;
  httpErrorMessage: string;
  noBranchMessage: string;
  noDepartmentMessage: string;

  constructor(public inj: Injector) {
    super(inj);
  }

  ngOnInit(): void {
    // Clear Session
    this.currentUser.clear();
    this.authService.userLogout();
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      grant_type: ['password'],
    });
  }

  public submitForgotPwd() {
    if (this.forgotPasswordForm.invalid) {
      this.validateAllFormFields(this.forgotPasswordForm);
      this.displayInvalidFormControls(this.forgotPasswordForm);
      return;
    }
  }

  get field() {
    return this.loginForm.controls;
  }

  /**
   * Fn. to submit login form
   */
  onSubmit() {
    if (this.loginForm.invalid) {
      this.validateAllFormFields(this.loginForm);
      this.displayInvalidFormControls(this.loginForm);
      return;
    }
    this.hasHttpError = false;
    this.loginService.userLogin(this.loginForm.value).subscribe(
      (response: any) => {
        if (response != null || response != undefined) {
          this.authService.setUserSession(response);
          this.initializer.initApplication().then((status) => {

            if (status.code == 'no-branch') {
              this.noBranchMessage = this.translate.instant('noBranchMessage');
              this.authService.userLogout();
              return;
            }

            if (status.code == 'password-expired') {
              //Navigate to change password
              this.currentUser.set('passwordExpired', true);
              this.router.navigate([`/auth/password-change`]);
              return;
            }

            if (status == true) {

              if (this.currentUser.hasCurrentBranch()) {
                console.log(this.currentUser);

                //Refresh Token With Branch
                let branchId = this.currentUser.getCurrentBranch().id;
                let data = {
                  refreshToken: this.authService.getRefreshToken(),
                  branchId: branchId
                };
                this.authService.refreshToken(data).toPromise().then(response => {
                  if (response) {
                    this.authService.setUserSession(response);

                    //Navigate to Select Dashboard
                    this.router.navigate(['/featured']);
                  }
                });
              } else {
                //Navigate to Select Branch
                this.router.navigate(['/auth/select-branch']);
              }
            }
          });
        } else {
          this.hasHttpError = true;
          this.httpErrorMessage = response['message'];
        }
      },
      (error) => {
        this.hasHttpError = true;
        this.httpErrorMessage = this.translate.instant('httpErrorMessage');
      }
    );
  }

}
