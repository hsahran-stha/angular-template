import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';

@Injectable()
export class LoginService {
  constructor(
    private httpClient: HttpClient,
    public authService: AuthService,
    private router: Router
  ) {
  }

  /**
   * Fn. for user login
   * @param data : Login Data
   */
  public userLogin(data) {
    const loginRequest = new FormData();
    loginRequest.append('username', data.username.trim());
    loginRequest.append('password', data.password);
    loginRequest.append('grant_type', data.grant_type);

    let basicAuth = btoa('financialtransfer:fintransfer@123');
    const headers = new HttpHeaders({
      'accept-language': localStorage.getItem('lang'),
      'Authorization': `Basic ${basicAuth}`,
    });
    return this.httpClient.post(
      `${environment.API_URL}oauth/token`,
      loginRequest,
      {
        headers: headers,
      }
    );
  }
}
