import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {AuthComponent} from './auth.component';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {SelectBranchComponent} from './select-branch/select-branch.component';
import {VerifyComponent} from './verify/verify.component';
import {AuthGuard} from '../guards/auth.guard';
import {PasswordResetRequestComponent} from './password-reset-request/password-reset-request.component';
import {PasswordChangeComponent} from './password-change/password-change.component';

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {path: '', redirectTo: 'login', pathMatch: 'full'},
      {path: 'login', component: LoginComponent},
      {path: 'forgot-password', component: ForgotPasswordComponent},
      {path: 'password-change', component: PasswordChangeComponent, canActivate: [AuthGuard]},
      {path: 'select-branch', component: SelectBranchComponent, canActivate: [AuthGuard]},
      {path: 'verify', component: VerifyComponent},
      {path: 'reset_req', component: PasswordResetRequestComponent}
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {
}
