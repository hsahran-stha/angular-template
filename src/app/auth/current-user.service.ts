import {Injectable} from '@angular/core';
import {User} from '../shared/models/auth/user';
import {Role} from '../shared/models/auth/role';
import {Menu} from '../shared/models/auth/menu';
import {Branch} from '../shared/models/auth/branch';
import {UserDetails} from '../shared/models/auth/userDetails';
import {Department} from '../shared/models/auth/department';
import {ApplicationProfile} from '../shared/models/auth/companyProfile';
import {environment} from '../../environments/environment';
import {MaxAuthData} from '../shared/models/auth/maxAuthData';

@Injectable({
  providedIn: 'root',
})
export class CurrentUserService {
  /**
   * Current User Instance
   * @type {User}
   */
  public current: User;

  /**
   * For caching current user's roles
   * @type {Object}
   */
  private allRoles: Role[];

  /**
   * For caching current user's actions
   * @type {Object}
   */
  private allActions: Array<any>;

  constructor() {
  }

  /**
   * Assign Current User
   * @param {User} model
   */
  public assignCurrentUser(model?: User) {
    //clear
    this.clear();
    if (model) {
      this.current = model;
      this.assignBranch();
      this.assignDepartment();
    }
  }

  /**
   * Assign Company
   */
  public assignCompany(applicationProfile?: ApplicationProfile) {
    if (applicationProfile) {
      this.setCurrentCompany(JSON.stringify(applicationProfile));

    }
  }

  /**
   * Assign department
   */
  public assignDepartment() {
    if (this.get('departments')) {
      let departments: Department[] = this.get('departments');
      if (departments.length == 1) {
        this.setCurrentDepartment(btoa(departments[0].id.toString()));
      }
    }
  }

  /**
   remove department
   */
  removeDepartment() {
    localStorage.removeItem('deptId');
  }

  /**
   * Assign Branch
   */
  public assignBranch() {
    //Set Branch
    if (this.get('branches')) {
      let branches: Branch[] = this.get('branches');
      //Having Only One Branch
      if (branches.length == 1) {
        this.setCurrentBranch(btoa(branches[0].id.toString()));
      }
    }
  }

  /**
   * Assign current user menus
   * @param menus : Menus List
   */
  public assignMenus(menus: Array<Menu>) {
    //clear
    this.clearMenus();
    if (menus) {
      this.current.menus = menus;
    }
  }

  /**
   * Assign current user details
   * @param userDetails : user details
   */
  public assignUserDetails(userDetails: UserDetails) {
    //clear
    //this.current.userDetails = [];
    if (userDetails) {
      this.current.userDetails = userDetails;
    }
  }

  /**
   * Clears the current user model
   */
  public clear() {
    this.current = new User({});
  }

  /**
   * Clears the current user model
   */
  public clearMenus() {
    this.current.menus = [];
  }

  public getMenus(): Array<Menu> {
    return this.current.menus;
  }

  /**
   * Returns current user Model
   */
  public getModel(): User {
    return Object.assign({}, this.current);
  }

  /**
   * Check Current User has given property exists or not
   * @param  {string}  property
   * @return {boolean}
   */
  public has(property: string): boolean {
    return this.current[property] && this.current[property] != null
      ? true
      : false;
  }

  /**
   * Return a current user property value
   * @param  {string} property propery name to get value
   * @return {any} property | false
   */
  public get(property: string): any {
    return this.current && this.current[property];
  }

  /**
   * Sets the current user property value
   * @param {string} property property name to set
   * @param {any}    value property name to set
   */
  public set(property: string, value: any): void {
    this.current[property] = value;
  }

  /**
   * Check if current user is logged in.
   */
  public isLoggedIn() {
    return this.get('userId') > 0 && localStorage.getItem('accessToken');
  }

  /**
   * Returns Promise that indicated user is logged or not
   */
  public isAuthenticated(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (this.isLoggedIn()) {
        resolve(true);
      } else {
        resolve(false);
      }
    });
  }

  /**
   * Check if current user is super user.
   */
  public isSuperUser() {
    return this.current && this.current['is_superuser'] == true;
  }

  /**
   * Returns all user's actions
   */
  public getAllActions(): Array<any> {
    //if actions already cached locally
    if (this.allActions) {
      return this.allActions;
    }

    //current user's groups and actions
    let actions = this.current['actions'] || [];
    return actions;
  }

  /**
   * Check if current user has permission to a specific  action or not
   * @param  {string}  action action name
   * @return {boolean}
   */
  public hasPermission(actionId: number): boolean {
    //check superuser
    //prepares array of action names and check the given perm exists or not
    return this.isSuperUser() || this.getAllActions().indexOf(actionId) > -1;
  }

  /**
   * Function that returns whether user has permission to module or not
   *
   * @param {number} processId
   * @returns {boolean}
   * @memberof CurrentUser
   */
  public hasModuleAccess(processId: number): boolean {
    return this.has('modules') && this.get('modules').indexOf(processId) > -1
      ? true
      : false;
  }

  /**
   * Function that checks whether the use has a particular role or not
   */
  public hasRole(role: any) {
    let userRoles: Array<Role> = this.allRoles || this.current['roles'];
    if (!userRoles) {
      return false;
    }
    //Check role exists

    return userRoles.includes(role);
  }

  /**
   * Function that current branch
   */
  public setCurrentBranch(bid: string) {
    localStorage.setItem('bid', bid);
  }

  /**
   * Function that current department
   */
  public setCurrentDepartment(deptId: string) {
    localStorage.setItem('deptId', deptId);
  }

  public setAllAssignedDepartment(departments: string) {
    localStorage.setItem('departments', departments);
  }

  /**
   * Check user has current branch
   */
  public hasCurrentBranch(): boolean {
    return !!localStorage.getItem('bid');
  }

  /**
   * Function that set current company
   */
  public setCurrentCompany(applicationProfile: any) {
    localStorage.setItem('application', applicationProfile);
    this.setFaviconIcon();
    this.setTitleName();
  }

  /**
   * Function that set current favicon icon
   */
  public setFaviconIcon() {
    let faviconLogo = this.getCurrentCompany()['favIcon'];
    if (faviconLogo) {
      let favicon: any = document.querySelector('#favicon');
      favicon.href = faviconLogo;
    }
  }

  /**
   * Function that set title name
   */
  public setTitleName() {
    let title: any = document.querySelector('title');
    title.innerHTML = this.getCurrentCompany()['applicationName'] ? this.getCurrentCompany()['applicationName'] : '';
  }


  /**
   * Function that returns selected branch
   */
  public getCurrentBranch(): Branch {
    //Get Bid from local Storage
    let bid: number = parseInt(atob(localStorage.getItem('bid')));
    // Get Selected Branch
    return this.current.branches.find(branch => branch.id == bid);
  }

  /**
   * Function that returns selected branch
   */
  public getCurrentDepartment(): Department {
    //Get deptid from Local Storage
    let deptId: number = parseInt(atob(localStorage.getItem('deptId')));

    // Get Selected department
    return this.current.departments.find(dept => dept.id == deptId);
  }

  /**
   * Function that returns company info
   */
  public getCurrentCompany(): ApplicationProfile {
    let application: any = localStorage.getItem('application');
    return JSON.parse(application);
  }

  /**
   * Get Current User Branch ID
   */
  public getCurrentBranchID(): number {
    return this.getCurrentBranch().id;
  }

  /**
   * Check Current Branch Is Head Branch or Not
   */
  public isHeadBranch(): boolean {
    let currentBranch: Branch = this.getCurrentBranch();
    // Head Branch Type = 1
    const HEAD_BRANCH_TYPE_ID: number = 1;
    if (currentBranch) {
      return currentBranch.branchtypeid == HEAD_BRANCH_TYPE_ID ? true : false;
    }
    return false;
  }

  /**
   * Check Current Branch Is ministry or Not
   */
  public isMinistryBranch(): boolean {
    let currentBranch: Branch = this.getCurrentBranch();
    // Head Branch Type = 1
    const HEAD_BRANCH_TYPE_ID: number = 2;
    if (currentBranch) {
      return currentBranch.branchtypeid == HEAD_BRANCH_TYPE_ID ? true : false;
    }
    return false;
  }
  /**
   * Check Current Branch Is ministry or Not
   */
  public isLocalBranch(): boolean {
    let currentBranch: Branch = this.getCurrentBranch();
    // Head Branch Type = 1
    const HEAD_BRANCH_TYPE_ID: number =3;
    if (currentBranch) {
      return currentBranch.branchtypeid == HEAD_BRANCH_TYPE_ID ? true : false;
    }
    return false;
  }

  /**
   * Set Current Menu
   * @param menu
   */
  public setCurrentMenu(menu: Menu) {
    this.current.currentMenu = menu;
  }

  /**
   * Get Current Menu
   */
  public getCurrentMenu(): Menu {
    return this.current.currentMenu;
  }

  /**
   * Clear Current Menu
   */
  public clearCurrentMenu(): Menu {
    return this.current.currentMenu = new Menu();
  }

  /**
   * Set Active Menu By Code
   * @param menuCode
   */
  public setMenuByCode(menuCode: string) {
    //Reset Current Menu
    this.clearCurrentMenu();
    if (this.current.menus) {
      for (let menu of this.current.menus) {
        if (menu['menucode'] == menuCode) {
          this.current.currentMenu = menu;
        } else {
          if (menu.children.length > 0) {
            //Recursive Over Children
            this.findMenuRecursivelyByKey(menu, 'menucode', menuCode);
          }
        }
      }
    }
  }

  /**
   * Set Active Menu By Code
   * @param menuCode
   */
  public searchMenuByCode(menuCode: string) {
    if (this.current.menus) {
      for (let menu of this.current.menus) {
        if (menu['menucode'] == menuCode) {
          this.current.currentMenu = menu;
        } else {
          if (menu.children.length > 0) {
            //Recursive Over Children
            this.findMenuRecursivelyByKey(menu, 'menucode', menuCode);
          }
        }
      }
    }
  }

  /**
   * Set Active Menu By URL
   * @param URL
   */
  public setMenuByURL(URL: string) {
    //Reset Current Menu
    this.clearCurrentMenu();
    if (this.current.menus) {
      for (let menu of this.current.menus) {
        if (menu['url'] == URL) {
          this.current.currentMenu = menu;
        } else {
          if (menu.children.length > 0) {
            //Recursive Over Children
            this.findMenuRecursivelyByKey(menu, 'url', URL);
          }
        }
      }
    }
  }


  /**
   * Check Permission to for action
   * @param action
   */
  public hasMenuActionPermission(action: string): boolean {
    // console.log(action);
    if (this.isSuperAdminUser()) {
      return true;
    }
    if (!this.current.currentMenu) {
      return false;
    }
    return this.current.currentMenu[action];
  }

  /**
   * Check maxAuthData
   * @param maxAuthData
   */
  public setMaxAuthLevel(maxAuthData: MaxAuthData) {
    this.current.currentMenu.maxAuthData = maxAuthData;
  }

  /**
   * Check maxAuthData
   * @param action
   */
  public hasMaxAuthLevel(action: string): number {
    return this.current.currentMenu.maxAuthData['maxauthlevel'];
  }

  /**
   * Find Menu By Code Recursively
   * @param menus
   * @param code
   */
  private findMenuRecursivelyByKey(menu: Menu, key: string, value: string): Menu {

    for (let mnu of menu.children) {
      // console.log(mnu);
      if (mnu[key] == value) {
        //Set Current Menu
        this.setCurrentMenu(mnu);
        return mnu;
      } else {
        //Recursive Over Children
        this.findMenuRecursivelyByKey(mnu, key, value);
      }
    }
  }

  /**
   * Check Whether The Current User Is Super User or not
   */
  public isSuperAdminUser(): boolean {
    try {
      let roles: string = this.getCurrentBranch().roles;
      let rolesArray: Array<string> = roles.split(',');
      if (rolesArray.indexOf('1') > -1) {
        return true;
      }
      return false;
    } catch (e) {

    }
  }

  /**
   * Get Profile Name
   */
  public getUserProfileName(): string {
    if (this.current.userDetails) {
      let fullname = this.current.userDetails.fullname;
      return `${fullname}`;

    }
    return this.current.username;
  }

  /**
   * Get Profile Image
   */
  public getUserProfileImage(): string {
    const len = this.current.userDetails.images.length;

    if (this.current.userDetails && this.current.userDetails.images[len - 1]) {
      return `${environment.API_URL}${this.current.userDetails.images[len - 1]}`;
    }
    //Retrun Default Image
    return './assets/img/user.jpg';
  }
}
