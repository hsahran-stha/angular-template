import { Component, OnInit, Injector } from '@angular/core';
import { AuthController } from './auth-controller';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
})
export class AuthComponent extends AuthController implements OnInit {
  constructor(public inj: Injector) {
    super(inj);
  }

  ngOnInit(): void {}
}
