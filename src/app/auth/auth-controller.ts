import {Injector} from '@angular/core';
import {AuthService} from './auth.service';
import {LoginService} from './login/login.service';
import {AppController} from '../app.controller';


export class AuthController extends AppController {
  public authService: AuthService;
  public loginService: LoginService;

  constructor(public injector: Injector) {
    super(injector);
    this.authService = this.injector.get(AuthService);
    this.loginService = this.injector.get(LoginService);
  }
}
