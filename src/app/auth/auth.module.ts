import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthRoutingModule} from './auth-routing.module';
import {LoginComponent} from './login/login.component';
import {LoginService} from './login/login.service';
import {AuthComponent} from './auth.component';
import {SharedModule} from '../shared/shared.module';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {SelectBranchComponent} from './select-branch/select-branch.component';
import {VerifyComponent} from './verify/verify.component';
import {PasswordResetRequestComponent} from './password-reset-request/password-reset-request.component';
import {PasswordChangeComponent} from './password-change/password-change.component';

@NgModule({
  declarations: [AuthComponent,
    LoginComponent,
    ForgotPasswordComponent,
    SelectBranchComponent,
    VerifyComponent,
    PasswordResetRequestComponent,
    PasswordChangeComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    SharedModule,
  ],
  providers: [LoginService],
})
export class AuthModule {
}
