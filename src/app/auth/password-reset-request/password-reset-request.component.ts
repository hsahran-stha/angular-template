import {Component, Injector, OnInit} from '@angular/core';
import {FormGroup, Validators} from '@angular/forms';
import {AuthController} from '../auth-controller';

@Component({
  selector: 'app-password-reset-request',
  templateUrl: './password-reset-request.component.html',
  styleUrls: ['./password-reset-request.component.scss']
})
export class PasswordResetRequestComponent extends AuthController implements OnInit {
  /**
   * reset request form group
   */
  resetRequestForm: FormGroup;

  /**
   * token for request
   */
  token: string;

  constructor(public inj: Injector) {
    super(inj);
    // Get Token and Verify Token with Server
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['token']) {
        this.token = params['token'];
        this.verifyToken(this.token);
      } else {
        this.redirectToLogin();
      }
    });
  }

  ngOnInit(): void {
    this.initializeForm();
    this.resetRequestForm.disable();
  }

  /**
   * form group initialization
   */
  initializeForm() {
    this.resetRequestForm = this.formBuilder.group({
      password: ['', Validators.required],
      retype: ['', Validators.required],
      token: ['']
    });
  }

  /**
   * on reset form submit
   */
  onSubmit() {
    if (this.resetRequestForm.invalid) {
      this.validateAllFormFields(this.resetRequestForm);
      this.displayInvalidFormControls(this.resetRequestForm);
      return;
    }

    let pwd = this.resetRequestForm.get('password').value;
    let retypedPwd = this.resetRequestForm.get('retype').value;

    if (pwd !== retypedPwd) {
      this.alertify.error('Passwords don\'t match.');
      return;
    }

    //API Call For Verify Token
    let data: Object = this.resetRequestForm.value;
    // data['token'] = this.token;
    this.authService.setLoginPassword(data).subscribe(response => {
      if (response['status']) {
        this.router.navigate(['/auth/login']).then(() => {
          this.alertify.success(response['message']);
        });
      }
    });
  }

  /**
   * verify if token is valid
   * @param token :token
   */
  verifyToken(token) {
    // Verify Token
    this.authService.verifyResetToken(token).subscribe(response => {
      if (response['status']) {
        this.resetRequestForm.enable();
        this.resetRequestForm.get('token').setValue(token);
      } else {
        this.redirectToLogin();
      }
    }, error => {
      if (error) {
        this.redirectToLogin();
      }
    });
  }

  /**
   * redirect to login page
   */
  redirectToLogin() {
    this.router.navigate(['/auth/login']).then(() => {
      this.alertify.error('Invalid Request!!');
    });
  }

}
