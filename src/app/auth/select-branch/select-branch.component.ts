import {Component, Injector, OnInit} from '@angular/core';
import {AuthController} from '../auth-controller';
import {FormGroup, Validators} from '@angular/forms';
import {Branch} from '../../shared/models/auth/branch';

@Component({
  selector: 'app-select-branch',
  templateUrl: './select-branch.component.html',
  styleUrls: ['./select-branch.component.scss']
})
export class SelectBranchComponent extends AuthController implements OnInit {

  /**
   * Default Constructor
   * @param injector
   */
  constructor(public injector: Injector) {
    super(injector);
  }

  private selectedBranchId: any = '';
  public selectBranchFom: FormGroup;
  noDepartmentMessage: string;

  ngOnInit(): void {
    //Check Selected
    if (this.currentUser.hasCurrentBranch()) {
      this.selectedBranchId = this.currentUser.getCurrentBranch().id;
    }
    this.selectBranchFom = this.formBuilder.group({
      branchId: [this.selectedBranchId, Validators.required]
    });
  }

  /**
   * Handle On Submit
   */
  public onSubmit() {
    if (this.selectBranchFom.invalid) {
      this.validateAllFormFields(this.selectBranchFom);
      this.displayInvalidFormControls(this.selectBranchFom);
      return;
    }
    //Selected Branch
    let branchId = this.selectBranchFom.get('branchId').value;

    this.currentUser.setCurrentBranch(btoa(branchId));

    //To Refresh Token With Branch
    let data = {
      refreshToken: this.authService.getRefreshToken(),
      branchId: branchId
    };
    //Refresh Token
    this.authService.refreshToken(data).toPromise().then(response => {
      if (response) {
        this.authService.setUserSession(response);
        this.router.navigate(['/featured']);
      }
    });
    //this.router.navigate(['/featured']);
  }
}
