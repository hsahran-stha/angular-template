import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {CurrentUserService} from './current-user.service';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private router: Router, private httpClient: HttpClient,
              private currentUser: CurrentUserService) {
  }


  isUserLoggedIn() {
    let token = localStorage.getItem('access_token');
    if (token === null) {
      return false;
    }
    return true;
  }

  isDepartmentLoggedIn() {
    let deptId = localStorage.getItem('deptId');
    if (deptId === null) {
      return false;
    }

    return true;
  }


  getLoggedInToken() {
    let token = localStorage.getItem('access_token');
    if (token === null) {
      return '';
    }
    return token;
  }

  getRefreshToken() {
    let token = localStorage.getItem('refresh_token');
    if (token === null) {
      return '';
    }
    return token;
  }

  /**
   * method to logout user
   */
  userLogout() {
    let language = localStorage.getItem('lang');
    let applicationProfile = localStorage.getItem('application');
    localStorage.clear();
    localStorage.setItem('lang', language);
    localStorage.setItem('application', applicationProfile);
    this.router.navigate(['/auth/login']);

  }

  /**
   * Function that sets current user local
   *
   * @private
   * @param {*} data response from server
   * @memberof ServicedeskLoginComponent
   */
  public setUserSession(data) {
    let accessToken = data.access_token;
    let refreshToken = data.refresh_token;
    this.setAccessToken(accessToken);
    this.setRefreshToken(refreshToken);
  }

  /**
   * Sets Auth Token
   * @param {string} token
   */
  public setAccessToken(token: string) {
    localStorage.setItem('access_token', token);
  }

  /**
   * Set Refresh Token
   *
   * @param {string} token
   * @memberof AuthService
   */
  public setRefreshToken(token: string) {
    localStorage.setItem('refresh_token', token);
  }

  /**
   * Reset Password
   * @param data
   */
  public resetPassword(data: Object): Observable<any> {
    let resetURL: string = `${environment.API_URL}reset_req`;
    return this.httpClient.post(resetURL, data);
  }

  /**
   * Verify Token
   * @param token
   */
  public verifyRegistrationToken(token: string): Observable<any> {
    let verificationURL: string = `${environment.API_URL}verify?token=${token}`;
    return this.httpClient.get(verificationURL, {observe: 'response'});
  }

  public verifyResetToken(token: string): Observable<any> {
    let verificationURL: string = `${environment.API_URL}reset_req?token=${token}`;
    return this.httpClient.get(verificationURL, {observe: 'response'});
  }

  /**
   * Function that sets password
   * @param data
   */
  public setLoginPassword(data: Object): Observable<any> {
    let setPasswordURL: string = `${environment.API_URL}password/reset`;
    return this.httpClient.post(setPasswordURL, data);
  }

  /**
   * Function that resets password
   * Incase of forgotten password
   * @param data
   */
  public resetLoginPassword(data: Object): Observable<any> {
    let setPasswordURL: string = `${environment.API_URL}password/reset`;
    return this.httpClient.post(setPasswordURL, data);
  }

  /**
   * Fn. for Refresh Token
   * @param data : Login Data
   */
  public refreshToken(data): Observable<any> {
    const loginRequest = new FormData();
    loginRequest.append('refresh_token', data.refreshToken);
    loginRequest.append('branchId', data.branchId);
    loginRequest.append('grant_type', 'refresh_token');
    loginRequest.append('access_token', this.getLoggedInToken());

    let basicAuth = btoa('financialtransfer:fintransfer@123');
    const headers = new HttpHeaders({
      Authorization: `Basic ${basicAuth}`,
    });
    return this.httpClient.post(
      `${environment.API_URL}oauth/token`,
      loginRequest,
      {
        headers: headers,
      }
    );
  }

  /**
   * Fn. for Fetching Menus
   * @param data : Login Data
   */
  public fetchMenus(officeId): Observable<any> {
    return this.httpClient.get(
      `${environment.API_URL}menu/get/user/${officeId}`
    ).pipe(map(data => data['data']));
  }


  /**
   * Fn. for Fetching Menus
   * @param data : change password
   */
  changePwd(data: any) {
    let url = environment.API_URL + 'password/new';
    return this.httpClient.post<any>(url, data).pipe();
  }

}
