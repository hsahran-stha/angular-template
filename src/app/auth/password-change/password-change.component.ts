import {Component, Injector, OnInit} from '@angular/core';
import {FormGroup, Validators} from "@angular/forms";
import {AuthController} from "../auth-controller";

@Component({
  selector: 'app-password-change',
  templateUrl: './password-change.component.html',
  styleUrls: ['./password-change.component.scss']
})
export class PasswordChangeComponent extends AuthController implements OnInit {


  changePwdForm: FormGroup

  // pwd mismatch flag
  pwdMismatch: boolean = false;

  /**
   * flag to check new pwd and confirm pwd
   */
  isPasswordSame: boolean;

  /**
   * flag to check old pwd is correct
   */
  invalidOldPwd: boolean = false;

  /**
   * invlaid old pwd msg
   */
  invalidOldMsg: string;

  constructor(public inj: Injector) {
    super(inj)
    console.log(this.currentUser)
  }

  ngOnInit(): void {
    this.initializeForm();
  }

  /**
   * form group initialization
   */
  initializeForm() {
    this.changePwdForm = this.formBuilder.group({
        password: ['', Validators.required],
        oldPassword: ['', Validators.required],
        retypePassword: ['', Validators.compose([Validators.required])],
      },
    );
  }

  /**
   * fn to handle password change
   */
  public onPasswordChange() {
    if (this.changePwdForm.invalid) {
      this.validateAllFormFields(this.changePwdForm);
      this.displayInvalidFormControls(this.changePwdForm);
      return;
    }

    let pwd = this.changePwdForm.get('password').value
    let retype = this.changePwdForm.get('retypePassword').value
    if (pwd !== retype) {
      this.pwdMismatch = true;
      setTimeout(() => {
        this.pwdMismatch = false
      }, 2000)
      return
    }

    this.authService.changePwd(this.changePwdForm.value).subscribe(
      (response) => {
        if (response.status) {
          this.alertify.success(response.message);
          this.modalService.dismissAll();
          this.authService.userLogout();
        } else {
          this.invalidOldPwd = true;
          this.invalidOldMsg = response.message;
          setTimeout(() => {
            this.invalidOldPwd = false
          }, 2000)
          // this.alertify.error(response.message);
        }
      }, (error) => {
        this.alertify.error(error);
      }
    );
  }


}
