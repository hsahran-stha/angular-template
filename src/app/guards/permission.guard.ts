import {Injectable} from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router, CanActivateChild,
} from '@angular/router';
import {Observable} from 'rxjs';
import {CurrentUserService} from '../auth/current-user.service';
import {environment} from '../../environments/environment';
import {TranslateService} from '@ngx-translate/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {AlertifyService} from '../shared/service/alertifyjs/alertify.service';

@Injectable({
  providedIn: 'root',
})
export class PermissionsGuard implements CanActivate, CanActivateChild {
  /**
   *Token Exclude Paths
   */
  private excludePaths: Array<string> = [
    `/`,
    `/auth/login`,
    `/auth/select-branch`,
  ];

  /**
   * Check Is Approval Request
   */
  public isApprovalRequest: boolean = false;

  /**
   * Navigation State Data
   */
  public navigationStateData: Object = {};

  constructor(
    private currentUser: CurrentUserService,
    private router: Router,
    private alertifyService: AlertifyService,
    private translate: TranslateService,
  ) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return true;
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    /**
     * No need to check in some paths
     */
    if (this.excludePaths.includes(state.url) && this.currentUser.get('username')) {
      return true;
    }
    if (this.currentUser.isSuperAdminUser()) {
      return true;
    }

    /**
     * Check Current Menu Set or not
     * If not set set current menu from routing data or current URL
     */
    this.checkNavigationStateData(childRoute, state);


    const pageName = state.url.split('/').slice(4, 5).toString();

    this.checkPermissionByPageName(environment.menuActions.view);

    if (pageName === 'create') {
      this.checkPermissionByPageName(environment.menuActions.create);
    }
    if (pageName === 'edit') {
      this.checkPermissionByPageName(environment.menuActions.update);
    }
    if (pageName === 'delete') {
      this.checkPermissionByPageName(environment.menuActions.delete);
    }

    return true;
  }

  /**
   * Check Perimission
   */
  public checkPermissionByPageName(page?: string): boolean {
    if (!this.currentUser.hasMenuActionPermission(page)) {
      this.router.navigate([`/financial-transfer/dashboard`]).then(() => {
        this.alertifyService.error(this.translate.instant('noAccessToResources'));
      });

      return false;
    }
  }

  /**
   * Check Navigation State Data
   */
  public checkNavigationStateData(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // Get Data From Routing Data
    if (childRoute.data['menucode']) {
      if (this.currentUser.getMenus()) {
        //Set Current Menu
        this.currentUser.setMenuByCode(childRoute.data['menucode']);
      }
    } else {
      this.currentUser.setMenuByURL(state.url);
    }
  }
}
