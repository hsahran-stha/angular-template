function init() {
    let inputArray = document.querySelectorAll(".form-grid-floating .form-control");

    for (var i = 0; i < inputArray.length; i++) {
        console.log("working")
        //check input value
        let func = (input) => {
            if (input.value !== "") {
                let label1 = input.parentNode.querySelector('.form-grid-floating label')
                label1.classList.add("floating")
            }
        }

        //add class when input is focused
        func(inputArray[i]);
        inputArray[i].addEventListener("focus", function () {
            let label1 = this.parentNode.querySelector('.form-grid-floating label')
            label1.classList.add("floating")
        })
 
        //remove class when not focused based on input value
        inputArray[i].addEventListener("blur", function () {
            let label1 = this.parentNode.querySelector('.form-grid-floating label')

            if (this.value !== "" || this.type == "date" || this.tagName == "SELECT") {
                label1.classList.add("floating");
            }

            else {
                label1.classList.remove("floating")
                console.log("blur working")
            }
        })
    }
}
