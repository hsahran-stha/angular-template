function shortcutShow() {
    let card = document.querySelector(".card--shortcut");
    card.style.transform = "translateX(0)";
}
function shortcutHide() {
    let card = document.querySelector(".card--shortcut");
    card.style.transform = "translateX(100%)";
}

