import $ from "jquery";
import "chart.js";
//bootstrap imports
import "bootstrap/dist/js/bootstrap";
import Tagify from '@yaireo/tagify';

import 'bstreeview/src/js/bstreeview';


//custom css
import "../../scss/main.scss";

//https://iamakulov.com/notes/optimize-images-webpack/ for image optimization
function myTest() {
  alert('Welcome to custom js');
}

$(function() {
  alert('Hello, custom js');
});

document.getElementById('toggler').addEventListener('click', function () {
  $(".app").toggleClass("toggled");
  console.log("working")
})

document.getElementById("test").addEventListener('click', () => {
  alert("working")
})
// if (document.querySelector("body").contains((document.querySelector("#line-chart") || document.querySelector("#pie-chart")))) {

//   $(() => {
// line chart
var chart = new Chart(document.getElementById("line-chart"), {
  type: "line",

  data: {
    // showInLegend: true,

    // markerType: "none",
    // legendText : "circle",
    labels: [
      "20/11",
      "22/11",
      "24/11",
      "26/11",
      "28/11",
      "30/11",
      "02/12",
      "04/12",
      "06/12",
      "08/12",
      "10/12",
      "12/12",
    ],

    datasets: [
      {
        label: "New Users",
        borderColor: "green",
        borderWidth: 2,
        backgroundColor: "transparent",

        data: [500, 700, 750, 600, 500, 550, 600, 700, 800, 900, 950, 900],
      },
      {
        label: "Referals",
        borderColor: "orange",
        borderWidth: 2,
        backgroundColor: "transparent",
        data: [300, 250, 400, 500, 600, 550, 700, 500, 400, 550, 800, 850],
      },
    ],
  },
  options: {
    responsive: true,

    legend: {
      position: "top",
      align: "start",
      display: true,
    },
    elements: { point: { radius: 0 } },
  },
});


// piechart
var piechart = new Chart(document.getElementById("pie-chart"), {
  type: "doughnut",
  data: {
    datasets: [
      {
        data: [30, 20, 30],
        backgroundColor: ["green", "red", "gray"],
      },
    ],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: ["Standard", "Big", "Luxury"],
  },
  options: {
    responsive: true,
  },
});

//   });
// }

// let tagify1 = new Tagify(document.getElementById("test1"), { whitelist: ["apple", "banana", "orange", "mango"] })
// let tagify2 = new Tagify(document.getElementById("test2"), { whitelist: ["apple", "banana", "orange", "mango"] })
// let tagify3 = new Tagify(document.getElementById("test3"), { whitelist: ["apple", "banana", "orange", "mango"] })
// let tagify4 = new Tagify(document.getElementById("test4"), { whitelist: ["apple", "banana", "orange", "mango"] })

// var tree = [
//   {
//     text: "Category 1",
//     nodes: [
//       {
//         text: "Sub-category 1",
//         nodes: [
//           {
//             id: "sub-node-1",
//             text: "Sub child category 1",

//             class: "nav-level-3",
//             href: "https://google.com"
//           },
//           {
//             text: "Sub child category 2",

//           }
//         ]
//       },
//       {
//         text: "Sub-category 2",
//       }
//     ]
//   },
//   {
//     text: "Category 2",

//   },
//   {
//     text: "Category 3",

//   },
//   {
//     text: "Category 4",

//   },

// ];




// $('#tree').bstreeview({
//   data: tree,
//   expandIcon: 'ic-arrow-up-a',
//   collapseIcon: 'ic-arrow-down-a',
//   indent: 1.25,
//   // parentsMarginLeft: '0',
//   openNodeLinkOnNewTab: true
// });


